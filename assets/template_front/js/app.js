var App = function (){
	var ur = $('.srd').data('srs');
	var lgn = $('.logo').data('lgn');

	var socket = function (){
		// var socketURL = 'http://localhost:3000';
		// var options = {
		//     transports: ['websocket']
		// };
		// ol = io.connect(socketURL, options);

		$lg = $('#usr-log').data();
	}

	function formatCurrency(n, currency) {
    	return currency + " " + n.toFixed(0).replace(/(\d)(?=(\d{3})+(\.|$))/g, "$1.");
    	// return currency + " " + numeral(n).format(".");
	}
	var promo = function (){
		if ($('div').hasClass('advertise')) {
			if ($('div.advertise').data('show') === 1) {
				$('#promo-modal').modal('show');
			}
		}
	}

	var datepicker = function (){
	    $(".datepicker").datepicker({
	      showInputs: false,
	      format:'yyyy-mm-dd',
	      autoclose: true
	    });
	}

	var hidenotif = function (){
		if ($('#notification').hasClass('shownotif')) {
			if ($('#notification').data('type') === 'success') {
				toastr.options = {
			            "preventDuplicates": true,
			            "positionClass": "toast-top-right"
			        }
			    toastr["success"]($('#notification').data('msg'));
			}else{
				toastr.options = {
			            "preventDuplicates": true,
			            "positionClass": "toast-top-right"
			        }
			    toastr["error"]($('#notification').data('msg'));
			}
			// setTimeout(function (){
			// 	$('#notification .alert').alert('close');
			// }, 10000);

		}
	}

	// var loadimage = function (){
	// 	$('.logo img, .image, .p-cat-wrap, .b-wrap, .p-image, .gallery, .item-l, .image-promo, img, .shp-image').lazy({
	// 		effect: 'fadeIn',
	// 		effectTime: 1000,
	// 		threshold: 0,
	// 		visibleOnly: true,
	// 		onError: function (element){
	// 			element.css('background-image','url("'+ur+'/default/default.jpg")');
	// 		}
	// 	});
	// }

	var validate = function (){

		$(document).on('click', '.subscribe', function (){
			var div = $(this);
			var id = $('input[name=newsletter]').val();
			// var qty = $('input[name=qty]').val();
			// var cart = $('.cart-header').text();
			$.ajax({
	            url: ur +'home/subscribe/',
	            dataType: 'json',
	            type: 'POST',
	            data: {newsletter: id},
	            success: function (result) {
	             window.location = window.location;
	            }
	        });
		});

		$(document).on('keypress', 'input[name="newsletter"]', function (e){
			if (e.which === 13) {
				var div = $(this);
				var id = $('input[name=newsletter]').val();
				$.ajax({
		            url: ur +'home/subscribe/',
		            dataType: 'json',
		            type: 'POST',
		            data: {newsletter: id},
		            success: function (result) {
		             window.location = window.location;
		            }
		        });
			}
		});

		$('.search-good').on('click', function (){
			$(this).parents('form').submit();
		});

		$('.lang-chng').on('click', function (){
			$.ajax({
	            url: ur +'home/language/',
	            dataType: 'json',
	            type: 'GET',
	            success: function (result) {
	             location.reload();
	            },
	            error: function (result){
	            	location.reload();
	            }
	        });
		});
	}

	var address = function (){
    $('.provinceselect').on('change', function(){
        var th = $(this);
        var dc = $(this).val();
        // console.log(dc);
        var prov = $(this).find('option:selected').text();
        if (parseInt(dc) === 0) {
            var inp = "<option value='0'>Select Province First</option>";
            th.closest('form').find('.cityselect').select2({'placeholder':'Select Province First'});
            th.closest('form').find('.cityselect').val("").trigger('change');
            th.closest('form').find('.cityselect').html(inp);
            return;
        }else{
          th.closest('form').find('.delivery_province').val(prov);
            $.ajax({
                url: ur + "account/get_city/" + dc,
                type: "GET",
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (result){
                	// console.log(result);
                    th.closest('form').find('.cityselect').select2({'placeholder':'Select City'});
                    th.closest('form').find('.cityselect').val("").trigger('change');
                    var inp = "<option value=''>Select City</option>";
                    if (result.length === 0) {
                        return;
                    }
                    for (var i = 0; i <= result.length - 1; i++) {
                        inp += "<option value='"+ result[i].city_id +"'>"+ result[i].type +" "+ result[i].city_name +"</option>"
                    }
                    th.closest('form').find('.cityselect').html(inp);
                    if (info_city !== "") {
                      th.closest('form').find('.cityselect').val(info_city).trigger('change');
                    }
                    // $('select[name=city_jne] option').each(function(i,j){
                    //     var data = $('#kota').val();
                    //     var is = $('select[name=city_jne]');
                    //     if(data == $(this).text()){
                    //         is.val($(this).val()).trigger('change');
                    //     }
                    // });
                }
            });
        }
    }).trigger('change');

    $('.cityselect').on('change', function(){
        var th = $(this);
        var city = $(this).find('option:selected').text();
        th.closest('form').find('.delivery_city').val(city);
        
    }).trigger('change');
  }

	$('#forgotpassword-modal').on('shown.bs.modal', function () {
	  	$('#login-modal').modal('hide')
	});
	$('#login-modal').on('shown.bs.modal', function () {
	  	$('#register-modal').modal('hide');
	  	$('#forgotpassword-modal').modal('hide');
	  	$('#cart-modal').modal('hide');
	});
	$('#register-modal').on('shown.bs.modal', function () {
	  	$('#login-modal').modal('hide')
	});
	$('#changeaddress-modal').on('shown.bs.modal', function () {
	  	$('#login-modal').modal('hide')
	});
	$('#addaddress-modal').on('shown.bs.modal', function () {
	  	$('#changeaddress-modal').modal('hide')
	});
	$('#cart-modal').on('shown.bs.modal', function () {
		App.initImage();
	  	// location.reload();
	});

	var select = function () {
      $('.select2').select2({
          minimumResultsForSearch: -1
      });
      $('.select2me').select2({
      });
  }

	// var loadproduct = function (){
	// 	(function($) {
	//         $.Lazy('loadingproduct', function(element, response) {
	//             // just for demonstration, write some text inside element
	//             element.html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');
	//             var mg = '';
	//             var pg = element.data('page');
	//             $.ajax({
	//             	url: ur + "product/ajax_appendpage/"+pg+"/"+null,
	// 	            type: "GET",
	// 	            contentType: "charset=utf-8",
	// 	            success: function (result){
	// 	            	element.parents('.p-wrap.main').append(result);
	// 	            	element.parent().addClass('hide');
	// 	            	element.parent().parent().find('br').remove();
	// 	            	App.initAjax();
	// 	            }
	//             });
	//             // return loaded state to Lazy
	//             response(true);
	//         });
	//     })(window.jQuery || window.Zepto);
 //    	$('.loading-product').lazy({
 //    		effect: 'fadeIn',
 //    		effectTime: 500,
 //    		visibleOnly: true,
 //    		threshold: 0,
 //    		autoDestroy: true,
 //    		chainable: true,
 //    		combined: true
 //    	});

 //    	$(document).on('click', '.load-prduc', function (){
 //    		$('.lodng-dis').removeClass('hide');
 //    		$(this).addClass('hide');
 //    		var th = $(this);
 //            $.ajax({
 //            	url: ur + "product/productappendpage",
	//             type: "GET",
	//             contentType: "charset=utf-8",
	//             success: function (result){
	//             	th.parents('.p-wrap.main').find('.p-list-side.main > ul').append(result);
	//             	th.removeClass('hide');
	//             	App.initAjax();
	//             },
	//             error: function (result){

	//             }
 //            }).done(function (){
	//             	$('.lodng-dis').addClass('hide');
	//             });
 //    	}); 

	// }

	var shop = function(){

		var lchl = $('.chosen-filter li').length;
		if (lchl === 0) {
			$('.filter-caption').addClass('hide');
		}

		$(document).on('blur keypress', '.budget-for-good', function(e){
			if(e.type == 'focusout' || e.which == 13){
				var min = ($('input[name="bmin"]').val()==="")?0:parseInt($('input[name="bmin"]').val());
				var max = ($('input[name="bmax"]').val()==="")?0:parseInt($('input[name="bmax"]').val());;
				var post = {"min":min, "max":max};
				$.ajax({
					url: ur +'product/ajax_budget/',
					dataType: 'json',
					// contentType: 'application/json charset=utf-8',
					type: 'POST',
					data: post,
					success: function (result) {
		            	location.reload();
					}
				});
			}
		});


		$('.sort-for-good').on('click', function (){
			var v = $(this).data('sort');
			var post = {"sort":v};
			// console.log(post);
			$.ajax({
				url: ur +'product/ajax_sort/',
				dataType: 'json',
				// contentType: 'application/json charset=utf-8',
				type: 'POST',
				data: post,
				success: function (result) {
					// console.log(result);
					location.reload();
				}
			});
		});

		$(document).on('click', '.remove-filter', function (){
			var v = $(this).data('name');
			var post = {"remove":v};
			// console.log(post);
			$.ajax({
				url: ur +'product/ajax_remove_filter/',
				dataType: 'json',
				// contentType: 'application/json charset=utf-8',
				type: 'POST',
				data: post,
				success: function (result) {
					// console.log(result);
					if(result.reload == "1"){
						location.reload();
					} else {
						location.href = ur + "shop";
					}
				}
			});
			$(this).parent('li').remove();
		});

	}

	var member_address = function(){
		var id_add = $('.addr_id_a').val();
		// console.log(id_add);
		$('.id_addr').each(function(){
			
			if(id_add == $(this).val()){
				$(this).attr('checked', true);
			}
		});

		$(document).on('click', '.chg_address', function(){
			var id = $('input[name=id_address]:checked').val();
			// console.log(id);
			$.ajax({
                url: ur +'account/get_address/' + id,
                dataType: 'json',
                type: 'POST',
                data: {id: id},
                success: function (result) {
                	// console.log(result.member_username);
                	$('.user').text(result.addr_receiver);
                	$('.address').text(result.addr_address);
                	$('.city').text(result.city);
                	$('.province').text(result.province);
                	$('.addr_id').val(result.addr_id);
                	$('.city_id').val(result.city_id);
                	$('.postalcode').text(result.addr_postal_code);
                	$('.district').text(result.addr_district);
                	$('.subdistrict').text(result.addr_subdistrict);
                	$('input[name=order_address]').val(result.addr_address);
                	$('input[name=order_city_id]').val(result.city_id);
                	$('input[name=order_province_id]').val(result.province_id);
                	$('input[name=delivery_province]').val(result.province);
                	$('input[name=order_recipient]').val(result.addr_receiver);
                	$('input[name=order_postalcode]').val(result.addr_postal_code);
                	$('#changeaddress-modal').modal('hide');

                	var cd = result.city_id;
                	var we = $('.weight_product').val();
                	var ind = $(this).data("index");
                	// console.log(cd);
                	// console.log(we);

                	$.ajax({
		            url: ur + "checkout/get_cost/"+cd+"/"+we,
		            type: "GET",
		            contentType: "application/json; charset=utf-8",
		            dataType: 'json',
		            success: function (result){
		                // console.log(result);
		                $('.loading-jne').show();
		                var inp = "";
		                if (result[0].costs.length === 0) {
		                    inp = "Data tidak tersedia."
		                }else{
		                    for (var i = 0; i <= result[0].costs.length - 1; i++) {
		                        inp += "<tr><td><div class='radio'><label><input type='radio' class ='jne_type' data-index='"+ind+"' value='"+result[0].costs[i].service +" "+ result[0].costs[i].description+"' name='jne_type' data-price='"+result[0].costs[i].cost[0].value+"'></label></div></td><td><label>"+result[0].costs[i].service +" "+ result[0].costs[i].description+"</label></td><td>Rp "+result[0].costs[i].cost[0].value+" ,-</td></tr>";
		                    }

		                }

		                $('.jne-price').html(inp);

		            }
		        });
                }
            });

            
	        // console.log(cd);
	        // var ind = $(this).data("index");
	        // var we = $('.weight_product').val();
	        // console.log(we);
	        // if (we === "" && vd !== 0) {
	        //     $(this).attr('title', 'isi berat dan kota terlebih dahulu');
	        //     $(this).tooltip('show');
	        // }
	        // $.ajax({
	        //     url: ur + "checkout/get_cost/"+vd+"/"+we,
	        //     type: "GET",
	        //     contentType: "application/json; charset=utf-8",
	        //     dataType: 'json',
	        //     success: function (result){
	        //         console.log(result);
	        //         $('.loading-jne').show();
	        //         var inp = "";
	        //         if (result[0].costs.length === 0) {
	        //             inp = "Data tidak tersedia."
	        //         }else{
	        //             for (var i = 0; i <= result[0].costs.length - 1; i++) {
	        //                 // inp += "<div class='col-sm-6'><input class='jne_type' type='radio' data-index='"+ind+"' value='"+result[0].costs[i].service +" "+ result[0].costs[i].description+"' name='jne_type' data-price='"+result[0].costs[i].cost[0].value+"'/> "+result[0].costs[i].service +" "+ result[0].costs[i].description+" <strong>[Rp. "+result[0].costs[i].cost[0].value+" ,-]</strong></div>";
	        //             	inp += "<table><tbody><tr><td><div class='radio'><label><input type='radio' class ='jne_type' data-index='"+ind+"' value='"+result[0].costs[i].service +" "+ result[0].costs[i].description+"' name='jne_type' data-price='"+result[0].costs[i].cost[0].value+"'></label></div></td><td><label>"+result[0].costs[i].service +" "+ result[0].costs[i].description+"</label></td><td>Rp "+result[0].costs[i].cost[0].value+" ,-</td></tr></tbody></table>";
	        //             }

	        //         }

	        //         $('.jne-price-list').html(inp);

	        //     }
	        // });
		});
	}

	var loadmore = function (){
		$(document).on('click', '.load-blog', function (){
			var t = $(this);
			var p = $(this).data('page');
			$(this).removeData('page');
			$.ajax({   
				url: ur+"blog/ajax_appendpage/"+p,
				dataType: 'json',
				type: 'GET',
				contentType: "application/json; charset=utf-8",
				success: function (result){
					if (result.next_page !== 0) {
						t.attr('data-page', result.next_page);
					}else{
						t.addClass('hide');
					}
					t.parents('.sect-bdy').find('ul.list-blog').append(result.html_page);
					// console.log(result);
				},
				error: function (result){
					// console.log(result);
				}
			})
		}); 

		$(document).on('click', '.load-lookbook', function (){
			var t = $(this);
			var p = $(this).data('page');
			$(this).removeData('page');
			$(this).html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');
			$.ajax({
				url: ur+"lookbook/ajax_appendpage/"+p,
				dataType: 'json',
				type: 'GET',
				contentType: "application/json; charset=utf-8",
				success: function (result){
					if (result.next_page !== 0) {
						t.attr('data-page', result.next_page);
						t.html('load more');
					}else{
						t.addClass('hide');
					}
					t.parents('.lookbook').find('ul.list-lookbook').append(result.html_page);
					App.initImage();
					// console.log(result);
				},
				error: function (result){
					// console.log(result);
				}
			})
		});
		$(document).on('click', '.load-wish', function (){
			var t = $(this);
			var p = $(this).data('page');
			$(this).removeData('page');
			$.ajax({
				url: ur+"account/ajax_appendpage/"+p,
				dataType: 'json',
				type: 'GET',
				contentType: "application/json; charset=utf-8",
				success: function (result){
					if (result.next_page !== 0) {
						t.attr('data-page', result.next_page);
					}else{
						t.addClass('hide');
					}
					t.parents('.wishlist').find('ul.list-wishlist').append(result.html_page);
					App.initImage();
				},
				error: function (result){
					// console.log(result);
				}
			})
		});
		$(document).on('click', '.load-review', function (){
			var t = $(this);
			var p = $(this).data('page');
			var k = $(this).data('key');
			$(this).removeData('page');
			$(this).html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');
			$.ajax({
				url: ur+"product/ajax_appendreview/"+p+"/"+k,
				dataType: 'json',
				type: 'GET',
				contentType: "application/json; charset=utf-8",
				success: function (result){
					if (result.next_pages !== 0) {
						t.attr('data-page', result.next_pages);
					}else{
						t.parent().addClass('hide');
					}
					t.parents('.review').find('ul.list-review').append(result.html_page);
					App.initRating();
					// console.log(result);
				},
				error: function (result){
					// console.log(result);
				}
			})
		});
	}

	var cart = function (){
		function totalcart(){
			var sum = 0;
        	$('.t-price2').each(function (){
				sum = parseFloat(sum) + parseFloat($(this).val());
			})
	       	$('.t-all').text(formatCurrency(sum, "Rp "));
		}
		$(document).on('click', '.qty-p', function (){
    		var div = $(this);
    		var id = div.parents('div.oneitemrow').find('input[name=rowid]').val();
    		var stock = div.parents('div.oneitemrow').find('input[name=stock]').val();
    		var qty = div.parents('div.oneitemrow').find('input[name=qty]').val();
    		var price = div.parents('div.oneitemrow').find('.price-a').val();
    		var varian = div.parents('div.oneitemrow').find('input[name=varian]').val();
    		var q = parseInt(qty) + 1; 
    
    		if(q <= stock){
    			div.parents('div.oneitemrow').find('input[name=qty]').val(q);
    			div.parents('div.oneitemrow').find('.qty-s').text(q);
    			$.ajax({
	                url: ur +'cart/update_cart_ajax',
	                dataType: 'json',
	                type: 'POST',
	                data: {stock: stock, id: id, qty: q, price:price, varian:varian},
	                success: function (result) {
	                	// console.log(result);
	                	div.parents('div.oneitemrow').find('.t-price').text(formatCurrency(result.total, "Rp "));
	                	div.parents('div.oneitemrow').find('.t-price2').val(result.total);
	                	totalcart();
                		appendFree(result.free_product,id);
	                }
            	});
    		}
    	});

    	function appendFree(product,id){
    		var f = product;
    		var c = $('#product_content');
    		$('.cart>ul li[data-id="'+id+'"]').remove();
    		if(f !== null){
    			c.find('li.idprod').attr('data-id',f.product_id);
	    		c.find('.p-item a').attr('href',ur + 'product/detail/' + f.product_permalink);
	    		c.find('.p-image').attr('data-src',ur + f.product_photo);
	    		c.find('p.title').html("<a href='"+ur + 'product/detail/' + f.product_permalink+"'>"+ f.product_name +"</a>");
	    		c.find('.price-discount span.price').text(f.product_price);
	    		c.find('.promo_name').text(f.promo_name);
	    		c.find('input[name="rowid"]').val(f.product_id);
	    		$('.cart>ul').append(c.html());
	    		App.initImage();
	    	}

    	}

    	$('.cart>ul .qty_val').change(function(){
    		var div = $(this);
    		var id = div.parents('div.oneitemrow').find('input[name=rowid]').val();
    		var stock = div.parents('div.oneitemrow').find('input[name=stock]').val();
    		var q = div.parents('div.oneitemrow').find('input[name=qty]').val();
    		var price = div.parents('div.oneitemrow').find('.price-a').val();
			$.ajax({
                url: ur +'cart/update_cart_ajax',
                dataType: 'json',
                type: 'POST',
                data: {stock: stock, id: id, qty: q, price:price},
                success: function (result) {
                	appendFree(result.free_product,id);
                }
        	});
    	}).trigger('change');

    	$(document).on('click', '.qty-m', function (){
    		var div = $(this);
    		var id = div.parents('div.oneitemrow').find('input[name=rowid]').val();
    		var stock = div.parents('div.oneitemrow').find('input[name=stock]').val();
    		var qty = div.parents('div.oneitemrow').find('input[name=qty]').val();
    		var price = div.parents('div.oneitemrow').find('.price-a').val();
    		var varian = div.parents('div.oneitemrow').find('input[name=varian]').val();
    		var q = parseInt(qty) - 1;

    		if(q > 0){
 				div.parents('div.oneitemrow').find('input[name=qty]').val(q);
    			div.parents('div.oneitemrow').find('.qty-s').text(q);
    			$.ajax({
	                url: ur +'cart/update_cart_ajax',
	                dataType: 'json',
	                type: 'POST',
	                data: {stock: stock, id: id, qty: q, price:price, varian:varian},
	                success: function (result) {
	                	div.parents('div.oneitemrow').find('.t-price').text(formatCurrency(result.total, "Rp "));
	                	div.parents('div.oneitemrow').find('.t-price2').val(result.total);
	                	totalcart();
	                	appendFree(result.free_product,id);
	                }
	            });
    		}
    	});

    	$(document).on('click', '.delete', function (){
    		var div = $(this);
    		var id = div.parents('div.oneitemrow').find('input[name=rowid]').val();
    		var varian = div.parents('div.oneitemrow').find('input[name=varian]').val();

    		var crt = $('.cart-header').text();
    		$.ajax({
                url: ur +'cart/delete_cart_ajax',
                dataType: 'json',
                type: 'POST',
                data: {id: id,varian: varian},
                success: function (result) {
                	div.parents('li').remove();
                	$('.cart-header').text(result);
                	totalcart();
            	    // location.reload();
                	// window.location = window.location;
                }
            });
    	});

    	$(document).on('click', '.add_cart', function (){
    		var div = $(this);
    		var id = div.parents('div.p-item').find('input[name=productid]').val();
    		// console.log(id);
    		var crt = $('.cart-header').text();
    		$.ajax({
                url: ur +'cart/add_to_cart_ajax/' + id,
                dataType: 'json',
                type: 'POST',
                data: {id: id},
                success: function (result) {
                	// console.log(result);
                	$('.cart-header').text(result);
                }
            });
    	});

    	$(document).on('click', '.add_cart_detail', function (){
    		var div = $(this);
    		var id = div.parents('div.p-item').find('input[name=productid]').val();
    		var crt = $('.cart-header').text();
    		$.ajax({
                url: ur +'product/add_to_cart_ajax/' + id,
                dataType: 'json',
                type: 'POST',
                data: {id: id},
                success: function (result) {
                	// console.log(result);
                	$('.cart-header').text(result);
                }
            });
    	});

    	$(document).on('click', '.add_wish', function (){
    		var div = $(this);
    		var id = div.parents('div.p-item').find('input[name=productid]').val();
    		var wsh = $('.wish-header').text();
    		$.ajax({
                url: ur +'account/add_to_wishlist_ajax/' + id,
                dataType: 'json',
                type: 'POST',
                data: {id: id},
                success: function (result) {
                	$('.wish-header').text(result);
                	div.html('<i class="fa fa-times-circle"></i>&nbsp; REMOVE FROM WISHLIST');
                	div.addClass('remove_wish');
                	div.removeClass('add_wish');
                	toastr.options = {
                            "preventDuplicates": true,
                            "positionClass": "toast-top-right"
                        }
                    toastr["success"]("Product has been added to your wishlist.");
                }
            });
    	});

    	$(document).on('click', '.remove_wish', function (){
    		var div = $(this);
    		var id = div.parents('div.p-item').find('input[name=productid]').val();
    		var wsh = $('.wish-header').text();
    		$.ajax({
                url: ur +'account/remove_from_wishlist_ajax/' + id,
                dataType: 'json',
                type: 'POST',
                data: {id: id},
                success: function (result) {
                	$('.wish-header').text(result);
                	div.html('<i class="fa fa-heart"></i>&nbsp; ADD TO WISHLIST');
                	div.addClass('add_wish');
                	div.removeClass('remove_wish');
                	toastr.options = {
                            "preventDuplicates": true,
                            "positionClass": "toast-top-right"
                        }
                    toastr["success"]("Product has been removed from your wishlist.");
                }
            });
    	});

    	$(document).on('change', '.cityselect', function (){
	        var th = $(this);
	        var ind = $(this).data("index");
	        var vd = $('select.cityselect').val();
	        var we = $('.weight_product').val();
	        $('.empty-shipping-type').addClass('hide');
	        $('.loading-jne').removeClass('hide');
	        if (we === "" && vd !== 0) {
	            // $(this).attr('title', 'isi berat dan kota terlebih dahulu');
	            // $(this).tooltip('show');
	        }
	        $.ajax({
	            url: ur + "checkout/get_cost/"+vd+"/"+we,
	            type: "GET",
	            contentType: "application/json; charset=utf-8",
	            dataType: 'json',
	            success: function (result){
	            	$('.loading-jne').addClass('hide');
	                var inp = "";
	                if (result[0].costs.length === 0) {
	                    inp = "Data tidak tersedia."
	                }else{
	                    for (var i = 0; i <= result[0].costs.length - 1; i++) {
	                        // inp += "<div class='col-sm-6'><input class='jne_type' type='radio' data-index='"+ind+"' value='"+result[0].costs[i].service +" "+ result[0].costs[i].description+"' name='jne_type' data-price='"+result[0].costs[i].cost[0].value+"'/> "+result[0].costs[i].service +" "+ result[0].costs[i].description+" <strong>[Rp. "+result[0].costs[i].cost[0].value+" ,-]</strong></div>";
	                    	inp += "<tr><td><div class='radio'><label for='c"+i+"'><input type='radio' class ='jne_type' id='c"+i+"' value='"+result[0].costs[i].service +" "+ result[0].costs[i].description+"' name='jne_type' data-price='"+result[0].costs[i].cost[0].value+"'></label></div></td><td><label for='c"+i+"'>"+result[0].costs[i].service +" "+ result[0].costs[i].description+"</label></td><td>"+formatCurrency(result[0].costs[i].cost[0].value, "Rp ")+"</td></tr>";
	                    }

	                }

	                $('.jne-price-list table tbody').html(inp);
	                $('.empty-shipping-type').addClass('hide');
	            },
	            error: function (){
	            	$('.loading-jne').addClass('hide');
	            	$('.empty-shipping-type').removeClass('hide');
	            }
	        });
	    });

	    $(document).on('change', '.city_id', function (){
	        // var th = $(this);
	        // console.log('sd');
	        // var ind = $(this).data("index");
	        // var vd = $(this).val();
	        // var we = $('.weight_product').val();
	        // console.log(vd);
	        // console.log(we);
	        // if (we === "" && vd !== 0) {
	        //     $(this).attr('title', 'isi berat dan kota terlebih dahulu');
	        //     $(this).tooltip('show');
	        // }
	        // $.ajax({
	        //     url: ur + "checkout/get_cost/"+vd+"/"+we,
	        //     type: "GET",
	        //     contentType: "application/json; charset=utf-8",
	        //     dataType: 'json',
	        //     success: function (result){
	        //         console.log(result);
	        //         $('.loading-jne').show();
	        //         var inp = "";
	        //         if (result[0].costs.length === 0) {
	        //             inp = "Data tidak tersedia."
	        //         }else{
	        //             for (var i = 0; i <= result[0].costs.length - 1; i++) {
	        //                 // inp += "<div class='col-sm-6'><input class='jne_type' type='radio' data-index='"+ind+"' value='"+result[0].costs[i].service +" "+ result[0].costs[i].description+"' name='jne_type' data-price='"+result[0].costs[i].cost[0].value+"'/> "+result[0].costs[i].service +" "+ result[0].costs[i].description+" <strong>[Rp. "+result[0].costs[i].cost[0].value+" ,-]</strong></div>";
	        //             	inp += "<table><tbody><tr><td><div class='radio'><label><input type='radio' class ='jne_type' data-index='"+ind+"' value='"+result[0].costs[i].service +" "+ result[0].costs[i].description+"' name='jne_type' data-price='"+result[0].costs[i].cost[0].value+"'></label></div></td><td><label>"+result[0].costs[i].service +" "+ result[0].costs[i].description+"</label></td><td>Rp "+result[0].costs[i].cost[0].value+" ,-</td></tr></tbody></table>";
	        //             }

	        //         }

	        //         $('.jne-price-list').html(inp);

	        //     }
	        // });
	    });

	    $(document).on('click', '.jne_type', function (event){
	    	var harga = $(this).data('price');
	    	$(".total_shipping").text(formatCurrency(harga, "Rp ")).trigger("change");
	    	
            // var ind = $(this).data('index');
            var code = $('.uniq').text();
            var total = parseInt(total_prod) + parseInt($(this).data('price')) + parseInt(code);
            $("input[name=order_unique_code]").val(code).trigger("change");
            // $(".total_shipping").text(formatCurrency($(this).data('price'),"Rp. ")).trigger("change");

            $("input[name=order_shipping_fee]").val($(this).data('price'));
            $(".total_bill").text(formatCurrency(total,"Rp ")).trigger("change");
            $("input[name=order_total_price]").val(total).trigger("change");
            $('.proceed').prop('disabled',false);
        });


		$(document).on('focusout', '.redeem', function(e){
			if (e.keyCode == 13) {               
			    e.preventDefault();
			    return false;
			}else{
				var code = $(this).val();
				// console.log(code);
				var total = $('input[name="order_total_price"]').val();
				// var total = parseInt(total_price) + 
				$.ajax({
					url: ur +'checkout/redeem_voucher/',
					dataType: 'json',
					// contentType: 'application/json charset=utf-8',
					type: 'POST',
					data: {'code':code,'total':total},
					success: function (result) {
						// console.log(result.status);
						if(result.status){
			            	$('.redeem_label').addClass('hide');
			            	$('.redeem_value').html(formatCurrency(result.value,"Rp "));
			            	$('input[name=order_promo]').val(result.value);
                    		if(result.percentage > 0){
				            	$('.redeem_label').html(result.percentage + "%");
				            	$('.redeem_label').removeClass('hide');
				            }
				            $('.total_bill').html(formatCurrency(total-result.value,"Rp. "));
				             $("input[name=order_total_price]").val(total-result.value);
                    		toastr["success"]("Your voucher is successfully redeem.");
                    		$('#banktr').focus();
						} else {
                    		toastr["error"]("Sorry, your voucher cannot be used.");
			            }
					}
				});
			}
		});
	}

	

	var preview = function (){
	    $('.preview_image').on('change', function (){
	      var ttl = $(this)[0].files.length;
	      var t = $(this);
	      for (var i = 0; i <= ttl-1; i++) {
	        t.parent().find('p.preview').html("<img src='"+URL.createObjectURL(event.target.files[i])+"' />")
	      }
	    });

	    $('.preview_image_multiple').on('change', function (){
	      var ttl = $(this)[0].files.length;
	      var t = $(this);
	      var im = '';
	      for (var i = 0; i <= ttl-1; i++) {
	        im += "<img src='"+URL.createObjectURL(event.target.files[i])+"' />";
	      }
	      t.parent().find('p.preview').html(im);
	    })
	}


	var productdetail = function (){
		$(document).on('click', '.q-plus', function (){
    		var div = $(this);
    		var id = $('input[name=productid]').val();
    		var stock = $('input[name=stock]').val();
    		var qty = $('input[name=qty]').val();
    		var q = parseInt(qty) + 1; 
    
    		if(q <= stock){
    			$('input[name=qty]').val(q);
    		}
    	});

    	$(document).on('click', '.q-min', function (){
    		var div = $(this);
    		var id = $('input[name=productid]').val();
    		var stock = $('input[name=stock]').val();
    		var qty = $('input[name=qty]').val();
    		var q = parseInt(qty) - 1; 
    
    		if(q > 0){
    			$('input[name=qty]').val(q);
    		}
    	});

    	$(document).on('click', '.addcart', function (){
    		var id = $('input[name=productid]').val();
    		var url = ur +'cart/add_to_cart_ajax/' + id;
    		console.log(ur);
    		// var qty = $('input[name=qty]').val();
    		// var varian = $('input[name=varian_name]').val();
    		// var arr = $('.var');
    		// console.log(varian);
    		// var var_info = [];
    		// var mix;
 			// arr.each(function (){
 			// 	mix = $(this).val();
 			// 	// console.log(mix);
 			// 	mix += ": " + $("select[name="+mix+"]").val();
 			// 	var_info.push(mix);
 			// });

    		var cart = $('.cart-header').text();
    		$.ajax({
                url: ur +'cart/add_to_cart_ajax/' + id,
                dataType: 'json',
                type: 'POST',
                data: {id: id},
                success: function (result) {
                	console.log(result);
                	$('.cart-header').text(result.total);
                	toastr.options = {
                            "preventDuplicates": true,
                            "positionClass": "toast-top-right"
                        }
                    if(result.total !== '1'){
                    	toastr["success"]("Product has been added to your cart. Let's shop again.");
                    	// location.reload();
                    	// $('#cart-modal').find('.body-cart').html(result.html_page);
                    	// $('#cart-modal').modal('show');
                    	App.initImage();
                    	// t.parents('.lookbook').find('ul.list-lookbook').append(result.html_page);
                    }else{
                    	toastr["error"]("Out of stock");
                    }
                }
            });
    	});


    	$(document).on('click', '.addwish', function (){
    		var div = $(this);
    		var id = $('input[name=productid]').val();
    		var wsh = $('.wish-header').text();
    		var ico = $('.icon-wish').text();
    		// console.log(ico);
    		$.ajax({
                url: ur +'account/add_to_wishlist_ajax/' + id,
                dataType: 'json',
                type: 'POST',
                data: {id: id},
                success: function (result) {
                	// console.log(result);
                	$('.wish-header').text(result);
                	// console.log(ico);
                	if(ico == 'Add To Wishlist'){
                		$('.icon-wish').text('Remove From List');
                	}else{
                		$('.icon-wish').text('Add To Wishlist');
                	}
                	
                }
            });
    	});

    	//stock on product detail
	    $('.show_stock').on('change', function(){
	      var varian = $('.show_stock');
	      var product_id = $(this).data('product_id');
	      console.log(varian);
	      // console.log(product_id);
	      var varian_name = "";
	      var i = 0;
	      varian.each(function(){
	        if(i > 0){
	          varian_name += "-";
	        }
	        varian_name += $(this).val();
	        i++;
	      });
	      
	      inp = "<input type='hidden' name='varian_name' value='"+ varian_name +"'>";
	      $('.varian_product').html(inp);
	      // console.log(varian);
	      $.ajax({
	          url: ur + 'product/ajax_get_stock',
	          dataType: 'json',
	          type: 'POST',
	          data: {varian: varian_name, product_id: product_id},
	          success: function (result) {
	              var status = "Out of Stock";
	              if(result.stock > 1){
	                status = "Item Available";
	              }else if(result.stock == 1){
	                status = "Only 1 More";
	              }
	              $('#stock_display').html(status);
	              if(result.product_discount == null){
	              	$('#final_price').html(result.product_price);
	              	$('#discount').html("");
	              	$('#price_discount').html("");
	              }else{
		              $('#price_discount').html(result.product_price);
		              $('#discount').html(result.product_discount);
		              if(result.product_discount > 0){
		              	$('#discount').addClass('label label-default');
		              }
		              $('#final_price').html(result.price);
		          }
	          }
	      });
	    }).trigger('change');

	    $('.readmore').on('click', function (e) {
	        e.preventDefault();
	        this.expand = !this.expand;
	        $(this).text(this.expand?"Less Info":"More Info");
	        $(this).closest('.description-detail').find('.short-detail, .long-detail').toggleClass('short-detail long-detail');
	    });

	}

	var rating = function (){
		$('.p-rating').each(function (){
			var currentRating = $(this).data('current-rating');
			$(this).barrating({
	            theme: 'fontawesome-stars-o',
	            showSelectedRating: false,
	            initialRating: currentRating,
	            deselectable: false,
	            hoverState: false, 
	            readonly: true
	        });
		});
		$('.detail-rating').each(function (){
			var currentRating = $(this).data('current-rating');
			$(this).barrating({
	            theme: 'fontawesome-stars-o',
	            showSelectedRating: true,
	            initialRating: currentRating,
	            deselectable: false,
	            hoverState: false,
	            readonly: true
	        });
		});
	}

	var review = function (){
		$('.insert-rating').each(function (){
			var currentRating = $(this).data('current-rating');
			$(this).barrating({
	            theme: 'fontawesome-stars-o',
	            showSelectedRating: true,
	            initialRating: currentRating,
	            deselectable: false,
	            hoverState: false,
	            readonly: false
	        });
		});
		$(document).on('click', '.toggle-review', function (){
			var t = $('.write-review-form');
			var h = $(this);
			if (t.hasClass('hide')) {
				t.removeClass('hide');
				h.html('<i class="fa fa-times"></i>');
			}else{
				t.addClass('hide');
				h.html('<i class="fa fa-pencil"></i> write review');
			}
		});
	}

	var user = function (){
		
	}

	var productimage = function (){
		$('.big-image .carousel-inner .item a').on('click', function(){
			lightbox($(this).data('sld'));
			// console.log($(this).data('sld'));
		});
		function lightbox(idx) {
		    //show the slider's wrapper: this is required when the transitionType has been set to "slide" in the ninja-slider.js
		    var ninjaSldr = document.getElementById("new-slider");
		    ninjaSldr.parentNode.style.display = "block";

		    nslider.init(idx);

		    var fsBtn = document.getElementById("fsBtn");
		    fsBtn.click();
		}

		function fsIconClick(isFullscreen) { //fsIconClick is the default event handler of the fullscreen button
		    if (isFullscreen) {
		        var ninjaSldr = document.getElementById("new-slider");
		        ninjaSldr.parentNode.style.display = "none";
		    }
		}
	}

	var paymth = function (){
		$('input[name="pm"]').on('change', function (){
			if ($(this).is(':checked')) {
				tgglactv();
			}	
		});
		tgglactv = function (val){
			$('input[name="pm"]').each(function (){
				if ($(this).is(':checked')) {
					$(this).parents('tr').addClass('active');
					$(this).parents('tr').find('.toggle-showup').show();
				}else{
					$(this).parents('tr').removeClass('active');
					$(this).parents('tr').find('.toggle-showup').hide();
				}
			});
		}
	}

	var menu = function (){
		$(document).on('click', '.navbar-toggle', function (){
		  $(this).toggleClass('open');
		});

		function DropDown(el) {
			this.dd = el;
			this.initEvents();
		}
		DropDown.prototype = {
			initEvents : function() {
				var obj = this;

				obj.dd.on('click', function(event){
					$(this).toggleClass('active');
					event.stopPropagation();
				});	
			}
		}

		$(function() {
			var dd = new DropDown( $('.dd') );
			$(document).click(function() {
				// all dropdowns
				$('.wrapper-dropdown-2').removeClass('active');
			});

		});
	}

	return {
		init: function (){
			menu();
			socket();
			// loadimage();
			// loadproduct();
			rating();
			// paymth();
			productimage();
			cart();
			shop();
			promo();
			hidenotif();
			productdetail();
			member_address();
			datepicker();
			loadmore();
			review();
			address();
			select();
			preview();
			validate();
		},
		initAjax: function (){
			loadimage();
			loadproduct();
			rating();
		},
		initImage:function (){
			loadimage();
		},
		initRating:function (){
			rating();
		}
	}
}();


function fsIconClick(isFullscreen) { //fsIconClick is the default event handler of the fullscreen button
    if (isFullscreen) {
        var ninjaSldr = document.getElementById("new-slider");
        ninjaSldr.parentNode.style.display = "none";
    }
}