var Template = function (){
  var baseUrl = $('.srd').data('srd');
  var base = $('.srd').data('srs');
  $('.refresh-page').click(function (){
    location.reload();
  });
  $('.user-loading a').click(function (){
    location.reload();
  });
  
  $('body').addClass(sessionStorage.getItem('Sidebar'));

  $(document).on('click', '.sidebar-toggle', function (){
    if (sessionStorage.getItem('Sidebar') === 'sidebar-collapse') {
      sessionStorage.setItem('Sidebar', '');
    }else{
      sessionStorage.setItem('Sidebar', 'sidebar-collapse');
    }
  });
   // $('[data-toggle="modal"]').modal();

  $(window).on('beforeunload', function () {
      $('.loading-icon').addClass('fa-spin');
  });

  var notif = function (){
    var config = {
        apiKey: "AIzaSyBuD9On_GYbp_mpxbEPF6T3HrpjToLT2U4",
        authDomain: "zeeals-9f9bf.firebaseapp.com",
        databaseURL: "https://zeeals-9f9bf.firebaseio.com",
        projectId: "zeeals-9f9bf",
        storageBucket: "zeeals-9f9bf.appspot.com",
        messagingSenderId: "63533152624"
      };
    firebase.initializeApp(config);

    const messaging = firebase.messaging();
    
    getPermission();

    messaging.onTokenRefresh(function() {
      messaging.getToken()
      .then(function(refreshedToken) {
        console.log(refreshedToken);
        console.log('Token refreshed.');
        // Indicate that the new Instance ID token has not yet been sent to the
        // app server.
        // Send Instance ID token to app server.
        // [START_EXCLUDE]
        // Display new Instance ID token and clear UI of all previous messages.
        // [END_EXCLUDE]
      })
      .catch(function(err) {
        console.log('Unable to retrieve refreshed token ', err);
      });
    });

    getOToken();

    

    messaging.onMessage(function(payload) {
      console.log("Message received. ", payload);
      // [START_EXCLUDE]
      // Update the UI to include the received message.      
      // [END_EXCLUDE]
    });

    function getPermission(){
      messaging.requestPermission()
      .then(function() {
        console.log('Notification permission granted.');
        // TODO(developer): Retrieve an Instance ID token for use with FCM.
        // [START_EXCLUDE]
        // In many cases once an app has been granted notification permission, it
        // should update its UI reflecting this.
        // [END_EXCLUDE]
      })
      .catch(function(err) {
        console.log('Unable to get permission to notify.', err);
      });
    }

    function getOToken(){
      messaging.getToken().then(function (token){
        console.log(token);
      }).catch(function (err){
        console.log(err);
      });
    }
  }
  
  var datepicker = function (){
    $(".datepicker").datepicker({
      showInputs: false,
      format:'yyyy-mm-dd',
      autoclose: true,
      todayHighlight: true
    });
  }

  var brandidentity = function (){
    
  }

  var copyfrom = function (){
    $('.copyfrom').on('click', function (){
      to = $(this).data('to');
      from = $(this).data('from');
      v = $(this).parents('form').find('input[name="'+from+'"]').val();
      vt = $(this).parents('form').find('textarea[name="'+from+'"]').val();
      $(this).closest('.form-group').find('input[name="'+to+'"]').val(v);
      $(this).closest('.form-group').find('textarea[name="'+to+'"]').val(vt);
    });
  }

  var select = function () {
      $('.select2').select2({
          minimumResultsForSearch: -1
      });
      $('.select2me').select2({
      });
  }

  var chart = function (){
    if ($('div').hasClass('chart-am')) {
        var grab = [];
        var temp = {}
        $('div.data-chart').each(function (i) {
            temp['name'] = $(this).find('ul').data('name');
            temp['type'] = $(this).find('ul').data('type');
            temp['title'] = $(this).find('ul').data('title');
            temp['data'] = [];
            $(this).find('ul li').each(function (j) {
                temp['data'][j] = $(this).data();
            });
            grab.push(temp);
            temp = {};
        });
        for (var i = 0; i <= grab.length - 1; i++){
          if (grab[i].type === 'serial') {
              var chart = [];
              var datasetchart = [];
              var serialFinalChart = [];
              // for (var i = 0; i <= grab.length - 1; i++) {
              chart[i] = grab[i].name;
              serialFinalChart[i] = AmCharts.makeChart(chart[i], {
                  "type": "serial",
                  "theme": "light",
                  "legend": {
                      "enabled": false
                  },
                  "dataProvider": grab[i].data,
                  "valueAxes": [{
                      "axisAlpha": 0.3,
                      "gridAlpha": 0,
                      "autoGridCount":false,
                      "gridCount":10
                  }],
                  "startDuration": 1,
                  "graphs": [{
                          "balloonText": "<b>[[value]]</b> Order",
                          "fillAlphas": 0.8,
                          "lineAlpha": 0.3,
                          "title": "Orders",
                          "type": "column",
                          "color": "#000",
                          "valueField": "amnt"
                      }],
                  "categoryField": "label",
                  "categoryAxis": {
                      "gridPosition": "start",
                      "axisAlpha": 0,
                      "tickLength": 0
                  },
                  "responsive":{
                      "enabled": true
                  },
                  "export":{
                      "enabled":true,
                      "menu":[]
                  }
              });
              // }
          } 
        }
    }
  }

  var datatable = function (){
    $('.table_data_pe').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": true
    });
  }

  var readyload = function (){
    $('ul.sidebar-menu').find('li ul.treeview-menu').find('li.active').parents('li.treeview').addClass('active');
    $('.group-checkable').change(function () {
        var set = $(this).attr("data-set");
        var checked = $(this).is(":checked");
        $(set).each(function () {
            if (checked) {
                $(this).prop("checked", true);
                $('.checkboxes').trigger('change');
            } else {
                $(this).prop("checked", false);
                $('.checkboxes').trigger('change');
            }
            $(this).parents('tr').toggleClass("active");
        });
        // $.uniform.update(set);
    });
    $('.preorder').click(function(){
        var po = $('input[name="product_ispreorder"]:checked').val();
        if(po == "1"){
            $('#processTime').show();
        } else {
            $('#processTime').hide();
        }
    });
    $("a#add_picture_comment").on('click', function(){   
      var input = '<div id="div2" class="col-sm-4">' + 
                  '<div class="box">' +
                    '<center>' + '<div class="form-group col-sm-12 <?php echo (form_error(\'prodphoto_path\') != "") ? "has-error" : "" ?>">' +
                    '<input type="file" class="form-control " name="prodphoto_path[]" placeholder="Prodphoto Path"  required  maxlength=255 multiple data-toggle="tooltip" data-placement="right" title="Photo">' + '<?php echo form_error(\'prodphoto_path\'); ?>' + '</div>' + '<div class="form-group col-sm-12 <?php echo (form_error(\'prodphoto_description\') != "") ? "has-error" : "" ?>">'+ '<input class="form-control" name="prodphoto_description[]" data-toggle="tooltip" data-placement="right" title="Description"><?php echo set_value(\'prodphoto_description\', $product_photo->prodphoto_description); ?>' + '<?php echo form_error(\'prodphoto_description\'); ?>' + '</div>' +
                    '</center>' + 
                    '<div class="box-footer text-center">' + 
                      '<a id="remove_picture_comment" href="javascript:;" class="btn default fileinput-exists btn-circle" data-dismiss="fileinput" data-toggle="tooltip" data-placement="bottom" title="Remove"><span class="fa fa-remove"></span></a>' + 
                      '<input type="radio" id="isprimary" name="prodphoto_isprimary[]" <?php //echo set_radio(\'prodphoto_isprimary\',\'1\'); ?> data-toggle="tooltip" data-placement="bottom" title="Primary Photo">' +   
                    '</div>' + 
                    '</div>' + 
                '</div>';
                function myFunction() {
                  var x = document.getElementById("isprimary").checked;
                    var i = 0; 
                  for (var i=0; i<prodphoto_isprimary.length; i++){
                    
                    if(x[i] != false){
                      'value = "1"<?php echo set_radio(\'prodphoto_isprimary\',\'1\', TRUE); ?>';
                    }
                    else{
                      'value = "0" <?php echo set_radio(\'prodphoto_isprimary\',\'0\') ; ?>"';
                    }
                  }
                }
              $(this).parents('table').append('<div>'+input+'</div>');
    });
    $("a#remove_picture").on('click', function(){
      $(this).parents("#pic").remove();
    });
    
    $('[data-toggle="tooltip"]').tooltip();  

    //PROMOTION
    $('#promo_type').change(function(){
      var val = $(this).val();
      if(val == '1'){
        $('.select_product').css('display','block');
        $('.promo_product').prop("required",true);
        $('.promo_discounttype').prop("required",true);
        $('.offer_product_promotion').css('display','block');
        $('.offer_promotion_code').css('display','none');
        $('.min-transaction').css('display','none');
        $('.amount-discount').css('display','none');
        $('.discount').css('display','none');
        $('.generate-code').css('display','none');
      }else if(val == '2'){
        $('.select_product').css('display','none');
        $('.offer_product_promotion').css('display','none');
        $('.offer_promotion_code').css('display','block');
        $('.promo_discounttype2').prop('required',true);
        $('.min-qty').css('display','none');
        $('.free-product').css('display','none');
        $('.discount').css('display','none');
        $('.promo-end').css('display','block');
      }
    }).trigger('change');
    $('.offer').change(function(){
      var val = $(this).val();
      if(val == '5'){
        $('.discount').css('display','block');
        $('.promo_discount').prop('required',true);
        $('.min-qty').css('display','none');
        $('.free-product').css('display','none');
        $('.promo-end').css('display','none');
        $('.promo_end').prop('required',false);
      }else if(val == '3'){
        $('.discount').css('display','none');
        $('.min-qty').css('display','block');
        $('.promo_min_qty').prop('required',true);
        $('.promo_product_id').prop('required',true);
        $('.free-product').css('display','block');
        $('.promo-end').css('display','block');
        $('.generate-code').css('display','none');
      }else if(val == '4'){
        $('.min-transaction').css('display','block');
        $('.min-qty').css('display','none');
        $('.free-product').css('display','none');
        $('.discount').css('display','none');
        $('.promo-end').css('display','block');
        $('.amount-discount').css('display','none');
        $('.generate-code').css('display','none');
        $('.promo_min_transaction').prop('required',true);
      }else if(val == '2'){
        $('.generate-code').css('display','block');
        $('.amount-discount').css('display','block');
        $('.discount').css('display','none');
        $('.min-transaction').css('display','block');
        $('.promo_code').prop('required',true);
        $('.promo_amount').prop('required',true);
        $('.promo_min_transaction').prop('required',true);
      }else if(val == '1'){
        $('.generate-code').css('display','block');
        $('.discount').css('display','block');
        $('.amount-discount').css('display','none');
        $('.min-transaction').css('display','block');
        $('.promo_code').prop('required',true);
        $('.promo_discount').prop('required', true);
        $('.promo_min_transaction').prop('required',true);
      }
    }).trigger('change');

    
    // $('#discount_type').change(function(){
    //   var val = $(this).val();
    //   if(val == '1' || val == '2'){
    //       $('.get_qty').hide();
    //       $('.free_product').hide();
    //       $('.min_qty').show();
    //       $('.discount').show();
    //   } else if(val == '3'){
    //       $('.discount').hide();
    //       $('.free_product').hide();
    //       $('.min_qty').show();
    //       $('.get_qty').show();
    //   } else if(val == '4'){
    //       $('.min_qty').hide();
    //       $('.discount').hide();
    //       $('.get_qty').hide();
    //       $('.free_product').hide();
    //   } else if(val == '5'){
    //       $('.discount').hide();
    //       $('.get_qty').hide();
    //       $('.min_qty').show();
    //       $('.free_product').show();
    //   } else {
    //       $('.min_qty').hide();
    //       $('.discount').hide();
    //       $('.get_qty').hide();
    //       $('.free_product').hide();
    //   }
    // }).trigger('change');

    //BANNER
    // $('input[name="logo_shape"]').change(function(){
    //   var val = $(this).val();
    //   console.log(val);
    //   if(val == '1'){
    //     $('.banner_link_collect').show();
    //     $('.banner_link_promo').hide();
    //     $('.banner_link_prod').hide();
    //     $('.banner_link_costum').hide();
    //   }else if(val == '2'){
    //     $('.banner_link_promo').show();
    //     $('.banner_link_prod').hide();
    //     $('.banner_link_costum').hide();
    //     $('.banner_link_collect').hide();
    //   }else if(val == '3'){
    //     $('.banner_link_prod').show();
    //     $('.banner_link_promo').hide();
    //     $('.banner_link_costum').hide();
    //     $('.banner_link_collect').hide();
    //   }else if(val == '4'){
    //     $('.banner_link_costum').show();
    //     $('.banner_link_promo').hide();
    //     $('.banner_link_prod').hide();
    //     $('.banner_link_collect').hide();
    //   }
    // }).trigger('change');


    
    $('#select_all').click(function(){
      var val = $(this).is(':checked');
      $('.cb').prop('checked',val);
    });
    $('.template_use').on('click', function(){
      var va = $(this).data('title');
      var to = $(this).data('to');
      $(this).closest('.form-group').find('input[name="'+to+'"]').val(va);
    });
    //INFO
    $('.info_store_name').click(function(){
      var val = $(this).data('title');
      $('input[name="info_meta_title"]').val(val);
    });
    $('.info_desc_name').click(function(){
      var val = $(this).data('title');
      $('textarea[name="info_meta_desc"]').val(val);
    });
    $('.product_show').change(function(){
      if($(this).is(':checked')){
        $('.prod_title').show();
        $('.prod_desc').show();
      }else{
        $('.prod_title').hide();
        $('.prod_desc').hide();
      }
    }).trigger('change'); 
    $('.promo_show').change(function(){
      if($(this).is(':checked')){
        $('.prom_title').show();
        $('.prom_desc').show();
      }else{
        $('.prom_title').hide();
        $('.prom_desc').hide();
      }
    }).trigger('change');    
    $('.address_show').change(function(){
      if($(this).is(':checked')){
        $('.addr_title').show();
        $('.addr_desc').show();
        $('.address').show();
      }else{
        $('.addr_title').hide();
        $('.addr_desc').hide();
        $('.address').hide();
      }
    }).trigger('change'); 
    $('.team_show').change(function(){
      if($(this).is(':checked')){
        $('.tim_title').show();
        $('.tim_desc').show();
        $('.team').show();
      }else{
        $('.tim_title').hide();
        $('.tim_desc').hide();
        $('.team').hide();
      }
    }).trigger('change'); 
    $('.blog_show').change(function(){
      if($(this).is(':checked')){
        $('.blo_title').show();
        $('.blo_desc').show();
      }else{
        $('.blo_title').hide();
        $('.blo_desc').hide();
      }
    }).trigger('change'); 
    $('.closing_show').change(function(){
      if($(this).is(':checked')){
        $('.close_title').show();
        $('.close_desc').show();
      }else{
        $('.close_title').hide();
        $('.close_desc').hide();
      }
    }).trigger('change'); 
    //GUIDE
    $('.member_show').change(function(){
      if($(this).is(':checked')){ $('.m_terms').show();}else{$('.m_terms').hide();}
    }).trigger('change'); 
    $('.order_show').change(function(){
      if($(this).is(':checked')){ $('.o_terms').show();}else{$('.o_terms').hide();}
    }).trigger('change');
    $('.payment_show').change(function(){
      if($(this).is(':checked')){ $('.p_terms').show();}else{$('.p_terms').hide();}
    }).trigger('change'); 
    $('.voucher_show').change(function(){
      if($(this).is(':checked')){ $('.v_terms').show();}else{$('.v_terms').hide();}
    }).trigger('change'); 
    $('.promo_show').change(function(){
      if($(this).is(':checked')){ $('.promo_terms').show();}else{$('.promo_terms').hide();}
    }).trigger('change'); 
    $('.delivery_show').change(function(){
      if($(this).is(':checked')){ $('.d_terms').show();}else{$('.d_terms').hide();}
    }).trigger('change'); 
    $('.return_prod_show').change(function(){
      if($(this).is(':checked')){ $('.r_p_terms').show();}else{$('.r_p_terms').hide();}
    }).trigger('change'); 
    $('.reseller_show').change(function(){
      if($(this).is(':checked')){ $('.reseller_terms').show();}else{$('.reseller_terms').hide();}
    }).trigger('change'); 
    $('.faq_show').change(function(){
      if($(this).is(':checked')){ $('.f_terms').show(); $('.faq').show();}else{$('.f_terms').hide(); $('.faq').hide();}
    }).trigger('change'); 
    
    $(".mask_number").keydown(function(event) {
      if ( $.inArray(event.keyCode,[46,8,9,27,13,190]) !== -1 ||
        (event.keyCode == 65 && event.ctrlKey === true) || 
        (event.keyCode >= 35 && event.keyCode <= 39)) {
           return;
      }
      else {
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
          event.preventDefault(); 
        }   
      }
    });
    $(document).on('keydown', ".mask_number", function(event) {
      if ( $.inArray(event.keyCode,[46,8,9,27,13,190]) !== -1 ||
        (event.keyCode == 65 && event.ctrlKey === true) || 
        (event.keyCode >= 35 && event.keyCode <= 39)) {
           return;
      }
      else {
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
          event.preventDefault(); 
        }   
      }
    });

    $(document).on('click', '.generate-code-forgood', function (){
      var res = randomString(9, '#A');
      $('input[name="promo_code"]').val(res);
    });

    function randomString(length, chars) {
        var mask = '';
        if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
        if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if (chars.indexOf('#') > -1) mask += '0123456789';
        if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
        var result = '';
        for (var i = length; i > 0; --i) result += mask[Math.floor(Math.random() * mask.length)];
        return result;
    }
  }

  var order = function (){
    $(document).on('change', '.member_get_address', function (){
      var se = $('select[name="member_id"]').val();
      var ch = $('input[name="set_address"]');
      if (ch.is(':checked') && se !== "") {
        $.ajax({
          url: baseUrl + "order/get_address",
          type: "GET",
          contentType: "application/json; charset=utf-8",
          dataType: 'json',
          success: function (result){

          },
          error: function (result){

          }
        })
      }else{
        return;
      }
    });

    $(document).on('click', '.cost_jne', function (){
        var th = $(this);
        var ind = $(this).data("index");
        var vd = $('select.cityselect').val();
        var we = $('.weight_product').val();
        if (we === "" && vd !== 0) {
            $(this).attr('title', 'isi berat dan kota terlebih dahulu');
            $(this).tooltip('show');
        }
        $.ajax({
            url: baseUrl + "/order/get_cost/"+vd+"/"+we,
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function (result){
                $('.loading-jne').show();
                var inp = "";
                if (result[0].costs.length === 0) {
                    inp = "Data tidak tersedia."
                }else{
                    for (var i = 0; i <= result[0].costs.length - 1; i++) {
                        inp += "<div class='col-sm-6'><input class='jne_type' type='radio' data-index='"+ind+"' value='"+result[0].costs[i].service +" "+ result[0].costs[i].description+"' name='jne_type' data-price='"+result[0].costs[i].cost[0].value+"'/>"+result[0].costs[i].service +" "+ result[0].costs[i].description+" <strong>[Rp. "+result[0].costs[i].cost[0].value+" ,-]</strong></div>";
                    }

                }

                $('.jne-price-list').html(inp);

            }
        });
    });

    $('select[name=product_id]').on('change', function () {
        var id = $(this).val();
        var th = $(this);
        if (id !== null && id !== 'null') {
            $.ajax({
                url: baseUrl + '/product/get_data',
                dataType: 'json',
                type: 'POST',
                data: {id: id},
                success: function (result) {
                    $('select[name=product_id]').select2('open');
                    $('select[name=product_id]').prop('selectedIndex', 0).trigger('change');
                    if (result.available === false) {
                        alert("stok habis!");
                    }else{
                      $('.empty').css('display','none');
                    }
                    
                   var html = "<tr><td><br>"+result.nama+"<input type='hidden' name='product_id[]' class='product_id' value='"+ result.id+"'></td>" + 
                   "<td>";
                   if(result.attr != ''){
                      html +="<p><i>Size/Color</i><select class='form-control varian' required name='attr_value[]'><option value=''>Pilih Varian</option>";
                      for(var i = 0; i < result.attr.length; i++){
                        html +="<option value='"+result.attr[i].attr_varian+"'>"+result.attr[i].attr_varian+"</option>"; 
                      }
                    }
                    html += "</select></p></td>" + 
                   "<td><p><i>Stock:</i><span class='stock'>"+ result.stock + "</span><input type='number' name='order_qty[]' class='form-control qty' min='1' max='"+ result.stock +"'></p></td>" + 
                   "<td><p><i>@Price:</i>Rp.<span class='harga'>"+  result.harga +"</span><input type='hidden' class='harga' value='"+  result.harga +"'><input type='hidden' name='order_prod_price[]' class='prod_price' value='"+ result.harga +"'>" +
                   "<input type='number' name='order_prod_discount[]' class='form-control discount'></p></td>" + 
                   "<td><p><i>Rp.</i><input type='text' name='prod_total[]' class='form-control total' readonly></p></td>" + 
                   "<td><br><a class='btn btn-danger remove_selection'><span class='fa fa-times'></span></a></td></tr>";
                    
                    $('#produk_list tbody').append(html);
                    $('select[name=product_id]').trigger("chosen:updated");

                }
            });
        }
    });

    $(document).on('click','a.remove_selection', function(){
      $(this).parents('tr').remove();
        count_total();
    });


    $(document).on('change', 'select[name="attr_value[]"]', function (){
        var id = $(this).val();
        var th = $(this);
        var product_id = th.closest('tr').find('.product_id').val();
        if (id !== null && id !== 'null') {
            $.ajax({
                url: baseUrl + '/product/get_data_attr',
                dataType: 'json',
                type: 'POST',
                data: {id: id, product_id: product_id},
                success: function (result) {
                    th.closest('tr').find('.stock').text(result.stock);
                    th.closest('tr').find('.harga').text(result.harga);
                    th.closest('tr').find('.harga').val(result.harga);
                    th.closest('tr').find('.discount').val(result.diskon);
                    th.closest('tr').find('.prod_price').val(result.harga);
                    th.closest('tr').find('.order_qty').attr("max",result.stock);
                }
            });
        }
    });

    $(document).on('keyup change', 'input[name="order_qty[]"]', function (){
        var id = $(this).val();
        var th = $(this);
        var harga = th.closest('tr').find('.harga').val();
        if(harga === ''){
          var harga = th.closest('tr').find('.harga').text();
        }
        var diskon = th.closest('tr').find('.discount').val();
        j_diskon = (diskon/100)*harga;
        harga_disc = harga - j_diskon;
        total_bayar = id * harga_disc;
        th.closest('tr').find('.total').val(total_bayar);
        th.closest('tr').find('.total_value').val(total_bayar);
        count_total();
    });

    $(document).on('keyup change', 'input[name="order_prod_discount[]"]', function (){
        var id = $(this).val();
        var th = $(this);
        var harga = th.closest('tr').find('.harga').val();
        if(harga === ''){
          var harga = th.closest('tr').find('.harga').text();
        }
        var qty = th.closest('tr').find('.qty').val();

        j_diskon = (id/100)*harga;
        harga_disc = harga - j_diskon;
        total_bayar = qty * harga_disc;
        th.closest('tr').find('.total').val(total_bayar);
        th.closest('tr').find('.total_value').val(total_bayar);
        count_total();
    });

    $(document).on('click', '.address', function (){
        if($(this).is(':checked')){
            var id = $('select[name="member_id"]').val();
            if(id == ''){
              $('.check_reseller').text("Pilih Reseller Terlebih Dahulu");
            }else{
              $.ajax({
                url: baseUrl + '/reseller/get_data',
                dataType: 'json',
                type: 'POST',
                data: {id: id},
                success: function (result) {
                    $('select[name=order_province_id]').val(result.province_id).trigger('change');
                    info_city = result.city_id;
                    $('input[name=postalcode]').val(result.kodepos);
                    $('textarea[name=address]').val(result.alamat);
                    $('input[name=addr_id]').val(result.id);
                }
            });
            }
        }else{
            $('.check_reseller').text("");
            $('input[name=postalcode]').val("");
            $('textarea[name=address]').val("");
            $('input[name=addr_id]').val("");
            $('select[name=order_province_id]').val("").trigger('change');
            info_city = "";
        }
    }).trigger('click');

    // $(document).on('change', '.reseller', function (){
    //     var id = $(this).val();
    //       $.ajax({
    //         url: baseUrl + '/reseller/get_data',
    //         dataType: 'json',
    //         type: 'POST',
    //         data: {id: id},
    //         success: function (result) {
    //             console.log(result);
    //             $('select[name=member_province]').val(result.province_id).trigger('change');
    //             info_city = result.city_id;
    //             $('input[name=postalcode]').val(result.kodepos);
    //             $('textarea[name=address]').val(result.alamat);
    //             $('input[name=addr_id]').val(result.id);
    //         }
    //     });
    // }).trigger('change');

    $(document).on('change', '.ongkir', function (){
        count_total();
    });

    $(document).on('change', '.order_type', function (){
      var id = $(this).val();
      if(id == 1){
        $('.sec_shipping').css('display','none');
      }else{
        $('.sec_shipping').css('display','block');
        $('.weight_product').attr('data-rule-required','true');
        $('.ongkir').attr('data-rule-required','true');
      }
    }).trigger('change');

    function count_total() {
        var sum = 0;
        var price = $('.total');
        var ongkir = $('.ongkir').val();
        price.each(function () {
            sum += parseFloat(this.value);
        });
        sum2 = sum + parseFloat(ongkir);
        $('#final_total_value').val(sum2);
        $('#final_total').text(toCurrency(sum2));
        $('#prod_total').val(sum);
    }

    function toCurrency(val) {
        var value = parseInt(val);
        var num = 'Rp ' + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        return num;
    }


  }

  var mce = function (){
    tinymce.init({
      selector: '.mce',
      theme: 'modern',
      height: 900,
      setup : function(ed) {
        ed.on("init", (function(ed) {
            ed.target.editorCommands.execCommand("fontName", false, "Source Sans Pro");
        }));
      },
      plugins: [
        'advlist autolink link image lists charmap print preview hr anchor spellchecker',
        'searchreplace wordcount visualblocks code insertdatetime media nonbreaking',
        'table contextmenu directionality template paste textcolor'
      ],
      toolbar: 'undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media'
    });

    tinymce.init({
      selector: '.mce-m',
      theme: 'modern',
      height: 100,
      setup : function(ed) {
        ed.on("init", (function(ed) {
            ed.target.editorCommands.execCommand("fontName", false, "Source Sans Pro");
        }));
      },
      menubar: false,
      plugins: [
        
      ],
      toolbar: 'bold italic underline'
    });

    $(document).on('click', '#category-modal .save-category', function (){
      var $url = $(this).parents('form').attr('action');
      var $megi = $(this).parents('form').find('input[name="cat_blog_name"]').val();
      var $post = {"name": $megi};
      if ($megi !== "") {
        $.ajax({
            url: $url,
            type: 'POST',
            dataType: 'JSON',
            contentType: 'application/json charset=utf-8',
            data: JSON.stringify($post),
            success: function (data) {
              toastr.options = {
                        "preventDuplicates": true,
                        "positionClass": "toast-top-right"
                    }
              toastr["success"]("Category inserted successfully.");
              var $io = '';
              for (var i = 0; i <= data.length - 1; i++) {
                $io += "<option value='"+data[i].cat_blog_id+"'>"+data[i].cat_blog_name+"</option>";
              }

              $('select[name="cat_blog_id"]').html($io);
            },
            error: function (data){
            }
        });
        $('#category-modal').modal('hide');
      }
    });
  }

  var address = function (){
    $('.provinceselect').on('change', function(){
        var th = $(this);
        var dc = $(this).val();
        var prov = $(this).find('option:selected').text();
        if (parseInt(dc) === 0) {
            var inp = "<option value='0'>Select Province First</option>";
            th.closest('form').find('.cityselect').select2({'placeholder':'Select Province First'});
            th.closest('form').find('.cityselect').val("").trigger('change');
            th.closest('form').find('.cityselect').html(inp);
            return;
        }else{
          th.closest('form').find('.delivery_province').val(prov);
            $.ajax({
                url: baseUrl + "/info/get_city/" + dc,
                type: "GET",
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (result){
                    th.closest('form').find('.cityselect').select2({'placeholder':'Select City'});
                    th.closest('form').find('.cityselect').val("").trigger('change');
                    var inp = "<option value='0'>Select City</option>";
                    if (result.length === 0) {
                        return;
                    }
                    for (var i = 0; i <= result.length - 1; i++) {
                        inp += "<option value='"+ result[i].city_id +"'>"+ result[i].type +" "+ result[i].city_name +"</option>"
                    }
                    th.closest('form').find('.cityselect').html(inp);
                    if (info_city !== "null") {
                      th.closest('form').find('.cityselect').val(info_city).trigger('change');
                    }
                    // $('select[name=city_jne] option').each(function(i,j){
                    //     var data = $('#kota').val();
                    //     var is = $('select[name=city_jne]');
                    //     if(data == $(this).text()){
                    //         is.val($(this).val()).trigger('change');
                    //     }
                    // });
                }
            });
        }
    }).trigger('change');

    $('.cityselect').on('change', function(){
        var th = $(this);
        var city = $(this).find('option:selected').text();
        th.closest('form').find('.delivery_city').val(city);
    }).trigger('change');
  }

  var jne = function (){
        $.ajax({
            url: baseUrl + "order/get_province",
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: function (result){
                var inp = "<option value='0'>Pilih</option>";
                if (result.length === 0) {
                    return;
                }
                for (var i = 0; i <= result.length - 1; i++) {
                    inp += "<option value='"+ result[i].province_id +"'>"+ result[i].province  +"</option>"
                }
                $('select[name=province_jne]').html(inp);
                $(document).find('select[name=province_jne]').each(function(i,j){
                    var th = $(this);
                    var data = $(this).closest('.box-body').find('#provinsi').val();
                    $(this).find('option').each(function(){
                        if(data == $(this).text()){
                            $(th).val($(this).val()).trigger('change');
                        }
                    })
                })
            }
        });

        $(document).on('change', 'select[name=province_jne]', function(){
            var th = $(this);
            var dc = $(this).val();
            var prov = $(this).find('option:selected').text();
            if (parseInt(dc) === 0) {
                var inp = "<option value='0'>Masukkan Provinsi</option>";
                th.closest('.content').find('select[name=city_jne]').html(inp);
                return;
            }else{
                th.closest('.box-body').find('#provinsi').val(prov);
                $.ajax({
                    url: baseUrl + "order/get_city/"+dc,
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    success: function (result){
                        var inp = "<option value='0'>Pilih</option>";
                        if (result.length === 0) {
                            return;
                        }
                        for (var i = 0; i <= result.length - 1; i++) {
                            inp += "<option value='"+ result[i].city_id +"'>"+ result[i].type +" "+ result[i].city_name +"</option>"
                        }
                        $('select[name=city_jne]').html(inp);
                        $('select[name=city_jne] option').each(function(i,j){
                            var data = $('#kota').val();
                            var is = $('select[name=city_jne]');
                            if(data == $(this).text()){
                                is.val($(this).val()).trigger('change');
                            }
                        })
                    }
                }); 
            }
        });


        $(document).on('change', 'select[name=city_jne]', function(){
            var th = $(this);
            var dc = $(this).val();
            var ct = $(this).find('option:selected').text();
            if (parseInt(dc) === 0) {
                var inp = "<option value='0'>Masukkan Kode Pos</option>";
                th.closest('.content').find('select[name=addr_postal_code]').html(inp);
                return;
            }else{
                th.closest('.box-body').find('#kota').val(ct);
                $.ajax({
                    url: baseUrl + "order/get_postal/"+dc,
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    dataType: 'json',
                    success: function (result){
                        var inp ="<option value='"+ result.postal_code +"'>"+ result.postal_code +"</option>";
                        $('select[name=addr_postal_code]').html(inp);
                    }
                }); 
            }
        });


        // $(document).on('click', '.cost_jne', function (){
        //     var th = $(this);
        //     var ind = $(this).data("index");
        //     console.log(ind);
        //     var vd = $('select[name=city_id]').val();
        //     var we = $('.weight_product').val();
        //     console.log(vd);
            
        //     if (we === "" && vd != 0) {
        //         $(this).attr('title', 'isi berat dan kota terlebih dahulu');
        //         $(this).tooltip('show');
        //     }
        //     $.ajax({
        //         url: baseUrl + "order/get_cost/"+vd+"/"+we,
        //         type: "GET",
        //         contentType: "application/json; charset=utf-8",
        //         dataType: 'json',
        //         success: function (result){
        //             $('.loading-jne').show();
        //             var inp = "";
        //             if (result[0].costs.length === 0) {
        //                 inp = "Data tidak tersedia."
        //             }else{
        //                 for (var i = 0; i <= result[0].costs.length - 1; i++) {
        //                     inp += "<div class='col-sm-6'><input class='jne_type' type='radio' data-index='"+ind+"' value='"+result[0].costs[i].service +" "+ result[0].costs[i].description+"' name='jne_type["+ind+"]' data-price='"+result[0].costs[i].cost[0].value+"'/>"+result[0].costs[i].service +" "+ result[0].costs[i].description+" <strong>[Rp. "+result[0].costs[i].cost[0].value+" ,-]</strong></div>";
        //                 }
        //             }
        //             $('.jne-price-list').html(inp);
        //         }
        //     });
        // });

        $(document).on('click', '.jne_type', function (event){
            var ind = $(this).data('index');
            $("input[name='delivery_fee["+ind+"]']").val($(this).data('price')).trigger("change");
            $("input[name='delivery_fee']").val($(this).data('price')).trigger("change");
            $("input[name='order_shipping_fee["+ind+"]']").val($(this).data('price')).trigger("change");
            $("input[name='order_shipping_fee']").val($(this).data('price')).trigger("change");
        });
        $(document).on('click', '.total_jne', function (){
            var th = $(this);
            var ind = $(this).data("index");
            var vd = $('input[name="city_jne"]:checked').val();
            var we = $('.weight_jne').val();
            
            if (we === "" && vd != 0) {
                $(this).attr('title', 'isi berat dan kota terlebih dahulu');
                $(this).tooltip('show');
            }
            $.ajax({
                url: baseUrl + "order/get_cost/"+vd+"/"+we,
                type: "GET",
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                success: function (result){
                    $('.loading-jne').show();
                    var inp = "";
                    if (result[0].costs.length === 0) {
                        inp = "Data tidak tersedia."
                    }else{
                        for (var i = 0; i <= result[0].costs.length - 1; i++) {
                            // inp += "<div class='col-sm-6'><input class='jne_type' type='radio' data-index='"+ind+"' value='"+result[0].costs[i].service +" "+ result[0].costs[i].description+"' name='jne_type' data-price='"+result[0].costs[i].cost[0].value+"'required>"+result[0].costs[i].service +" "+ result[0].costs[i].description+" <strong>[Rp. "+result[0].costs[i].cost[0].value+" ,-]</strong></div>";
                        }
                    }
                    $('.jne-price-list').html(inp);
                }
            });
        });
        $(document).on('click', '.jne_type', function (event){
            var ind = $(this).data('index');
            $("input[name='delivery_fee["+ind+"]']").val($(this).data('price')).trigger("change");
            $("input[name='delivery_fee']").val($(this).data('price')).trigger("change");
        });
  }

  var table = function () {
      $('button#deleteall').attr('disabled', true);
      $('button#deleteall').prop('disabled', true);
      if ($("input[type=checkbox]").hasClass('checkboxes')) {
          var cv = $('div.table-responsive .table-hover tbody').find('.checkboxes').length;
          $(document).on('change', '.checkboxes', function () {
              var check = $('div.table-responsive .table-hover tbody').find('.checkboxes:checked').length;
              
              if (check === 0) {
                  $('button#deleteall').attr('disabled', true);
                  $('button#deleteall').prop('disabled', true);
                  $('.slctd-dta').addClass('hide');
              } else {
                  $('button#deleteall').attr('disabled', false);
                  $('button#deleteall').prop('disabled', false);
                  $('.slctd-dta').removeClass('hide').html(check+' Data Selected');
              }
          });
          $('.checkboxes').on('change', function () {
              var check = $('div.table-responsive .table-hover tbody').find('.checkboxes:checked').length;
              
              if (check === 0) {
                  $('button#deleteall').attr('disabled', true);
                  $('button#deleteall').prop('disabled', true);
                  $('.slctd-dta').addClass('hide');
              } else {
                  $('button#deleteall').attr('disabled', false);
                  $('button#deleteall').prop('disabled', false);
                  $('.slctd-dta').removeClass('hide').html(check+' Data Selected');
              }
          });
      }
  }

  var preview = function (){
    $('.preview_image').on('change', function (){
      var ttl = $(this)[0].files.length;
      var t = $(this);
      for (var i = 0; i <= ttl-1; i++) {
        t.parent().find('p.preview').html("<img src='"+URL.createObjectURL(event.target.files[i])+"' />")
      }
    });

    $('.preview_image_multiple').on('change', function (){
      var ttl = $(this)[0].files.length;
      var t = $(this);
      var im = '';
      for (var i = 0; i <= ttl-1; i++) {
        im += "<img src='"+URL.createObjectURL(event.target.files[i])+"' />";
      }
      t.parent().find('p.preview').html(im);
    })
  }

  var product = function (){
    var val = {};
    var arr = [];
    var temp = {};
    $('.generate-price').on('click', function (){
      arr = [];
      var tle = $('#form_var > tr').length;
        if (tle >= 1) {
          $('#form_var > tr').each(function (){
            var t = $(this);
            $(this).find('.btn').addClass('hide');
            $(this).find('input').attr('readonly', true);
            $(this).find('input').prop('readonly', true);
            $('#add_varian').html('Edit Variant').addClass('willeditforgood');
            var v = $(this).find('input[name*="attr_name"]').val();
            if (v !== '') {
              temp['name'] = v;
              temp['opt'] = [];
              t.find('table.list_options > tbody > tr').each(function (){
                var x = $(this).find('input[name*="options"]').val();
                if (x !== '') {
                  temp['opt'].push(x);
                }
              });
              if (temp['opt'].length === 0) {
                temp = {};
                return;
              }
              arr.push(temp);
              temp = {};
            }
          });
        }else{
          $('#add_varian').html('Edit Variant').addClass('willeditforgood');
        }
      if (jQuery.isEmptyObject(arr)) {
        $('.price-nol').removeClass('hide');
        if ($('table.produce_price_list').parents('.form-group').hasClass('hide')) {

        }else{
          $('table.produce_price_list').parents('.form-group').addClass('hide');
        }
        
      }else{
        var tr;
        if ($('.price-nol').hasClass('hide')) {

        }else{
          $('.price-nol').addClass('hide');
        }
        if (arr.length === 1) {
          for(i=0;i<=arr[0].opt.length-1;i++){
            tr += '<tr>'+
              '<td>'+
                '<div class="checkbox">'+
                  '<label><input type="checkbox" name="product_variant_available['+i+']" value="1" checked/></label>'+
                '</div>'+
              '</td>' +
              '<td>'+
                '<ul class="list-unstyled">'+
                  '<li>'+arr[0].opt[i]+'</li>'+
                '</ul>'+
                '<input type="hidden" class="" name="product_variant_name['+i+']" value="'+arr[0].opt[i]+'"/>'+
              '</td>'+
              '<td>'+
                '<input type="text" class="form-control mask_number change-price-well" name="product_variant_price['+i+']" data-name="product_variant_price"/>'+
              '</td>'+
              '<td>'+
                '<input type="text" class="form-control mask_number change-price-well" value="0" name="product_variant_discount['+i+']" data-name="product_variant_discount"/>'+
              '</td>'+
              '<td>'+
                '<input type="text" class="form-control mask_number" readonly name="product_variant_disp_price['+i+']"/>'+
              '</td>'+
              '<td>'+
                '<input type="text" class="form-control mask_number" name="product_variant_stock['+i+']" data-name="product_variant_stock"/>'+
              '</td>'+
            '</tr>';
          }
        }else if(arr.length === 2){
          var cn = 0;
          for(i=0;i<=arr[0].opt.length-1;i++){
            for(j=0;j<=arr[1].opt.length-1;j++){
                tr += '<tr>'+
                  '<td>'+
                    '<div class="checkbox">'+
                      '<label><input type="checkbox" name="product_variant_available['+cn+']" value="1" checked/></label>'+
                    '</div>'+
                  '</td>' +
                  '<td>'+
                    '<ul class="list-unstyled">'+
                      '<li>'+arr[0].opt[i]+'</li>'+
                      '<li>'+arr[1].opt[j]+'</li>'+
                    '</ul>'+
                    '<input type="hidden" class="" name="product_variant_name['+cn+']" value="'+arr[0].opt[i]+'-'+arr[1].opt[j]+'"/>'+
                  '</td>'+
                  '<td>'+
                    '<input type="text" class="form-control mask_number change-price-well" name="product_variant_price['+cn+']" data-name="product_variant_price"/>'+
                  '</td>'+
                  '<td>'+
                    '<input type="text" class="form-control mask_number change-price-well" value="0" name="product_variant_discount['+cn+']" data-name="product_variant_discount"/>'+
                  '</td>'+
                  '<td>'+
                    '<input type="text" class="form-control mask_number" readonly name="product_variant_disp_price['+cn+']"/>'+
                  '</td>'+
                  '<td>'+
                    '<input type="text" class="form-control mask_number" name="product_variant_stock['+cn+']" data-name="product_variant_stock"/>'+
                  '</td>'+
                '</tr>';
                cn++;
            }
          }
        }
        else if(arr.length === 3){
          var cn = 0;
          for(i=0;i<=arr[0].opt.length-1;i++){
            for(j=0;j<=arr[1].opt.length-1;j++){
              for(k=0;k<=arr[2].opt.length-1;k++){
                tr += '<tr>'+
                  '<td>'+
                    '<div class="checkbox">'+
                      '<label><input type="checkbox" name="product_variant_available['+cn+']" value="1" checked/></label>'+
                    '</div>'+
                  '</td>' +
                  '<td>'+
                    '<ul class="list-unstyled">'+
                      '<li>'+arr[0].opt[i]+'</li>'+
                      '<li>'+arr[1].opt[j]+'</li>'+
                      '<li>'+arr[2].opt[k]+'</li>'+
                    '</ul>'+
                    '<input type="hidden" class="" name="product_variant_name['+cn+']" value="'+arr[0].opt[i]+'-'+arr[1].opt[j]+'-'+arr[2].opt[k]+'"/>'+
                  '</td>'+
                  '<td>'+
                    '<input type="text" class="form-control mask_number change-price-well" name="product_variant_price['+cn+']" data-name="product_variant_price"/>'+
                  '</td>'+
                  '<td>'+
                    '<input type="text" class="form-control mask_number change-price-well" value="0" name="product_variant_discount['+cn+']" data-name="product_variant_discount"/>'+
                  '</td>'+
                  '<td>'+
                    '<input type="text" class="form-control mask_number" readonly name="product_variant_disp_price['+cn+']"/>'+
                  '</td>'+
                  '<td>'+
                    '<input type="text" class="form-control mask_number" name="product_variant_stock['+cn+']" data-name="product_variant_stock"/>'+
                  '</td>'+
                '</tr>';
                cn++;
              }
            }
          }
        }
        $('table.produce_price_list').parents('.form-group').removeClass('hide');
        $('table.produce_price_list > tbody').html(tr);
        
      }
      $(this).addClass('hide');
    });

    $(document).on('click','.willeditforgood', function (){
      $(this).removeClass('willeditforgood').html('Add Variant');
      $('#form_var > tr').find('.btn').removeClass('hide');
      $('.generate-price').removeClass('hide');
      $('#form_var > tr').find('input').attr('readonly', false);
      $('#form_var > tr').find('input').prop('readonly', false);
    });

    var fg;
    $(document).on('change', 'table.produce_price_list > tbody > tr:first-child input[name="product_variant_price[0]"]', function(){
      $('#acceptmodal').modal('show')
      fg = $(this);
    });
    // $('#acceptmodal').on('shown.bs.modal', function () {
      $(document).on('click', '#acceptmodal .acceptence', function (){
        if($(this).data('red') === 1){
          fg.closest('tbody').find('input[name*="'+fg.data('name')+'"]').not(fg).val(fg.val()).trigger('keyup')
        }
        fg = '';
        $('#acceptmodal').modal('hide');
      });
    // });
    $(document).on('change', 'table.produce_price_list > tbody > tr:first-child input[name="product_variant_discount[0]"]', function(){
      $('#acceptmodal').modal('show')
      fg = $(this);
    });
    $(document).on('change', 'table.produce_price_list > tbody > tr:first-child input[name="product_variant_stock[0]"]', function(){
      $('#acceptmodal').modal('show')
      fg = $(this);
    });

    $(document).on('keyup', '.change-price-well', function (){
      var p = parseInt($(this).closest('tr').find('input[name*="product_variant_price"]').val());
      var d = parseInt($(this).closest('tr').find('input[name*="product_variant_discount"]').val());
      var h = $(this);
      var t = 0;
      if (p !== NaN && d !== NaN) {
        t = (p - (d/100*p));
        if (!isNaN(t)) {
          h.closest('tr').find('input[name*="product_variant_disp_price"]').val(t);
        }
      }else{
        return;
      }
    });
    $('.change-price-well').on('keyup', function (){
      var p = parseInt($(this).closest('tr').find('input[name*="product_variant_price"]').val());
      var d = parseInt($(this).closest('tr').find('input[name*="product_variant_discount"]').val());
      var h = $(this);
      var t = 0;
      if (p !== NaN && d !== NaN) {
        t = (p - (d/100*p));
        if (!isNaN(t)) {
          h.closest('tr').find('input[name*="product_variant_disp_price"]').val(t);
        }
      }else{
        return;
      }
    });
    $('#add_varian').on('click', function (){
      if ($(this).hasClass('willeditforgood')) {
          return;
      }
      var cf = $("#form_var > tr:last-child").find('input.conter-forgood').data('con');
      var ln = (typeof cf !== "undefined")?(cf+1):1;
      if ($("#form_var > tr").length === 3) {
          return;
      }
      var tr = '<tr>'+
        '<td>'+
            '<input type="text" data-con="'+ln+'"name="attr_name['+ln+']"  class="form-control conter-forgood"  placeholder="Variant Name"/>'+
        '</td>'+
        '<td>'+
            '<table class="option_table" width="100%">'+
                '<tr>'+
                    '<td colspan="7" style="vertical-align: top">'+
                        '<div>'+
                            '<button type="button" class="btn btn-primary btn-flat add_option"><i class="fa fa-plus"></i></button>'+
                        '</div>'+
                    '</td>'+
                    '<td>'+
                        '<table class="list_options options-forgood" data-con="'+ln+'">'+
                            '<tr>'+
                                '<td><p><input type="text" name="options['+ln+'][]" class="form-control"></p></td>'+
                                '<td>'+
                                    '<p><button type="button" onclick="remove_selection(this)" class="btn btn-flat btn-danger"><span class="fa fa-times"></span></button></p>'+
                                '</td>'+
                            '</tr>'+
                        '</table>'+
                    '</td>'+
                '</tr>'+
            '</table>'+
        '</td>'+
        '<td>'+
            '<button type="button" onclick="remove_selection(this)" class="btn btn-flat btn-danger"><span class="fa fa-times"></span></button>'+
        '</td>'+
      '</tr>';
      $('#form_var').append(tr);
    });
    $(document).on("click", ".add_option", function () {
      var ln = $(this).parents('table.option_table').find('.options-forgood').data('con');
      var tr = '<tr>'+
                  '<td><p><input type="text" name="options['+ln+'][]" class="form-control no-empty"></p></td>'+
                  '<td>'+
                      '<p><button type="button" onclick="remove_selection(this)" class="btn btn-flat btn-danger"><span class="fa fa-times"></span></button></p>'+
                  '</td>'+
              '</tr>';
      $(this).parents('table.option_table').find('.options-forgood > tbody').append(tr);
    });
    $(document).on('keyup', '.price-disc-frn', function (){
      var prc = parseInt(($('input[name="product_price"]').val()!=="")?$('input[name="product_price"]').val():0);
      var dsc = parseInt(($('input[name="product_discount"]').val()!=="")?$('input[name="product_discount"]').val():0);
      var tot = (prc - (dsc/100*prc));
      var ms = numeral(tot).format('0,0');
      $('.fix-price strong').html("Rp "+ms+" ,-");
    });
    $(document).on('focusin', 'input[name*="attr_name"]', function (){
      toastr.options = {
                        "preventDuplicates": true,
                        "positionClass": "toast-bottom-right",
                        "timeOut": 0,
                        "extendedTimeOut": 0,
                        "tapToDismiss": false
                    }
      toastr["info"]("<span>You can press <kbd>enter</kbd> to add variant</span>");
    });
    $(document).on('keydown', 'input[name*="attr_name"]', function (e){
      if (e.which === 13) {
        e.preventDefault();
        $('#add_varian').trigger('click');
        $(this).closest('tr').next().find('input[name*="attr_name"]').focus();
      }
    });
    $(document).on('focusout', 'input[name*="attr_name"]', function (){
      toastr.clear();
    });
    $(document).on('focusin', 'input[name*="option"]', function (){
      toastr.options = {
                        "preventDuplicates": true,
                        "positionClass": "toast-bottom-right",
                        "timeOut": 0,
                        "extendedTimeOut": 0,
                        "tapToDismiss": false
                    }
      toastr["info"]("<span>You can press <kbd>enter</kbd> to add option</span>");
    });
    $(document).on('keydown', 'input[name*="option"]', function (e){
      if (e.which === 13) {
        e.preventDefault();
        $(this).parents('.option_table').find('.add_option').trigger('click');
        $(this).closest('tr').next().find('input[name*="option"]').focus();
      }
    });
    $(document).on('focusout', 'input[name*="option"]', function (){
      toastr.clear();
    });
    $(document).on('click', '.ref-collec', function (){
      $.ajax({
          url: baseUrl+'/product/ajax_get_collection',
          type: 'GET',
          dataType: 'JSON',
          contentType: 'application/json charset=utf-8',
          success: function (data) {
            console.log(data);
            $('select[name="cat_id"]').html(data).select2();
          },
          error: function (data){
            console.log(data);
            toastr.options = {
                      "preventDuplicates": true,
                      "positionClass": "toast-top-right"
                  }
            toastr["error"]("Something wrong. Please refresh your page.");
          }
        });
    });

  }

  var imageupload = function(){
    Dropzone.autoDiscover = false;
    var fm;
    var lf;
    var ur = $('.dropzone').data('url');
    var tk = $('.dropzone').data('token');
    var tm = $('.dropzone').data('temp');
    if (!$('form').hasClass('prod-dropz')) {
      return;
    }
    var mockFile = {
      'name': '16bd881a5afe6ea281ee6a4fb162607c.png',
    }
    var upload = new Dropzone(".dropzone", {
      url: ur,
      acceptedFiles:"image/*",
      paramName:"imagesend",
      init: function (){
        lf = this.files.length;

        $(this.element).parents('form').find("input[type=submit]").on("click", function(e) {
              // Make sure that the form isn't actually being sent.
              e.preventDefault();
              e.stopPropagation();
              if ($(this).parents('form').valid()) {
                $(this).button('loading');
                if (upload.files.length !== 0) {
                  upload.processQueue();
                }else{
                  $(this).parents('form').submit();
                }
              }
              fm = $(this);
            });
      },
      headers: { "Product-Token": tk },
      uploadMultiple: true,
      dictInvalidFileType:"Forbidden type files.",
      addRemoveLinks:true,
      maxFiles: 5,
      autoProcessQueue: false,
      maxfilesexceeded: function (file){
      },
      parallelUploads: 5,
      dictDefaultMessage: 'Click to upload files',
      previewTemplate: '<div class="dz-preview dz-image-preview">'+
        '<div class="dz-image" >'+
          '<img data-dz-thumbnail />'+
        '</div>'+
        '<div class="dz-details">'+
          '<div class="dz-filename"><span data-dz-name></span></div>'+
          '<div class="dz-size" data-dz-size></div>'+
        '</div>'+
        '<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>'+
        '<div class="dz-success-mark"><span><svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">      <title>Check</title>      <defs></defs>      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>      </g>    </svg></span></div>'+
        '<div class="dz-error-mark"><span><svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">      <title>Error</title>      <defs></defs>      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">          <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>        </g>      </g>    </svg></span></div>'+
        '<div class="dz-error-message"><span data-dz-errormessage></span></div>'+
        '<a href="javascript:;" class="dz-primary primary-photo" data-primary="0">Set as primary</a>'+
      '</div>'

    });

    // upload.emit("addedfile", mockFile);
    // upload.createThumbnailFromUrl(mockFile, base+"/upload/lookbook/31/16bd881a5afe6ea281ee6a4fb162607c.png");
    // upload.files.push(mockFile);

    upload.on("sendingmultiple",function(file, xhr, form){
      // file.token = tk;
      var arr = []
      $(file).each(function(ind,val){
        arr.push(val.tkname);
      })
      pm = $(file.previewElement).find('.primary-photo').data('primary');
      nm = file.tkname;
      form.append("prm", pm);
      form.append('name', JSON.stringify(arr));
    });

    upload.on('successmultiple', function (file){
       fm.parents('form').submit();
    });

    upload.on("error", function (file){
      if (!file.accepted) this.removeFile(file);
    });

    upload.on("addedfiles", function(file){
      if ($(upload.element).hasClass('had-prim')) {

      }else{
        $(file[0].previewElement).find('.primary-photo').removeData('primary');
        $(file[0].previewElement).find('.primary-photo').attr('data-primary', '1');
        $(file[0].previewElement).find('.primary-photo').html('Primary');
        $(this).addClass("prim-ed");
        $(file[0].previewElement).find('.dz-remove').addClass('hide');
        $(upload.element).addClass('had-prim');
      }
      gen_input_temp(upload.files);
    });

    upload.on("removedfile", function (file){
      gen_input_temp(upload.files);
    });

    function gen_input_temp (file){
      var ti = '';
      var to = '';
      $(file).each(function (ind, val){
        val.tkname = $.md5(val.name+ind)+".png";
        var name = val.tkname;
        var primary = $(val.previewElement).find('.primary-photo').data('primary');
        ti += '<input type="hidden" name="path_image_temp['+ind+']" value="'+name+'"><input type="hidden" name="primary_temp['+ind+']" value="'+primary+'">';
        to += name;
      });
      $('input[name="image_required"]').val(to);
      // console.log($('input[name="image_required"]').val());
      $('#temp_input').html(ti);
      ti = '';
      to = '';
    };

    $(document).on('click', '.primary-photo', function (){
      $(this).removeData('primary');
      $(this).attr("data-primary", "1");
      $(this).html("Primary");
      $(this).addClass("prim-ed");
      $(this).parents('.dz-preview').find('.dz-remove').addClass('hide');
      $('.primary-photo').not($(this)).removeData('primary');
      $('.primary-photo').not($(this)).attr('data-primary', '0');
      $('.primary-photo').not($(this)).html('Set as primary');
      $('.primary-photo').not($(this)).removeClass('prim-ed');
      $('.primary-photo').not($(this)).parents('.dz-preview').find('.dz-remove').removeClass('hide');
      gen_input_temp(upload.files);
    });

  }

  var imageload = function (){
    $('img').lazy({
      effect: 'fadeIn',
      effectTime: 1000,
      threshold: 0,
      visibleOnly: true
    });
  }

  var validat = function(){
    $('#zeealsForm').bind().validate({
      ignore: "",
      errorElement: 'span',
      errorPlacement: function (error, element){
        $(element).closest('.form-group').find('.error-message .alert').append(error);
      },
      highlight: function (e) {
          $(e).closest('.form-group').find('.error-message').removeClass('hide');
      },
      unhighlight: function (e) {
          $(e).closest('.form-group').find('.error-message').addClass('hide');
      }
    });
    $('#zeealsNForm').bind().validate({
      ignore: "",
      errorElement: 'span',
      errorPlacement: function (error, element){
        $(element).closest('.form-group').find('.error-message .alert').append(error);
      },
      highlight: function (e) {
          $(e).closest('.form-group').find('.error-message').removeClass('hide');
      },
      unhighlight: function (e) {
          $(e).closest('.form-group').find('.error-message').addClass('hide');
      }
    });
  }

  return{
    init: function (){
      readyload();
      datepicker();
      mce();
      select();
      address();
      table();
      chart();
      datatable();
      preview();
      copyfrom();
      product();
      order();
      imageupload();
      validat();
      imageload();
      jne();
      notif();
      $('#deleteModal').on('show.bs.modal', function(e) {
          $(this).find('.danger').attr('onclick', 'location.href=\"' + $(e.relatedTarget).data('href') + '\"');
      });
      $('#deleteAll').on('show.bs.modal', function (e) {
          $(this).find('.danger2').attr('onclick', 'go_delete(\"' + $(e.relatedTarget).data('href') + '\")');
      });
    }
  }
}();

var username = $('body').data('user');
var dates = $('body').data('date');

function go_delete(p_url) {
      id_obj = [];
      $('.checkboxes:checked').each(function ()
      {
          ids = $(this).data();
          var id_array = {};
          $.each(ids,function(index, value){
              if(index !== 'uniformed'){
                  id_array[index] = value.toString();
              }
          });
          id_obj.push(id_array);
      }).get();
      var post = {ids: id_obj, updated_by: username, updated_on: dates}
      $.ajax({
          url: p_url,
          type: 'POST',
          dataType: 'JSON',
          contentType: 'application/json charset=utf-8',
          data: JSON.stringify(post),
          success: function (data) {
              location.reload();
          },
          error: function (data){
            // console.log(data);

          }
      });
      $('#del_All').modal('hide');
  }