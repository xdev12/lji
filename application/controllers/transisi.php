<?php

class Transisi extends CI_Controller {

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'transisi/');
        $this->data = array();
        init_generic_dao();
        $this->load->library(array('template_admin'));
        $this->logged_in();
        $this->data['page_title'] = "transisi";
    }
    
    public function index(){
        $this->load->view('transisi',$this->data);
    }
	
	function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "auth");
        }
    }

}

?>