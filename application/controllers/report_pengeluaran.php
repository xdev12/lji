<?php

class report_pengeluaran extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'report_pengeluaran/');
        $this->data = array();
        init_generic_dao();
        $this->load->model(array('m_pengeluaran','m_pembelian','m_jenis_pengeluaran'));
        $this->load->library(array('template_admin'));
        $this->logged_in();
        $this->data['page_title'] = "pengeluaran";
        date_default_timezone_set("Asia/Jakarta");
        $this->path_bukti = "upload/bukti_pengeluaran";
    }

    private function validate() {           
        $this->form_validation->set_rules('pengeluaran_gambar', 'Bukti Pengeluaran', 'callback_upload_foto');

        return $this->form_validation->run();
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
        $this->data['jenis_pengeluaran_list'] = $this->m_jenis_pengeluaran->fetch();
    }

    public function index($page = 1) {
        $this->preload();
        $this->session->set_userdata(array('filter_pengeluaran' => array(
                'pengeluaran_bulan' => '',
                'pengeluaran_tahun' => '',
                'pengeluaran_jenis' => ''))
        );
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset);
    }

    public function fetch_record($keys) {
        $this->data['pengeluaran'] = $this->m_pengeluaran->by_id($keys);
    }

    private function fetch_data($limit, $offset, $key) {
        $this->data['pengeluaran'] = $this->m_pengeluaran->get_data($key, $limit, $offset);
        $this->data['total_rows'] = count($this->data['pengeluaran']);
    }


    private function fetch_input() {
        $data = array('pengeluaran_jenis' => $this->input->post('pengeluaran_jenis'),
                    'pengeluaran_tgl' => $this->input->post('pengeluaran_tgl'),
                    'pengeluaran_ket' => $this->input->post('pengeluaran_ket'),
                    'pengeluaran_total' => $this->input->post('pengeluaran_total'),
                    'pengeluaran_gambar' => $this->input->post('pengeluaran_gambar')
                    );

        return $data;
    }


    public function search($page = 1) {
        $this->preload();
        $key = $this->session->userdata('filter_pengeluaran');

        if ($this->input->post('search')) {
            $key = array(
                'pengeluaran_bulan' => $this->input->post('pengeluaran_bulan'),
                'pengeluaran_tahun' => $this->input->post('pengeluaran_tahun'),
                'pengeluaran_jenis' => $this->input->post('pengeluaran_jenis')
            );
            $this->session->set_userdata(array('filter_pengeluaran' => $key));  
        }
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset, $key);
    }
    
    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->fetch_data($limit, $offset, $key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key))?'search':'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $this->data['offset'] = $offset;
        $this->data['pagination'] = $this->pagination->create_links();
        $this->template_admin->display('report/pengeluaran', $this->data);
    }

    public function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "admin/auth");
        }
    }

}

?>