<?php

class penjualan extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'penjualan/');
        $this->data = array();
        init_generic_dao();
        $this->load->model(array('m_penjualan','m_pembelian','m_jenis_penjualan'));
        $this->load->library(array('template_admin'));
        $this->logged_in();
        $this->data['page_title'] = "penjualan";
        date_default_timezone_set("Asia/Jakarta");
        $this->path_invoice = "upload/invoice_penjualan";
        $this->path_bukti = "upload/buktibayar_penjualan";
    }

    private function validate() {           
        $this->form_validation->set_rules('penjualan_foto_invoice', 'Foto', 'callback_upload_foto');
        $this->form_validation->set_rules('penjualan_bukti_bayar', 'Bukti Bayar', 'callback_upload_bukti');

        return $this->form_validation->run();
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
        $this->data['jenis_penjualan_list'] = $this->m_jenis_penjualan->fetch();
    }

    public function index($page = 1) {
        $this->preload();
        $this->session->set_userdata(array('filter_penjualan' => array(
                'penjualan_no_invoice' => ''))
        );
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset);
    }

    public function fetch_record($keys) {
        $this->data['penjualan'] = $this->m_penjualan->by_id($keys);
    }

    private function fetch_data($limit, $offset, $key) {
        $this->data['penjualan'] = $this->m_penjualan->fetch($limit, $offset, null, true,null, null, $key);
        $this->data['total_rows'] = $this->m_penjualan->fetch(null,null, null, true,null, null, $key,true);
    }


    private function fetch_input() {
        $data = array('penjualan_jenis' => $this->input->post('penjualan_jenis'),
                    'penjualan_tgl_invoice' => $this->input->post('penjualan_tgl_invoice'),
                    'penjualan_no_invoice' => $this->input->post('penjualan_no_invoice'),
                    'penjualan_uraian_barang' => $this->input->post('penjualan_uraian_barang'),
                    'penjualan_nama_perusahaan' => $this->input->post('penjualan_nama_perusahaan'),
                    'penjualan_nama_tug_boat' => $this->input->post('penjualan_nama_tug_boat'),
                    'penjualan_nama_tongkang' => $this->input->post('penjualan_nama_tongkang'),
                    'penjualan_tonase' => $this->input->post('penjualan_tonase'),
                    'penjualan_harga_satuan' => $this->input->post('penjualan_harga_satuan'),
                    'penjualan_jumlah_total' => $this->input->post('penjualan_jumlah_total'),
                    'penjualan_foto_invoice' => $this->input->post('penjualan_foto_invoice'),
                    'penjualan_bukti_bayar' => $this->input->post('penjualan_bukti_bayar'),
                    );

        return $data;
    }

    public function add() {
        $obj = $this->fetch_input();
        $obj['created_by'] = $this->session->userdata('username');
        $obj['created_on'] = date('Y-m-d H:i:s');

        if ($this->validate() != false) {
            $file1 = $this->file_link_foto;
            $file2 = $this->file_link_bukti;
            $obj['penjualan_foto_invoice'] = (!empty($file1)?$file1:null);
            $obj['penjualan_bukti_bayar'] = (!empty($file2)?$file2:null);
            $this->m_penjualan->insert($obj);

            $this->session->set_flashdata(array('message'=>'Data inserted successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = false;
            #set value
            $this->data['penjualan'] = (object) $obj;
            $this->template_admin->display('penjualan/penjualan_insert', $this->data);
        }
    }

     function upload_foto() {
        $param = $_FILES['penjualan_foto_invoice'];
        if (!empty($param['name'])) {
            $_FILES['userfile'] = $param;
            $config = array();
            $config['upload_path'] = "./" . $this->path_invoice;
            $config['allowed_types'] = 'jpg|png|gif|jpeg';
            $config['encrypt_name'] = TRUE;
            $config['max_size'] = '100000';
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload()) {
                $upload_data = $this->upload->data();
                $this->file_link_foto = $this->path_invoice .'/'. $upload_data['file_name'];
                return true;
            } else {
                $this->form_validation->set_message('handle_upload', $this->upload->display_errors());
                return false;
            }
        } else {
            $this->file_link = null;
            return true;
        }
    }

    function upload_bukti() {
        $param = $_FILES['penjualan_bukti_bayar'];
        if (!empty($param['name'])) {
            $_FILES['userfile'] = $param;
            $config = array();
            $config['upload_path'] = "./" . $this->path_bukti;
            $config['allowed_types'] = 'jpg|png|gif|jpeg';
            $config['encrypt_name'] = TRUE;
            $config['max_size'] = '100000';
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload()) {
                $upload_data = $this->upload->data();
                $this->file_link_bukti = $this->path_bukti .'/'. $upload_data['file_name'];
                return true;
            } else {
                $this->form_validation->set_message('handle_upload', $this->upload->display_errors());
                return false;
            }
        } else {
            $this->file_link = null;
            return true;
        }
    }

    /**

      @description
      viewing editing form. repopulation for every data needed in form done here.
     */
    public function edit($penjualan_id) {
        $obj = $this->fetch_input();
        $obj['updated_by'] = $this->session->userdata('username');
        $obj['updated_on'] = date('Y-m-d H:i:s');

        $obj_id = array('penjualan_id' => $penjualan_id);

        if ($this->validate() != false) {
            $this->m_penjualan->update($obj, $obj_id);
            $this->session->set_flashdata(array('message'=>'Data edited successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = true;
            $this->fetch_record($obj_id);
            $this->template_admin->display('penjualan/penjualan_insert', $this->data);
        }
    }


    public function delete($offer_id) {
        $obj_id = array('offer_id' => $offer_id);
        // $this->m_penjualan_account->delete($obj_id);
        $this->m_penjualan->delete($obj_id);
        $this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        redirect(CURRENT_CONTEXT);
    }
    

    public function search($page = 1) {
        $this->preload();
        $key = $this->session->userdata('filter_penjualan');

        if ($this->input->post('search')) {
            $key = array(
                'penjualan_no_invoice' => $this->input->post('penjualan_no_invoice')
            );
            $this->session->set_userdata(array('filter_penjualan' => $key));  
        }
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset, $key);
    }
    
    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->fetch_data($limit, $offset, $key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key))?'search':'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $this->data['offset'] = $offset;
        $this->data['pagination'] = $this->pagination->create_links();
        $this->template_admin->display('penjualan/penjualan_list', $this->data);
    }

    public function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "auth");
        }
    }

}

?>