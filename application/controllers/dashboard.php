<?php

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'dashboard/');
        $this->data = array();
        init_generic_dao();
        $this->load->library(array('template_admin'));
        $this->logged_in();
        $this->data['page_title'] = "Dashboard";
    }
    
    public function master(){
        $this->session->set_userdata(array('menu_category' => 3));
        $this->template_admin->display('dashboard',$this->data);
    }

    public function coal(){
        $this->session->set_userdata(array('menu_category' => 1));
        $this->template_admin->display('dashboard',$this->data);
    }
	
	function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "auth");
        }
    }

}

?>