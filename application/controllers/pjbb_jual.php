<?php

class Pjbb_jual extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'pjbb_jual/');
        $this->data = array();
        init_generic_dao();
        $this->load->model(array('m_pjbb_jual', 'm_kontrak'));
        $this->load->library(array('template_admin'));
        $this->logged_in();
        $this->data['page_title'] = "pjbb jual";
        date_default_timezone_set("Asia/Jakarta");
    }

    private function validate() {			
        $this->form_validation->set_rules('pjual_nama', 'pjual_nama', 'trim|required|max_length[100]');

        return $this->form_validation->run();
    }

    private function validate_kontrak() {           
        $this->form_validation->set_rules('kontrak_isi', 'kontrak_isi', 'trim|required');

        return $this->form_validation->run();
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
    }

    public function index($page = 1) {
        $this->preload();
        $this->session->set_userdata(array('filter_pjbb_jual' => array(
				'pjual_nama' => ''))
        );
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset);
    }

    public function fetch_record($keys) {
        $this->data['pjbb_jual'] = $this->m_pjbb_jual->by_id($keys);
    }

    public function fetch_record_kontrak($keys) {
        $this->data['kontrak'] = $this->m_kontrak->by_id($keys);
    }

    private function fetch_data($limit, $offset, $key) {
        $this->data['pjbb_jual'] = $this->m_pjbb_jual->get_data($key, $limit, $offset);
        // print_r($this->db->last_query());die();
        // $this->data['pjbb_jual'] = $this->m_pjbb_jual->fetch($limit, $offset, null, true,null, null, $key);
        $this->data['total_rows'] = count($this->data['pjbb_jual']);
    }

    private function fetch_input() {
        $data = array('pjual_no' => $this->input->post('pjual_no'),
                    'pjual_nama' => $this->input->post('pjual_nama'),
                    'pjual_perusahaan' => $this->input->post('pjual_perusahaan'),
                    'pjual_alamat' => $this->input->post('pjual_alamat'),
                    'pjual_spek' => $this->input->post('pjual_spek'),
                    'pjual_kondisi' => $this->input->post('pjual_kondisi'),
                    'pjual_asal_batu' => $this->input->post('pjual_asal_batu'),
                    'pjual_alamat_asal_batu' => $this->input->post('pjual_alamat_asal_batu'),
                    'pjual_jumlah_muatan' => $this->input->post('pjual_jumlah_muatan'),
                    'pjual_harga_perton' => $this->input->post('pjual_harga_perton'),
                    'pjual_toleransi' => $this->input->post('pjual_toleransi'),
                    'pjual_pelabuhan_muat' => $this->input->post('pjual_pelabuhan_muat'),
                    'pjual_pelabuhan_bongkar' => $this->input->post('pjual_pelabuhan_bongkar'),
                    'pjual_pembayaran1' => $this->input->post('pjual_pembayaran1'),
                    'pjual_pembayaran2' => $this->input->post('pjual_pembayaran2'),
                    'pjual_pembayaran3' => $this->input->post('pjual_pembayaran3'),
                    'pjual_status' => $this->input->post('pjual_status'),
                    'pjual_ttd' => $this->input->post('pjual_ttd')
                    );

        return $data;
    }

    public function add() {
        $obj = $this->fetch_input();
        $obj['created_by'] = $this->session->userdata('username');
        $obj['created_on'] = date('Y-m-d H:i:s');

        if ($this->validate() != false) {
            $this->m_pjbb_jual->insert($obj);
            $this->session->set_flashdata(array('message'=>'Data inserted successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = false;
            $this->data['code'] = $this->generate_code();
            #set value
            $this->data['pjbb_jual'] = (object) $obj;
            $this->template_admin->display('pjbb_jual/pjbb_jual_insert', $this->data);
        }
    }

    /**

      @description
      viewing editing form. repopulation for every data needed in form done here.
     */
    public function edit($pjual_id) {
        $obj = $this->fetch_input();
        $obj['updated_by'] = $this->session->userdata('username');
        $obj['updated_on'] = date('Y-m-d H:i:s');

        $obj_id = array('pjual_id' => $pjual_id);

        if ($this->validate() != false) {
            $this->m_pjbb_jual->update($obj, $obj_id);
            $this->session->set_flashdata(array('message'=>'Data edited successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = true;
            $this->fetch_record($obj_id);
            $this->template_admin->display('pjbb_jual/pjbb_jual_insert', $this->data);
        }
    }

    public function edit_kontrak($kontrak_jenis) {
        $obj['kontrak_isi'] = $this->input->post('kontrak_isi');
        $obj['updated_by'] = $this->session->userdata('username');
        $obj['updated_on'] = date('Y-m-d H:i:s');

        $obj_id = array('kontrak_jenis' => $kontrak_jenis);

        if ($this->validate_kontrak() != false) {
            $this->m_kontrak->update($obj, $obj_id);
            $this->session->set_flashdata(array('message'=>'Data edited successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = true;
            $this->fetch_record_kontrak($obj_id);
            $this->template_admin->display('pjbb_jual/pjbb_jual_kontrak', $this->data);
        }
    }

    public function export_pdf($pjual_id){
        ob_start();
        $key = array('pjual_id' => $pjual_id);
        $this->fetch_record($key);
        $this->load->view('pjbb_jual/pjbb_jual_pdf', $this->data);
        $html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/plugins/html2pdf/html2pdf.class.php');
        $pdf = new HTML2PDF('L','A4','en');
        $pdf->WriteHTML($html);
        $pdf->Output('PJBB ['.date('Y-m-d').']' . '.pdf');
    }

    /**
      @description
      viewing record. repopulation for every data needed for view.
     */
    public function detail($pjbb_jual_id) {
        $obj_id = array('pjbb_jual_id' => $pjbb_jual_id);

        $this->preload();
        $this->fetch_record($obj_id);
        $this->template_admin->display('admin/pjbb_jual/pjbb_jual_detail', $this->data);
    }

    public function delete($pjbb_jual_id) {
        $obj_id = array('pjbb_jual_id' => $pjbb_jual_id);
        // $this->m_pjbb_jual_account->delete($obj_id);
        $this->m_pjbb_jual->delete($obj_id);
		$this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        redirect(CURRENT_CONTEXT);
    }
	
	public function delete_multiple(){
        $data = file_get_contents('php://input');
        $id = json_decode($data);
		foreach($id->ids as $id){
			$obj_id = array('pjbb_jual_id' => $id->pjbb_jual_id);
			$this->m_pjbb_jual->delete($obj_id);
		}
		$this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        echo json_encode(array('status'=>200));
    }

	public function search($page = 1) {
        $this->preload();
		$key = $this->session->userdata('filter_pjbb_jual');

        if ($this->input->post('search')) {
            $key = array(
                'month' => $this->input->post('month'),
                'year' => $this->input->post('year'),
                'pjual_nama' => $this->input->post('pjual_nama')
            );
			$this->session->set_userdata(array('filter_pjbb_jual' => $key));  
        }
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset, $key);
    }
	
    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->fetch_data($limit, $offset, $key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key))?'search':'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $this->data['offset'] = $offset;
        $this->data['pagination'] = $this->pagination->create_links();
        $this->template_admin->display('pjbb_jual/pjbb_jual_list', $this->data);
    }

    public function generate_code() {
        $currentDate = date('Y-m-d');
        $date = explode('-', $currentDate);
        $last_no = $this->m_pjbb_jual->fetch(1, 0, null, false, null, null, null, false, false);
        if(!empty($last_no)){
            $a = explode('/', $last_no[0]->pjual_no);
            $number = (int) $a[0] + 1;
            $new_code = str_pad($number, 3, '0', STR_PAD_LEFT);
        }else{
            $new_code = '001';
        }
        $code = $new_code.'/PJBB/'.$date['1'].'/'.$date[0];
        return $code;
    }

    public function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "auth");
        }
    }

}

?>