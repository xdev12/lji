<?php

class pengeluaran extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'pengeluaran/');
        $this->data = array();
        init_generic_dao();
        $this->load->model(array('m_pengeluaran','m_pembelian','m_jenis_pengeluaran'));
        $this->load->library(array('template_admin'));
        $this->logged_in();
        $this->data['page_title'] = "pengeluaran";
        date_default_timezone_set("Asia/Jakarta");
        $this->path_bukti = "upload/bukti_pengeluaran";
    }

    private function validate() {           
        $this->form_validation->set_rules('pengeluaran_gambar', 'Bukti Pengeluaran', 'callback_upload_foto');

        return $this->form_validation->run();
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
        $this->data['jenis_pengeluaran_list'] = $this->m_jenis_pengeluaran->fetch();
    }

    public function index($page = 1) {
        $this->preload();
        $this->session->set_userdata(array('filter_pengeluaran' => array(
                'pengeluaran_ket' => ''))
        );
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset);
    }

    public function fetch_record($keys) {
        $this->data['pengeluaran'] = $this->m_pengeluaran->by_id($keys);
    }

    private function fetch_data($limit, $offset, $key) {
        $this->data['pengeluaran'] = $this->m_pengeluaran->fetch($limit, $offset, null, true,null, null, $key);
        $this->data['total_rows'] = $this->m_pengeluaran->fetch(null,null, null, true,null, null, $key,true);
    }


    private function fetch_input() {
        $data = array('pengeluaran_jenis' => $this->input->post('pengeluaran_jenis'),
                    'pengeluaran_tgl' => $this->input->post('pengeluaran_tgl'),
                    'pengeluaran_ket' => $this->input->post('pengeluaran_ket'),
                    'pengeluaran_total' => $this->input->post('pengeluaran_total'),
                    'pengeluaran_gambar' => $this->input->post('pengeluaran_gambar')
                    );

        return $data;
    }

    public function add() {
        $obj = $this->fetch_input();
        $obj['created_by'] = $this->session->userdata('username');
        $obj['created_on'] = date('Y-m-d H:i:s');

        if ($this->validate() != false) {
            $file = $this->file_link_foto;
            $obj['pengeluaran_gambar'] = (!empty($file)?$file:null);
            $this->m_pengeluaran->insert($obj);

            $this->session->set_flashdata(array('message'=>'Data inserted successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = false;
            #set value
            $this->data['pengeluaran'] = (object) $obj;
            $this->template_admin->display('pengeluaran/pengeluaran_insert', $this->data);
        }
    }

     function upload_foto() {
        $param = $_FILES['pengeluaran_gambar'];
        if (!empty($param['name'])) {
            $_FILES['userfile'] = $param;
            $config = array();
            $config['upload_path'] = "./" . $this->path_bukti;
            $config['allowed_types'] = 'jpg|png|gif|jpeg';
            $config['encrypt_name'] = TRUE;
            $config['max_size'] = '100000';
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload()) {
                $upload_data = $this->upload->data();
                $this->file_link_foto = $this->path_bukti .'/'. $upload_data['file_name'];
                return true;
            } else {
                $this->form_validation->set_message('handle_upload', $this->upload->display_errors());
                return false;
            }
        } else {
            $this->file_link = null;
            return true;
        }
    }


    /**

      @description
      viewing editing form. repopulation for every data needed in form done here.
     */
    public function edit($pengeluaran_id) {
        $obj = $this->fetch_input();
        $obj['updated_by'] = $this->session->userdata('username');
        $obj['updated_on'] = date('Y-m-d H:i:s');

        $obj_id = array('pengeluaran_id' => $pengeluaran_id);

        if ($this->validate() != false) {
            $this->m_pengeluaran->update($obj, $obj_id);
            $this->session->set_flashdata(array('message'=>'Data edited successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = true;
            $this->fetch_record($obj_id);
            $this->template_admin->display('pengeluaran/pengeluaran_insert', $this->data);
        }
    }


    public function delete($offer_id) {
        $obj_id = array('offer_id' => $offer_id);
        // $this->m_pengeluaran_account->delete($obj_id);
        $this->m_pengeluaran->delete($obj_id);
        $this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        redirect(CURRENT_CONTEXT);
    }
    

    public function search($page = 1) {
        $this->preload();
        $key = $this->session->userdata('filter_pengeluaran');

        if ($this->input->post('search')) {
            $key = array(
                'pengeluaran_ket' => $this->input->post('pengeluaran_ket')
            );
            $this->session->set_userdata(array('filter_pengeluaran' => $key));  
        }
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset, $key);
    }
    
    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->fetch_data($limit, $offset, $key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key))?'search':'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $this->data['offset'] = $offset;
        $this->data['pagination'] = $this->pagination->create_links();
        $this->template_admin->display('pengeluaran/pengeluaran_list', $this->data);
    }

    public function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "admin/auth");
        }
    }

}

?>