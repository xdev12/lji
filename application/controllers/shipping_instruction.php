<?php

class Shipping_instruction extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'shipping_instruction/');
        $this->data = array();
        init_generic_dao();
        $this->load->model(array('m_shipping_instruction','m_pjbb_jual'));
        $this->load->library(array('template_admin'));
        $this->logged_in();
        $this->data['page_title'] = "Shipping Instruction";
        date_default_timezone_set("Asia/Jakarta");
    }

    private function validate() {			$this->form_validation->set_rules('si_shipper', 'si_shipper', 'trim|required|max_length[100]');

        return $this->form_validation->run();
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
        $this->data['pjbb_jual'] = $this->m_pjbb_jual->fetch();
    }

    public function index($page = 1) {
        $this->preload();
        $this->session->set_userdata(array('filter_shipping_instruction' => array(
				'si_shipper' => ''))
        );
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset);
    }

    public function fetch_record($keys) {
        $this->data['shipping_instruction'] = $this->m_shipping_instruction->by_id($keys);
    }

    private function fetch_data($limit, $offset, $key) {
        $this->data['shipping_instruction'] = $this->m_shipping_instruction->fetch($limit, $offset, null, true,null, null, $key);
        $this->data['total_rows'] = $this->m_shipping_instruction->fetch(null,null, null, true,null, null, $key,true);
    }

    private function fetch_input() {
        $data = array('si_no' => $this->input->post('si_no'),
                    'si_pjbb_no' => $this->input->post('si_pjbb_no'),
                    'si_to' => $this->input->post('si_to'),
                    'si_shipper' => $this->input->post('si_shipper'),
                    'si_consignee' => $this->input->post('si_consignee'),
                    'si_notif_address' => $this->input->post('si_notif_address'),
                    'si_pol' => $this->input->post('si_pol'),
                    'si_pod' => $this->input->post('si_pod'),
                    'si_date_load' => $this->input->post('si_date_load'),
                    'si_vessel' => $this->input->post('si_vessel'),
                    'si_cargo' => $this->input->post('si_cargo'),
                    'si_desc' => $this->input->post('si_desc'),
                    'si_mark' => $this->input->post('si_mark'),
                    'si_agen_kapal' => $this->input->post('si_agen_kapal'));

        return $data;
    }

    public function add() {
        $obj = $this->fetch_input();
        $obj['created_by'] = $this->session->userdata('username');
        $obj['created_on'] = date('Y-m-d H:i:s');

        if ($this->validate() != false) {
            $this->m_shipping_instruction->insert($obj);
            $this->session->set_flashdata(array('message'=>'Data inserted successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = false;
            #set value
            $this->data['shipping_instruction'] = (object) $obj;
            $this->template_admin->display('shipping_instruction/shipping_instruction_insert', $this->data);
        }
    }

     public function export_pdf($si_id){
        ob_start();
        $key = array('si_id' => $si_id);
        $this->fetch_record($key);
        $this->load->view('shipping_instruction/shipping_instruction_pdf', $this->data);
        $html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/plugins/html2pdf/html2pdf.class.php');
        $pdf = new HTML2PDF('L','A4','en');
        $pdf->WriteHTML($html);
        $pdf->Output('SI ['.date('Y-m-d').']' . '.pdf');
    }

    /**

      @description
      viewing editing form. repopulation for every data needed in form done here.
     */
    public function edit($si_id) {
        $obj = $this->fetch_input();
        $obj['updated_by'] = $this->session->userdata('username');
        $obj['updated_on'] = date('Y-m-d H:i:s');

        $obj_id = array('si_id' => $si_id);

        if ($this->validate() != false) {
            $this->m_shipping_instruction->update($obj, $obj_id);
            $this->session->set_flashdata(array('message'=>'Data edited successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = true;
            $this->fetch_record($obj_id);
            $this->template_admin->display('admin/shipping_instruction/shipping_instruction_insert', $this->data);
        }
    }

    /**
      @description
      viewing record. repopulation for every data needed for view.
     */
    public function detail($si_id) {
        $obj_id = array('si_id' => $si_id);

        $this->preload();
        $this->fetch_record($obj_id);
        $this->template_admin->display('admin/shipping_instruction/shipping_instruction_detail', $this->data);
    }

    public function delete($si_id) {
        $obj_id = array('si_id' => $si_id);
        // $this->m_shipping_instruction_account->delete($obj_id);
        $this->m_shipping_instruction->delete($obj_id);
		$this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        redirect(CURRENT_CONTEXT);
    }
	
	public function delete_multiple(){
        $data = file_get_contents('php://input');
        $id = json_decode($data);
		foreach($id->ids as $id){
			$obj_id = array('si_id' => $id->si_id);
			$this->m_shipping_instruction->delete($obj_id);
		}
		$this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        echo json_encode(array('status'=>200));
    }

	public function search($page = 1) {
        $this->preload();
		$key = $this->session->userdata('filter_shipping_instruction');

        if ($this->input->post('search')) {
            $key = array(
                'pbeli_nama' => $this->input->post('pbeli_nama')
            );
			$this->session->set_userdata(array('filter_shipping_instruction' => $key));  
        }
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset, $key);
    }
	
    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->fetch_data($limit, $offset, $key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key))?'search':'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $this->data['offset'] = $offset;
        $this->data['pagination'] = $this->pagination->create_links();
        $this->template_admin->display('shipping_instruction/shipping_instruction_list', $this->data);
    }

    public function get_data() {
        $id = $this->input->post('id');
        $result = $this->m_pjbb_jual->by_id(array('pjual_no' => $id));
        $data['pjual_perusahaan'] = $result->pjual_perusahaan;
        // print_r($result);die();

        echo json_encode($data);
    }

    public function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "auth");
        }
    }

}

?>