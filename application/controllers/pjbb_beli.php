<?php

class Pjbb_beli extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'pjbb_beli/');
        $this->data = array();
        init_generic_dao();
        $this->load->model(array('m_pjbb_beli'));
        $this->load->library(array('template_admin'));
        $this->logged_in();
        $this->data['page_title'] = "pjbb beli";
        date_default_timezone_set("Asia/Jakarta");
    }

    private function validate() {			$this->form_validation->set_rules('pbeli_nama', 'pbeli_nama', 'trim|required|max_length[100]');

        return $this->form_validation->run();
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
    }

    public function index($page = 1) {
        $this->preload();
        $this->session->set_userdata(array('filter_pjbb_beli' => array(
				'pbeli_nama' => ''))
        );
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset);
    }

    public function fetch_record($keys) {
        $this->data['pjbb_beli'] = $this->m_pjbb_beli->by_id($keys);
    }

    private function fetch_data($limit, $offset, $key) {
        $this->data['pjbb_beli'] = $this->m_pjbb_beli->get_data($key, $limit, $offset);
        $this->data['total_rows'] = count($this->data['pjbb_beli']);
    }

    private function fetch_input() {
        $data = array('pbeli_no' => $this->input->post('pbeli_no'),
                    'pbeli_nama' => $this->input->post('pbeli_nama'),
                    'pbeli_perusahaan' => $this->input->post('pbeli_perusahaan'),
                    'pbeli_alamat' => $this->input->post('pbeli_alamat'),
                    'pbeli_spek' => $this->input->post('pbeli_spek'),
                    'pbeli_kondisi' => $this->input->post('pbeli_kondisi'),
                    'pbeli_asal_batu' => $this->input->post('pbeli_asal_batu'),
                    'pbeli_alamat_asal_batu' => $this->input->post('pbeli_alamat_asal_batu'),
                    'pbeli_jumlah_muatan' => $this->input->post('pbeli_jumlah_muatan'),
                    'pbeli_harga_perton' => $this->input->post('pbeli_harga_perton'),
                    'pbeli_toleransi' => $this->input->post('pbeli_toleransi'),
                    'pbeli_pelabuhan_muat' => $this->input->post('pbeli_pelabuhan_muat'),
                    'pbeli_pelabuhan_bongkar' => $this->input->post('pbeli_pelabuhan_bongkar'),
                    'pbeli_pembayaran1' => $this->input->post('pbeli_pembayaran1'),
                    'pbeli_pembayaran2' => $this->input->post('pbeli_pembayaran2'),
                    'pbeli_pembayaran3' => $this->input->post('pbeli_pembayaran3'),
                    'pbeli_status' => $this->input->post('pbeli_status'),
                    'pbeli_ttd' => $this->input->post('pbeli_ttd'),
                    'pbeli_no_rek' => $this->input->post('pbeli_no_rek'),
                    'pbeli_bank' => $this->input->post('pbeli_bank')
                );

        return $data;
    }

    public function add() {
        $obj = $this->fetch_input();
        $obj['created_by'] = $this->session->userdata('username');
        $obj['created_on'] = date('Y-m-d H:i:s');

        if ($this->validate() != false) {
            $this->m_pjbb_beli->insert($obj);
            $this->session->set_flashdata(array('message'=>'Data inserted successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = false;
            #set value
            $this->data['pjbb_beli'] = (object) $obj;
            $this->template_admin->display('pjbb_beli/pjbb_beli_insert', $this->data);
        }
    }

    /**

      @description
      viewing editing form. repopulation for every data needed in form done here.
     */
    public function edit($pbeli_id) {
        $obj = $this->fetch_input();
        $obj['updated_by'] = $this->session->userdata('username');
        $obj['updated_on'] = date('Y-m-d H:i:s');

        $obj_id = array('pbeli_id' => $pbeli_id);

        if ($this->validate() != false) {
            $this->m_pjbb_beli->update($obj, $obj_id);
            $this->session->set_flashdata(array('message'=>'Data edited successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = true;
            $this->fetch_record($obj_id);
            $this->template_admin->display('pjbb_beli/pjbb_beli_insert', $this->data);
        }
    }

    public function edit_kontrak($kontrak_jenis) {
        $obj['kontrak_isi'] = $this->input->post('kontrak_isi');
        $obj['updated_by'] = $this->session->userdata('username');
        $obj['updated_on'] = date('Y-m-d H:i:s');

        $obj_id = array('kontrak_jenis' => $kontrak_jenis);

        if ($this->validate_kontrak() != false) {
            $this->m_kontrak->update($obj, $obj_id);
            $this->session->set_flashdata(array('message'=>'Data edited successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = true;
            $this->fetch_record_kontrak($obj_id);
            $this->template_admin->display('pjbb_beli/pjbb_beli_kontrak', $this->data);
        }
    }

    public function export_pdf($pbeli_id){
        ob_start();
        $key = array('pbeli_id' => $pbeli_id);
        $this->fetch_record($key);
        $this->load->view('pjbb_beli/pjbb_beli_pdf', $this->data);
        $html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/plugins/html2pdf/html2pdf.class.php');
        $pdf = new HTML2PDF('L','A4','en');
        $pdf->WriteHTML($html);
        $pdf->Output('PJBB ['.date('Y-m-d').']' . '.pdf');
    }

    /**
      @description
      viewing record. repopulation for every data needed for view.
     */
    public function detail($pbeli_id) {
        $obj_id = array('pbeli_id' => $pbeli_id);

        $this->preload();
        $this->fetch_record($obj_id);
        $this->template_admin->display('admin/pjbb_beli/pjbb_beli_detail', $this->data);
    }

    public function delete($pbeli_id) {
        $obj_id = array('pbeli_id' => $pbeli_id);
        // $this->m_pjbb_beli_account->delete($obj_id);
        $this->m_pjbb_beli->delete($obj_id);
		$this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        redirect(CURRENT_CONTEXT);
    }
	
	public function delete_multiple(){
        $data = file_get_contents('php://input');
        $id = json_decode($data);
		foreach($id->ids as $id){
			$obj_id = array('pbeli_id' => $id->pbeli_id);
			$this->m_pjbb_beli->delete($obj_id);
		}
		$this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        echo json_encode(array('status'=>200));
    }

	public function search($page = 1) {
        $this->preload();
		$key = $this->session->userdata('filter_pjbb_beli');

        if ($this->input->post('search')) {
            $key = array(
                'month' => $this->input->post('month'),
                'year' => $this->input->post('year'),
                'pbeli_nama' => $this->input->post('pbeli_nama')
            );
			$this->session->set_userdata(array('filter_pjbb_beli' => $key));  
        }
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset, $key);
    }
	
    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->fetch_data($limit, $offset, $key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key))?'search':'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $this->data['offset'] = $offset;
        $this->data['pagination'] = $this->pagination->create_links();
        $this->template_admin->display('pjbb_beli/pjbb_beli_list', $this->data);
    }

    public function generate_code() {
        $currentDate = date('Y-m-d');
        $date = explode('-', $currentDate);
        $last_no = $this->m_pjbb_beli->fetch(1, 0, null, false, null, null, null, false, false);
        if(!empty($last_no)){
            $a = explode('/', $last_no[0]->pjual_no);
            $number = (int) $a[0] + 1;
            $new_code = str_pad($number, 3, '0', STR_PAD_LEFT);
        }else{
            $new_code = '001';
        }
        $code = $new_code.'/PJBB/'.$date['1'].'/'.$date[0];
        return $code;
    }

    public function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "admin/auth");
        }
    }

}

?>