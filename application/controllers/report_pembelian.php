<?php

class report_pembelian extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'report_pembelian/');
        $this->data = array();
        init_generic_dao();
        $this->load->model(array('m_pembelian'));
        $this->load->library(array('template_admin'));
        $this->logged_in();
        $this->data['page_title'] = "report pembelian";
        date_default_timezone_set("Asia/Jakarta");
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
    }

    public function index($page = 1) {
        $this->preload();
        $this->session->set_userdata(array('filter_pembelian' => array(
                'pembelian_jenis' => '',
                'pembelian_bulan' => '',
                'pembelian_tahun' => ''))
        );
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset);
    }

    private function fetch_data($limit, $offset, $key) {
        $this->data['pembelian'] = $this->m_pembelian->get_data($key, $limit, $offset);
        $this->data['total_rows'] = count($this->data['pembelian']);
    }

	public function search($page = 1) {
        $this->preload();
		$key = $this->session->userdata('filter_pembelian');

        if ($this->input->post('search')) {
            $key = array(
                'pembelian_jenis' => $this->input->post('pembelian_jenis'),
                'pembelian_bulan' => $this->input->post('pembelian_bulan'),
                'pembelian_tahun' => $this->input->post('pembelian_tahun')
            );
			$this->session->set_userdata(array('filter_pembelian' => $key));  
        }
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset, $key);
    }
	
    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->fetch_data($limit, $offset, $key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key))?'search':'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $this->data['offset'] = $offset;
        $this->data['pagination'] = $this->pagination->create_links();
        $this->template_admin->display('report/pembelian', $this->data);
    }

    public function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "admin/auth");
        }
    }

}

?>