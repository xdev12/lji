<?php

class jenis_pengeluaran extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'jenis_pengeluaran/');
        $this->data = array();
        init_generic_dao();
        $this->load->model(array('m_jenis_pengeluaran'));
        $this->load->library(array('template_admin'));
        $this->logged_in();
        $this->data['page_title'] = "jenis_pengeluaran";
        date_default_timezone_set("Asia/Jakarta");
        $this->path_invoice = "upload/jenis_pengeluaran";
        $this->path_bukti = "upload/buktibayar_asuransi";
    }

    private function validate() {			
        $this->form_validation->set_rules('jp_nama', 'Nama Kategori', 'trim|required|max_length[100]');

        return $this->form_validation->run();
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
    }

    public function index($page = 1) {
        $this->preload();
        $this->session->set_userdata(array('filter_jenis_pengeluaran' => array(
				'jp_nama' => ''))
        );
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset);
    }

    public function fetch_record($keys) {
        $this->data['jenis_pengeluaran'] = $this->m_jenis_pengeluaran->by_id($keys);
    }

    private function fetch_data($limit, $offset, $key) {
        $this->data['jenis_pengeluaran'] = $this->m_jenis_pengeluaran->fetch($limit, $offset, null, true,null, null, $key);
        $this->data['total_rows'] = $this->m_jenis_pengeluaran->fetch(null,null, null, true,null, null, $key,true);
    }

    private function fetch_input() {
        $data = array('jp_nama' => $this->input->post('jp_nama'),
                    'jp_deskripsi' => $this->input->post('jp_deskripsi')
                    );

        return $data;
    }

    public function add() {
        $obj = $this->fetch_input();
        $obj['created_by'] = $this->session->userdata('username');
        $obj['created_on'] = date('Y-m-d H:i:s');

        if ($this->validate() != false) {
            $this->m_jenis_pengeluaran->insert($obj);
            $this->session->set_flashdata(array('message'=>'Data inserted successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = false;
            #set value
            $this->data['jenis_pengeluaran'] = (object) $obj;
            $this->template_admin->display('jenis_pengeluaran/jenis_pengeluaran_insert', $this->data);
        }
    }

    
    /**

      @description
      viewing editing form. repopulation for every data needed in form done here.
     */
    public function edit($offer_id) {
        $obj = $this->fetch_input();
        $obj['updated_by'] = $this->session->userdata('username');
        $obj['updated_on'] = date('Y-m-d H:i:s');

        $obj_id = array('offer_id' => $offer_id);

        if ($this->validate() != false) {
            $this->m_jenis_pengeluaran->update($obj, $obj_id);
            $this->session->set_flashdata(array('message'=>'Data edited successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = true;
            $this->fetch_record($obj_id);
            $this->template_admin->display('admin/jenis_pengeluaran/jenis_pengeluaran_insert', $this->data);
        }
    }

    /**
      @description
      viewing record. repopulation for every data needed for view.
     */
    public function detail($offer_id) {
        $obj_id = array('offer_id' => $offer_id);

        $this->preload();
        $this->fetch_record($obj_id);
        $this->template_admin->display('admin/jenis_pengeluaran/jenis_pengeluaran_detail', $this->data);
    }

    public function delete($offer_id) {
        $obj_id = array('offer_id' => $offer_id);
        // $this->m_jenis_pengeluaran_account->delete($obj_id);
        $this->m_jenis_pengeluaran->delete($obj_id);
		$this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        redirect(CURRENT_CONTEXT);
    }
	
	public function delete_multiple(){
        $data = file_get_contents('php://input');
        $id = json_decode($data);
		foreach($id->ids as $id){
			$obj_id = array('offer_id' => $id->offer_id);
			$this->m_jenis_pengeluaran->delete($obj_id);
		}
		$this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        echo json_encode(array('status'=>200));
    }

	public function search($page = 1) {
        $this->preload();
		$key = $this->session->userdata('filter_jenis_pengeluaran');

        if ($this->input->post('search')) {
            $key = array(
                'offer_to' => $this->input->post('offer_to')
            );
			$this->session->set_userdata(array('filter_jenis_pengeluaran' => $key));  
        }
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset, $key);
    }
	
    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->fetch_data($limit, $offset, $key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key))?'search':'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $this->data['offset'] = $offset;
        $this->data['pagination'] = $this->pagination->create_links();
        $this->template_admin->display('jenis_pengeluaran/jenis_pengeluaran_list', $this->data);
    }

    public function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "admin/auth");
        }
    }

}

?>