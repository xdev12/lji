<?php

class Bank_account extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        $status = $this->uri->segment(5);
        $page = $this->uri->segment(6);
        define('CURRENT_CONTEXT', base_url() . 'admin/bank_account/'.@$status .(!empty($status)?'/':''). @$page);
        $this->data = array();
        init_generic_dao();
        $this->load->model(array('m_bank_account','m_bank'));
        $this->load->library(array('template_admin'));
        $this->logged_in();
        $this->data['page_title'] = "Bank Account";
        date_default_timezone_set("Asia/Jakarta");
    }

    private function validate() {			$this->form_validation->set_rules('bank_id', 'Bank Id', 'trim|required|max_length[10]|integer');
			$this->form_validation->set_rules('account_name', 'Account Name', 'trim|required|max_length[100]');
			$this->form_validation->set_rules('account_no', 'Account No', 'trim|required|max_length[50]|integer');
			$this->form_validation->set_rules('account_branch', 'Account Branch', 'trim|required|max_length[50]');
			$this->form_validation->set_rules('account_status', 'Account Status', 'trim|required|max_length[3]|integer');

        return $this->form_validation->run();
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
		$this->data['bank_list'] = $this->m_bank->fetch();
    }

    public function index($page = 1) {
        $this->preload();
        $this->session->set_userdata(array(
            'filter_bank_account' => array(
				'bank_id' => '',
				'account_name' => ''))
        );
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset);
    }

    public function fetch_record($keys) {
        $this->data['bank_account'] = $this->m_bank_account->by_id($keys);
    }

    private function fetch_data($limit, $offset, $key) {
        $this->data['bank_account'] = $this->m_bank_account->fetch($limit, $offset, null, true,null, null, $key);
        $this->data['total_rows'] = $this->m_bank_account->fetch(null,null, null, true,null, null, $key,true);
    }

    private function fetch_input() {
        $data = array('bank_id' => $this->input->post('bank_id'),
			'account_name' => $this->input->post('account_name'),
			'account_no' => $this->input->post('account_no'),
			'account_branch' => $this->input->post('account_branch'),
			'account_status' => $this->input->post('account_status'));

        return $data;
    }

    public function add() {
        $obj = $this->fetch_input();
        $obj['created_by'] = $this->session->userdata('username');
        $obj['created_on'] = date('Y-m-d H:i:s');

        if ($this->validate() != false) {
            $this->m_bank_account->insert($obj);
            $this->session->set_flashdata(array('message'=>'Data inserted successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = false;
            #set value
            $this->data['bank_account'] = (object) $obj;
            $this->template_admin->display('admin/bank_account/bank_account_insert', $this->data);
        }
    }

    /**

      @description
      viewing editing form. repopulation for every data needed in form done here.
     */
    public function edit($account_id) {
        $obj = $this->fetch_input();
        $obj['updated_by'] = $this->session->userdata('username');
        $obj['updated_on'] = date('Y-m-d H:i:s');

        $obj_id = array('account_id' => $account_id);

        if ($this->validate() != false) {
            $this->m_bank_account->update($obj, $obj_id);
            $this->session->set_flashdata(array('message'=>'Data edited successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = true;
            $this->fetch_record($obj_id);
            $this->template_admin->display('admin/bank_account/bank_account_insert', $this->data);
        }
    }

    /**
      @description
      viewing record. repopulation for every data needed for view.
     */
    public function detail($account_id) {
        $obj_id = array('account_id' => $account_id);

        $this->preload();
        $this->fetch_record($obj_id);
        $this->template_admin->display('bank_account/bank_account_detail', $this->data);
    }

    public function delete($account_id) {
        $obj_id = array('account_id' => $account_id);

        $this->m_bank_account->delete($obj_id);
		$this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        redirect(CURRENT_CONTEXT);
    }
	
	public function delete_multiple(){
        $data = file_get_contents('php://input');
        $id = json_decode($data);
		foreach($id->ids as $id){
			$obj_id = array('account_id' => $id->account_id);
			$this->m_bank_account->delete($obj_id);
		}
		$this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        echo json_encode(array('status'=>200));
    }

	public function search($page = 1) {
        $this->preload();
		$key = $this->session->userdata('filter_bank_account');
        if ($this->input->post('search')) {
            $key = array(
				'account_name' => $this->input->post('account_name')
            );
			$this->session->set_userdata(array('filter_bank_account' => $key));  
        }
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset, $key);
    }
	
    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->fetch_data($limit, $offset, $key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key))?'search':'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $this->data['offset'] = $offset;
        $this->data['pagination'] = $this->pagination->create_links();
        $this->template_admin->display('admin/bank_account/bank_account_list', $this->data);
    }

    public function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "admin/auth");
        }
    }

}

?>