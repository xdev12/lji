<?php

class offering extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'offering/');
        $this->data = array();
        init_generic_dao();
        $this->load->model(array('m_offering'));
        $this->load->library(array('template_admin'));
        $this->logged_in();
        $this->data['page_title'] = "Offering";
        date_default_timezone_set("Asia/Jakarta");
    }

    private function validate() {			$this->form_validation->set_rules('offer_to', 'offer_to', 'trim|required|max_length[100]');

        return $this->form_validation->run();
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
    }

    public function index($page = 1) {
        $this->preload();
        $this->session->set_userdata(array('filter_offering' => array(
				'offer_to' => ''))
        );
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset);
    }

    public function fetch_record($keys) {
        $this->data['offering'] = $this->m_offering->by_id($keys);
    }

    private function fetch_data($limit, $offset, $key) {
        $this->data['offering'] = $this->m_offering->fetch($limit, $offset, null, true,null, null, $key);
        $this->data['total_rows'] = $this->m_offering->fetch(null,null, null, true,null, null, $key,true);
    }

    private function fetch_input() {
        $data = array('offer_no' => $this->input->post('offer_no'),
                    'offer_to' => $this->input->post('offer_to'),
                    'offer_attn' => $this->input->post('offer_attn'),
                    'offer_calarie_value' => $this->input->post('offer_calarie_value'),
                    'offer_ash_content' => $this->input->post('offer_ash_content'),
                    'offer_sulfur' => $this->input->post('offer_sulfur'),
                    'offer_size' => $this->input->post('offer_size'),
                    'offer_qty' => $this->input->post('offer_qty'),
                    'offer_delivery' => $this->input->post('offer_delivery'),
                    'offer_price' => $this->input->post('offer_price'),
                    'offer_top1' => $this->input->post('offer_top1'),
                    'offer_top2' => $this->input->post('offer_top2'),
                    'offer_date' => $this->input->post('offer_date')
                    );

        return $data;
    }

    public function add() {
        $obj = $this->fetch_input();
        $obj['created_by'] = $this->session->userdata('username');
        $obj['created_on'] = date('Y-m-d H:i:s');

        if ($this->validate() != false) {
            $this->m_offering->insert($obj);
            $this->session->set_flashdata(array('message'=>'Data inserted successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = false;
            #set value
            $this->data['offering'] = (object) $obj;
            $this->template_admin->display('offering/offering_insert', $this->data);
        }
    }

    /**

      @description
      viewing editing form. repopulation for every data needed in form done here.
     */
    public function edit($offer_id) {
        $obj = $this->fetch_input();
        $obj['updated_by'] = $this->session->userdata('username');
        $obj['updated_on'] = date('Y-m-d H:i:s');

        $obj_id = array('offer_id' => $offer_id);

        if ($this->validate() != false) {
            $this->m_offering->update($obj, $obj_id);
            $this->session->set_flashdata(array('message'=>'Data edited successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = true;
            $this->fetch_record($obj_id);
            $this->template_admin->display('admin/offering/offering_insert', $this->data);
        }
    }

    public function export_pdf($offer_id){
        ob_start();
        $key = array('offer_id' => $offer_id);
        $this->fetch_record($key);
        $this->load->view('offering/offering_pdf', $this->data);
        $html = ob_get_contents();
        ob_end_clean();

        require_once('./assets/plugins/html2pdf/html2pdf.class.php');
        $pdf = new HTML2PDF('L','A4','en');
        $pdf->WriteHTML($html);
        $pdf->Output('Offering ['.date('Y-m-d').']' . '.pdf');
    }

    /**
      @description
      viewing record. repopulation for every data needed for view.
     */
    public function detail($offer_id) {
        $obj_id = array('offer_id' => $offer_id);

        $this->preload();
        $this->fetch_record($obj_id);
        $this->template_admin->display('admin/offering/offering_detail', $this->data);
    }

    public function delete($offer_id) {
        $obj_id = array('offer_id' => $offer_id);
        // $this->m_offering_account->delete($obj_id);
        $this->m_offering->delete($obj_id);
		$this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        redirect(CURRENT_CONTEXT);
    }
	
	public function delete_multiple(){
        $data = file_get_contents('php://input');
        $id = json_decode($data);
		foreach($id->ids as $id){
			$obj_id = array('offer_id' => $id->offer_id);
			$this->m_offering->delete($obj_id);
		}
		$this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        echo json_encode(array('status'=>200));
    }

	public function search($page = 1) {
        $this->preload();
		$key = $this->session->userdata('filter_offering');

        if ($this->input->post('search')) {
            $key = array(
                'offer_to' => $this->input->post('offer_to')
            );
			$this->session->set_userdata(array('filter_offering' => $key));  
        }
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset, $key);
    }
	
    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->fetch_data($limit, $offset, $key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key))?'search':'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $this->data['offset'] = $offset;
        $this->data['pagination'] = $this->pagination->create_links();
        $this->template_admin->display('offering/offering_list', $this->data);
    }

    public function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "admin/auth");
        }
    }

}

?>