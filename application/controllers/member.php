<?php

class Member extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        $status = $this->uri->segment(5);
        $page = $this->uri->segment(6);
        define('CURRENT_CONTEXT', base_url() . 'admin/member/'.@$status .(!empty($status)?'/':''). @$page);
        $this->data = array();
        init_generic_dao();
        $this->load->model(array('m_member','m_order'));
        $this->load->library(array('template_admin', 'upload'));
        $this->logged_in();
        // $this->load->library('rest', array('server' => server_url()));
        $this->data['page_title'] = "Member";
        $this->path = "upload/member_photo/";
        date_default_timezone_set("Asia/Jakarta");
    }

    private function validate() {           
            $edit = $this->data['edit'] = false;
            $this->form_validation->set_rules('member_status', 'Member Status', 'trim|required|max_length[3]|integer');

        return $this->form_validation->run();
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
    }

    public function index($page = 1) {
        $this->preload();
        $this->session->set_userdata(array('filter_member' => array(
				'member_fullname' => '',
				'member_email' => ''))
        );
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset);
    }

    public function fetch_record($keys) {
        // $this->data['member_addr'] = $this->m_member_address->fetch();
        $this->data['member'] = $this->m_member->by_id($keys);
    }

    private function fetch_data($limit, $offset, $key) { 
        
        // print_r($key); die();
        $this->data['member'] = $this->m_member->fetch($limit, $offset, null, true, null, null);
        $this->data['total_rows'] = $this->m_member->fetch($limit, $offset, null, true, null, null, null, true);
    }

    private function fetch_input() {
        $data = array(
   //          'member_username' => $this->input->post('member_username'),
			// 'member_fullname' => $this->input->post('member_fullname'),
			// 'member_password' => md5($this->input->post('member_password')),
			// 'member_email' => $this->input->post('member_email'),
			// 'member_birthday' => $this->input->post('member_birthday'),
			// 'member_gender' => $this->input->post('member_gender'),
			// 'member_isreseller' => $this->input->post('member_isreseller'),
			// 'member_reseller_discount' => $this->input->post('member_reseller_discount'),
			// 'member_reseller_ispercent' => $this->input->post('member_reseller_ispercent'),
   //          'member_banned_note' => $this->input->post('member_banned_note'),
			'member_status' => $this->input->post('member_status'));
        return $data;
    }

    public function check_username($username = null){
        if(!empty($username)){
            $result = $this->m_member->fetch(null,null,null,true,null,array('member_username'=>$username),null,false,true,false);
            return $result;
        }
    }


    /**

      @description
      viewing editing form. repopulation for every data needed in form done here.
     */
    // public function edit($member_id) {
    //     $obj = $this->fetch_input();
    //     $obj['updated_by'] = $this->session->userdata('username');
    //     $obj['updated_on'] = date('Y-m-d H:i:s');
    //     // $obj['member_photo'] = $this->input->post('member_photo');
    //     $obj_id = array('member_id' => $member_id);
    //     if ($this->validate() != false) {
    //         $this->m_member->update($obj, $obj_id);
    //         $this->session->set_flashdata(array('message'=>'Data edited successfully.','type_message'=>'success'));
    //         redirect(CURRENT_CONTEXT);
    //     } else {
    //         $this->preload();
    //         $this->data['edit'] = true;
    //         $this->data['reseller'] = false;
    //         $this->fetch_record($obj_id);
    //         // $this->data['province'] = $this->rest->get('province');
    //         // $this->data['member_addr'] = $this->m_member_address->fetch(null,null,null,false,null,$obj_id);
    //         // print_r($this->data['member_addr']);die();
    //         $this->template_admin->display('member/member_insert', $this->data);
    //     }
    // }

    public function banned($member_id) {
        $obj['member_status'] = 2;
        $obj['updated_by'] = $this->session->userdata('username');
        $obj['updated_on'] = date('Y-m-d H:i:s');
        $obj_id = array('member_id' => $member_id);
        $this->m_member->update($obj, $obj_id);
        $this->session->set_flashdata(array('message' => 'Data berhasil disimpan.', 'type_message' => 'success'));
        redirect(CURRENT_CONTEXT);
    }

    /**
      @description
      viewing record. repopulation for every data needed for view.
     */
    public function detail($member_id) {
        $obj_id = array('member_id' => $member_id);

        $this->preload();
        $this->fetch_record($obj_id);
        $this->template_admin->display('admin/member/member_detail', $this->data);
    }

    public function delete($member_id) {
        $obj_id = array('member_id' => $member_id);
        $this->m_order->delete($obj_id);
        // $this->m_order_product->delete($obj_id);
        // $this->m_member_address->delete($obj_id);
        $old = $this->m_member->by_id($obj_id);
        @unlink("./".$old->member_photo);
        $this->m_member->delete($obj_id);
		$this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        redirect(CURRENT_CONTEXT);
    }
	
	public function delete_multiple(){
        $data = file_get_contents('php://input');
        $id = json_decode($data);
		foreach($id->ids as $id){
			$obj_id = array('member_id' => $id->member_id);
            $this->m_order->delete($obj_id);
            $this->m_member_address->delete($obj_id);
            $old = $this->m_member->by_id($obj_id);
            @unlink("./".$old->member_photo);
            $this->m_member->delete($obj_id);
		}
		$this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        echo json_encode(array('status'=>200));
    }

	public function search($page = 1) {
        // echo "heloo"; die();
        $this->preload();
        
		$key = $this->session->userdata('filter_member');
        if ($this->input->post('search')) {
            $key = array(
                'member_fullname' => $this->input->post('member_fullname'),
				'member_email' => $this->input->post('member_email')
            );
            //print_r($key);
			$this->session->set_userdata(array('filter_member' => $key));  
        }
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset, $key);
    }
	
    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->fetch_data($limit, $offset, $key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key))?'search':'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $this->data['offset'] = $offset;
        $this->data['pagination'] = $this->pagination->create_links();
        $this->template_admin->display('admin/member/member_list', $this->data);
    }

    public function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "admin/auth");
        }
    }

    public function get_city($province) {
        $data = array("province" => $province);
        $result = $this->rest->get('city', $data);
        if ($result->rajaongkir->status->code == 200) {
            echo json_encode($result->rajaongkir->results);
        }
    }

    private function debug($obj){
        echo "<pre>";
        print_r($obj);
        echo "</pre>";
        die();

    }

}

?>