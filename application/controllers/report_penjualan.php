<?php

class report_penjualan extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'report_penjualan/');
        $this->data = array();
        init_generic_dao();
        $this->load->model(array('m_penjualan','m_pembelian','m_jenis_penjualan'));
        $this->load->library(array('template_admin'));
        $this->logged_in();
        $this->data['page_title'] = "penjualan";
        date_default_timezone_set("Asia/Jakarta");
        $this->path_invoice = "upload/invoice_penjualan";
        $this->path_bukti = "upload/buktibayar_penjualan";
    }

    private function validate() {           
        $this->form_validation->set_rules('penjualan_foto_invoice', 'Foto', 'callback_upload_foto');
        $this->form_validation->set_rules('penjualan_bukti_bayar', 'Bukti Bayar', 'callback_upload_bukti');

        return $this->form_validation->run();
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
        $this->data['jenis_penjualan_list'] = $this->m_jenis_penjualan->fetch();
    }

    public function index($page = 1) {
        $this->preload();
        $this->session->set_userdata(array('filter_penjualan' => array(
                'penjualan_bulan' => '',
                'penjualan_tahun' => '',
                'penjualan_jenis' => ''))
        );
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset);
    }

    public function fetch_record($keys) {
        $this->data['penjualan'] = $this->m_penjualan->by_id($keys);
    }

    private function fetch_data($limit, $offset, $key) {
        $this->data['penjualan'] = $this->m_penjualan->get_data($key, $limit, $offset);
        $this->data['total_rows'] = count($this->data['penjualan']);
    }


    private function fetch_input() {
        $data = array('penjualan_jenis' => $this->input->post('penjualan_jenis'),
                    'penjualan_tgl_invoice' => $this->input->post('penjualan_tgl_invoice'),
                    'penjualan_no_invoice' => $this->input->post('penjualan_no_invoice'),
                    'penjualan_uraian_barang' => $this->input->post('penjualan_uraian_barang'),
                    'penjualan_nama_perusahaan' => $this->input->post('penjualan_nama_perusahaan'),
                    'penjualan_nama_tug_boat' => $this->input->post('penjualan_nama_tug_boat'),
                    'penjualan_nama_tongkang' => $this->input->post('penjualan_nama_tongkang'),
                    'penjualan_tonase' => $this->input->post('penjualan_tonase'),
                    'penjualan_harga_satuan' => $this->input->post('penjualan_harga_satuan'),
                    'penjualan_jumlah_total' => $this->input->post('penjualan_jumlah_total'),
                    'penjualan_foto_invoice' => $this->input->post('penjualan_foto_invoice'),
                    'penjualan_bukti_bayar' => $this->input->post('penjualan_bukti_bayar'),
                    );

        return $data;
    }

    public function search($page = 1) {
        $this->preload();
        $key = $this->session->userdata('filter_penjualan');

        if ($this->input->post('search')) {
            $key = array(
                'penjualan_bulan' => $this->input->post('penjualan_bulan'),
                'penjualan_tahun' => $this->input->post('penjualan_tahun'),
                'penjualan_jenis' => $this->input->post('penjualan_jenis')
            );
            $this->session->set_userdata(array('filter_penjualan' => $key));  
        }
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset, $key);
    }
    
    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->fetch_data($limit, $offset, $key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key))?'search':'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $this->data['offset'] = $offset;
        $this->data['pagination'] = $this->pagination->create_links();
        $this->template_admin->display('report/penjualan', $this->data);
    }

    public function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "auth");
        }
    }

}

?>