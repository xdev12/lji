<?php

class roa extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'roa/');
        $this->data = array();
        init_generic_dao();
        $this->load->model(array('m_roa'));
        $this->load->library(array('template_admin'));
        $this->logged_in();
        $this->data['page_title'] = "roa";
        date_default_timezone_set("Asia/Jakarta");
        $this->path = "upload/roa_file";
    }

    private function validate() {			
        $this->form_validation->set_rules('roa_desc', 'roa_desc', 'trim|required|max_length[100]');
        $this->form_validation->set_rules('roa_file', 'File', 'callback_upload_pdf');

        return $this->form_validation->run();
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
    }

    public function index($page = 1) {
        $this->preload();
        $this->session->set_userdata(array('filter_roa' => array(
				'roa_desc' => ''))
        );
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset);
    }

    public function fetch_record($keys) {
        $this->data['roa'] = $this->m_roa->by_id($keys);
    }

    private function fetch_data($limit, $offset, $key) {
        $this->data['roa'] = $this->m_roa->fetch($limit, $offset, null, true,null, null, $key);
        $this->data['total_rows'] = $this->m_roa->fetch(null,null, null, true,null, null, $key,true);
    }

    private function fetch_input() {
        $data = array('roa_desc' => $this->input->post('roa_desc'),
                    'roa_file' => $this->input->post('roa_file')
                    );

        return $data;
    }

    public function add() {
        $obj = $this->fetch_input();
        $obj['created_by'] = $this->session->userdata('username');
        $obj['created_on'] = date('Y-m-d H:i:s');

        if ($this->validate() != false) {
            $file = $this->file_link;
            if(!empty($file)){
                $obj['roa_file'] = $file;
            }
            $this->m_roa->insert($obj);
            $this->session->set_flashdata(array('message'=>'Data inserted successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = false;
            #set value
            $this->data['roa'] = (object) $obj;
            $this->template_admin->display('roa/roa_insert', $this->data);
        }
    }

    function upload_pdf() {
        $param = $_FILES['roa_file'];
        if (!empty($param['name'])) {
            $_FILES['userfile'] = $param;
            $config = array();
            $config['upload_path'] = "./" . $this->path;
            $config['allowed_types'] = 'jpg|png|gif|jpeg|pdf';
            $config['encrypt_name'] = TRUE;
            $config['max_size'] = '100000';
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload()) {
                $upload_data = $this->upload->data();
                $this->file_link = $this->path .'/'. $upload_data['file_name'];
                return true;
            } else {
                $this->form_validation->set_message('handle_upload', $this->upload->display_errors());
                return false;
            }
        } else {
            $this->file_link = null;
            return true;
        }
    }

    /**

      @description
      viewing editing form. repopulation for every data needed in form done here.
     */
    public function edit($offer_id) {
        $obj = $this->fetch_input();
        $obj['updated_by'] = $this->session->userdata('username');
        $obj['updated_on'] = date('Y-m-d H:i:s');

        $obj_id = array('offer_id' => $offer_id);

        if ($this->validate() != false) {
            $this->m_roa->update($obj, $obj_id);
            $this->session->set_flashdata(array('message'=>'Data edited successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = true;
            $this->fetch_record($obj_id);
            $this->template_admin->display('admin/roa/roa_insert', $this->data);
        }
    }

    /**
      @description
      viewing record. repopulation for every data needed for view.
     */
    public function detail($offer_id) {
        $obj_id = array('offer_id' => $offer_id);

        $this->preload();
        $this->fetch_record($obj_id);
        $this->template_admin->display('admin/roa/roa_detail', $this->data);
    }

    public function delete($offer_id) {
        $obj_id = array('offer_id' => $offer_id);
        // $this->m_roa_account->delete($obj_id);
        $this->m_roa->delete($obj_id);
		$this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        redirect(CURRENT_CONTEXT);
    }
	
	public function delete_multiple(){
        $data = file_get_contents('php://input');
        $id = json_decode($data);
		foreach($id->ids as $id){
			$obj_id = array('offer_id' => $id->offer_id);
			$this->m_roa->delete($obj_id);
		}
		$this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        echo json_encode(array('status'=>200));
    }

	public function search($page = 1) {
        $this->preload();
		$key = $this->session->userdata('filter_roa');

        if ($this->input->post('search')) {
            $key = array(
                'offer_to' => $this->input->post('offer_to')
            );
			$this->session->set_userdata(array('filter_roa' => $key));  
        }
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset, $key);
    }
	
    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->fetch_data($limit, $offset, $key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key))?'search':'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $this->data['offset'] = $offset;
        $this->data['pagination'] = $this->pagination->create_links();
        $this->template_admin->display('roa/roa_list', $this->data);
    }

    public function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "admin/auth");
        }
    }

}

?>