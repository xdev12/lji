<?php

class Review extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'admin/review/');
        $this->data = array();
        init_generic_dao();
        $this->load->model(array('m_product_review','m_product'));
        $this->load->library(array('template_admin'));
        $this->logged_in();
        $this->data['page_title'] = "Role";
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
    }

    public function index($page = 1) {
        $this->preload();
        $this->session->set_userdata(array(
            'filter_role' => array(
                'role_name' => '',
                'role_status' => ''))
        );
        $offset = ($page - 1) * $this->limit;
        $this->data['offset'] = $offset;
        $this->get_list($this->limit, $offset);
    }


    private function fetch_data($limit, $offset, $key) {
        $this->data['review'] = $this->m_product_review->fetch($limit, $offset, null, true, null, null, $key);
        $this->data['total_rows'] = $this->m_product_review->fetch(null, null, null, true, null, null, $key, true);
    }

    private function fetch_input() {
        $data = array('role_name' => $this->input->post('role_name'),
            'role_status' => $this->input->post('role_status'),
            'role_canlogin' => $this->input->post('role_canlogin'),
            'created_at' => $this->input->post('created_at'),
            'updated_at' => $this->input->post('updated_at'));

        return $data;
    }

    public function publish($review_id){
        $obj_id = array('review_id' => $review_id);
        $this->m_product_review->update(array('review_status' => 1), $obj_id);
        redirect(CURRENT_CONTEXT);
    }

    public function block($review_id){
        $obj_id = array('review_id' => $review_id);
        $this->m_product_review->update(array('review_status' => 2), $obj_id);
        redirect(CURRENT_CONTEXT);
    }


    /**

      @description
      viewing editing form. repopulation for every data needed in form done here.
     */
    public function edit($role_id) {
        $obj = $this->fetch_input();
        $obj['updated_by'] = $this->session->userdata('username');
        $obj['updated_at'] = date('Y-m-d H:i:s');

        $obj_id = array('role_id' => $role_id);
        $id = $role_id;
        $this->menu_id($id);
        $parent = $this->input->post('parent');
        $child = $this->input->post('child');
        $grandchild = $this->input->post('grandchild');

        if ($this->validate() != false) {
            $this->m_role->update($obj, $obj_id);
            $this->m_role_menu->delete($obj_id);

            //insert menu
            if(!empty($parent)){ $this->insert_menu($parent, $role_id, true); }
            if(!empty($child)){ $this->insert_menu($child, $role_id, true); }
            if(!empty($grandchild)){ $this->insert_menu($grandchild, $role_id, true); }

            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = true;
            $this->allmenu();
            $this->fetch_record($obj_id);
            $this->template_admin->display('admin/role/role_insert', $this->data);
        }
    }

    /**
      @description
      viewing record. repopulation for every data needed for view.
     */
    public function detail($role_id) {
        $obj_id = array('role_id' => $role_id);

        $this->preload();
        $this->fetch_record($obj_id);
        $this->menu_role($role_id);

        $this->template_admin->display('admin/role/role_detail', $this->data);
    }


    public function delete($role_id) {
        $obj_id = array('role_id' => $role_id);
        $obj = array('updated_by' => $this->session->userdata('username'), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1);
        $this->m_role->update($obj, $obj_id);
        $this->session->set_flashdata(array('message' => 'Data berhasil dihapus.', 'type_message' => 'success'));
        redirect(CURRENT_CONTEXT);
    }

    public function delete_multiple() {
        $data = file_get_contents('php://input');
        $id = json_decode($data);
        $obj = array('updated_by' => $this->session->userdata('username'), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1);
        foreach ($id->ids as $id) {
            $obj_id = array('role_id' => $id->role_id);
            $this->m_role->update($obj,$obj_id);
        }
        $this->session->set_flashdata(array('message' => 'Data berhasil dihapus.', 'type_message' => 'success'));
        echo json_encode(array('status' => 200));
    }

    public function search($page = 1) {
        $this->preload();
        $key = $this->session->userdata('filter_role');
        if ($this->input->post('search')) {
            $key = array(
                'role_name' => $this->input->post('role_name'),
                'role_status' => $this->input->post('role_status')
            );
            $this->session->set_userdata(array('filter_role' => $key));
        }
        $offset = ($page - 1) * $this->limit;
        $this->data['offset'] = $offset;
        $this->get_list($this->limit, $offset, $key);
    }

    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->fetch_data($limit, $offset, $key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key)) ? 'search' : 'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->template_admin->display('admin/product/product_review', $this->data);
    }

    function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "auth");
        }
    }

}

?>