<?php
class M_pjbb_beli extends Generic_dao {

    public function table_name() {
        return Tables::$pjbb_beli;
    }

    public function field_map() {
		return array(
			'pbeli_id' => 'pbeli_id',
			'pbeli_no' => 'pbeli_no',
			'pbeli_nama' => 'pbeli_nama',
			'pbeli_perusahaan' => 'pbeli_perusahaan',
			'pbeli_alamat' => 'pbeli_alamat',
			'pbeli_spek' => 'pbeli_spek',
			'pbeli_kondisi' => 'pbeli_kondisi',
			'pbeli_asal_batu' => 'pbeli_asal_batu',
			'pbeli_alamat_asal_batu' => 'pbeli_alamat_asal_batu',
			'pbeli_jumlah_muatan' => 'pbeli_jumlah_muatan',
			'pbeli_harga_perton' => 'pbeli_harga_perton',
			'pbeli_toleransi' => 'pbeli_toleransi',
			'pbeli_pelabuhan_muat' => 'pbeli_pelabuhan_muat',
			'pbeli_pelabuhan_bongkar' => 'pbeli_pelabuhan_bongkar',
			'pbeli_pembayaran1' => 'pbeli_pembayaran1',
			'pbeli_pembayaran2' => 'pbeli_pembayaran2',
			'pbeli_pembayaran3' => 'pbeli_pembayaran3',
			'pbeli_status' => 'pbeli_status',
			'pbeli_ttd' => 'pbeli_ttd',
			'pbeli_nama_rek' => 'pbeli_nama_rek',
			'pbeli_no_rek' => 'pbeli_no_rek',
			'pbeli_bank' => 'pbeli_bank',
			'created_by' => 'created_by',
			'created_on' => 'created_on',
			'updated_by' => 'updated_by',
			'updated_on' => 'updated_on'
		);
    }

    public function __construct() {
        parent::__construct();
    }

    function get_data($key, $limit = 0, $offset = 0){
		$month = (!empty($key['month']) && !empty($key['year']))?" Month(created_on) = ".$key['month']." && Year(created_on) = ".$key['year'].(!empty($key['pjual_nama'])?"||":"") : "";
		$buyer = (!empty($key['pbeli_nama']))?" pbeli_nama = '".$key['pbeli_nama']."'" : "";
    	
    	$sql = "select * from pjbb_beli ".(!empty($key)?"where":"").$month.$buyer." limit ".$offset.",".$limit;
    	// print_r($sql);die();
        $query = $this->ci->db->query($sql);
        return $query->result();
    }

}

?>