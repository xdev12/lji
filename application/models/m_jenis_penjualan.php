<?php
class M_jenis_penjualan extends Generic_dao {

    public function table_name() {
        return Tables::$jenis_penjualan;
    }


    public function field_map() {
		return array(
			'jpj_id' => 'jpj_id',
			'jpj_nama' => 'jpj_nama',
			'jpj_deskripsi' => 'jpj_deskripsi',
			'created_by' => 'created_by',
			'created_on' => 'created_on',
			'updated_by' => 'updated_by',
			'updated_on' => 'updated_on'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>