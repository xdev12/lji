<?php
class M_offering extends Generic_dao {

    public function table_name() {
        return Tables::$offering;
    }

    public function field_map() {
		return array(
			'offer_id' => 'offer_id',
			'offer_no' => 'offer_no',
			'offer_to' => 'offer_to',
			'offer_attn' => 'offer_attn',
			'offer_calarie_value' => 'offer_calarie_value',
			'offer_ash_content' => 'offer_ash_content',
			'offer_sulfur' => 'offer_sulfur',
			'offer_size' => 'offer_size',
			'offer_qty' => 'offer_qty',
			'offer_delivery' => 'offer_delivery',
			'offer_price' => 'offer_price',
			'offer_top1' => 'offer_top1',
			'offer_top2' => 'offer_top2',
			'offer_date' => 'offer_date',
			'created_by' => 'created_by',
			'created_on' => 'created_on',
			'updated_by' => 'updated_by',
			'updated_on' => 'updated_on'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>