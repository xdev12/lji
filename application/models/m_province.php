<?php
class M_province extends Generic_dao {

    public function table_name() {
        return Tables::$province;
    }

    public function field_map() {
		return array(
			'province_id' => 'province_id',
			'province_name' => 'province_name',
			'created_by' => 'created_by',
			'created_on' => 'created_on',
			'updated_by' => 'updated_by',
			'updated_on' => 'updated_on'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>