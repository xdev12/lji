<?php
class M_invoice_asuransi extends Generic_dao {

    public function table_name() {
        return Tables::$invoice_asuransi;
    }

    public function field_map() {
		return array(
			'inac_id' => 'inac_id',
			'inac_tgl_invoice' => 'inac_tgl_invoice',
			'inac_no_invoice' => 'inac_no_invoice',
			'inac_nama_perusahaan' => 'inac_nama_perusahaan',
			'inac_jumlah' => 'inac_jumlah',
			'inac_foto_invoice' => 'inac_foto_invoice',
			'inac_bukti_bayar' => 'inac_bukti_bayar',
			'created_by' => 'created_by',
			'created_on' => 'created_on',
			'updated_by' => 'updated_by',
			'updated_on' => 'updated_on'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>