<?php
class M_penjualan extends Generic_dao {

    public function table_name() {
        return Tables::$penjualan;
    }


    public function field_map() {
		return array(
			'penjualan_id' => 'penjualan_id',
			'penjualan_jenis' => 'penjualan_jenis',
			'penjualan_tgl_invoice' => 'penjualan_tgl_invoice',
			'penjualan_no_invoice' => 'penjualan_no_invoice',
			'penjualan_nama_perusahaan' => 'penjualan_nama_perusahaan',
			'penjualan_uraian_barang' => 'penjualan_uraian_barang',
			'penjualan_nama_tug_boat' => 'penjualan_nama_tug_boat',
			'penjualan_nama_tongkang' => 'penjualan_nama_tongkang',
			'penjualan_tonase' => 'penjualan_tonase',
			'penjualan_harga_satuan' => 'penjualan_harga_satuan',
			'penjualan_jumlah_total' => 'penjualan_jumlah_total',
			'penjualan_foto_invoice' => 'penjualan_foto_invoice',
			'penjualan_bukti_bayar' => 'penjualan_bukti_bayar',
			'created_by' => 'created_by',
			'created_on' => 'created_on',
			'updated_by' => 'updated_by',
			'updated_on' => 'updated_on'
		);
    }

    public function __construct() {
        parent::__construct();
    }

    public function joined_table() {
        return array(
            array(
                'table_name' => Tables::$jenis_penjualan,
                'condition' => Tables::$jenis_penjualan . '.jpj_id = ' . $this->table_name() . '.penjualan_jenis',
                'field' => 'jpj_nama'
            )
        );
    }

    function get_data($key, $limit = 0, $offset = 0){
		$month = (!empty($key['penjualan_bulan']) && !empty($key['penjualan_tahun']))?" Month(penjualan_tgl_invoice) = ".$key['penjualan_bulan']." && Year(penjualan_tgl_invoice) = ".$key['penjualan_tahun'].(!empty($key['penjualan_jenis'])?"&&":"") : "";
		$jenis = (!empty($key['penjualan_jenis']))?" penjualan_jenis = '".$key['penjualan_jenis']."'" : "";
    	
    	$sql = "select * from penjualan inner join jenis_penjualan on jpj_id = penjualan_jenis ".(!empty($key)?"where":"").$month.$jenis." limit ".$offset.",".$limit;
    	// print_r($sql);die();
        $query = $this->ci->db->query($sql);
        return $query->result();
    }

}

?>