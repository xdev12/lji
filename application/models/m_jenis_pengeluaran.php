<?php
class M_jenis_pengeluaran extends Generic_dao {

    public function table_name() {
        return Tables::$jenis_pengeluaran;
    }


    public function field_map() {
		return array(
			'jp_id' => 'jp_id',
			'jp_nama' => 'jp_nama',
			'jp_deskripsi' => 'jp_deskripsi',
			'created_by' => 'created_by',
			'created_on' => 'created_on',
			'updated_by' => 'updated_by',
			'updated_on' => 'updated_on'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>