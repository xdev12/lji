<?php
class M_member extends Generic_dao {

    public function table_name() {
        return Tables::$member;
    }

    public function field_map() {
		return array(
			'member_id' => 'member_id',
			'member_fullname' => 'member_fullname',
			'member_password' => 'member_password',
			'member_email' => 'member_email',
            'member_phone' => 'member_phone',
            'member_login' => 'member_login',
			'member_status' => 'member_status',
            'member_banned_note' => 'member_banned_note',
            'member_address' => 'member_address',
			'created_by' => 'created_by',
			'created_on' => 'created_on',
			'updated_by' => 'updated_by',
			'updated_on' => 'updated_on',
            'province_id' => 'province_id',
            'city_id' => 'city_id'
		); 
    }

    public function __construct() {
        parent::__construct();
    }

    function check_login($username, $password, $member_status)
    {
        $password = md5($password);
        $query = $this->ci->db->query("select * from ".$this->table_name()." where member_email='$username' and member_password='$password' and member_status='$member_status'");
        // print_r($this->ci->db->last_query());die();
        return ($query->num_rows() == 1);
    }

    function check_username($username, $member_status)
    {
        $query = $this->ci->db->query("select * from ".$this->table_name()." where member_username='$username' and member_status='$member_status'");
        return ($query->num_rows() == 1);
    }

    function check_name($username, $password, $member_status)
    {
        $password = md5($password);
        $query = $this->ci->db->query("select * from ".$this->table_name()." where member_username='$username' and member_password='$password' and member_status='$member_status'");
    }

    public function UpdateData($table, $data, $where) {
        return $this->ci->db->update($table, $data, $where);
        
    }

    //  public function joined_table() 
    //  {
    //     return array(
    //         array(
    //             'table_name' => Tables::$member_address,
    //             'condition' => Tables::$member_address . '.member_id = ' . $this->table_name() . '.member_id',
    //             'field' => 
    //         )
    //     );
    // }


    // public function joined_table() {
    //     return array(
    //         array(
    //             'table_name' => Tables::$member_address,
    //             'condition' => Tables::$member_address . '.member_id = ' . $this->table_name() . '.member_id',
    //             'field' => 'member_fullname as member_fullname'
    //         ),
    //         array(
    //             'table_name' => Tables::$city,
    //             'condition' => Tables::$city . '.city_id = ' . $this->table_name() . '.city_id',
    //             'field' => 'city_name as city'
    //         ),
    //         array(
    //             'table_name' => Tables::$province,
    //             'condition' => Tables::$province . '.province_id = ' . $this->table_name() . '.province_id',
    //             'field' => 'province_name as province'
    //         )
    //     );
    // }

    public function joined_table() {
        return array(
            array(
                'table_name' => Tables::$city,
                'condition' => Tables::$city . '.city_id = ' . $this->table_name() . '.city_id',
                'field' => 'city_name as city',
                'direction' => 'left'
            ),
            array(
                'table_name' => Tables::$province,
                'condition' => Tables::$province . '.province_id = ' . $this->table_name() . '.province_id',
                'field' => 'province_name as province',
                'direction' => 'left'
            )
        );
    }



}

?>