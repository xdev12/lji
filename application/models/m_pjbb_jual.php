<?php
class M_pjbb_jual extends Generic_dao {

    public function table_name() {
        return Tables::$pjbb_jual;
    }

    public function field_map() {
		return array(
			'pjual_id' => 'pjual_id',
			'pjual_no' => 'pjual_no',
			'pjual_nama' => 'pjual_nama',
			'pjual_perusahaan' => 'pjual_perusahaan',
			'pjual_alamat' => 'pjual_alamat',
			'pjual_spek' => 'pjual_spek',
			'pjual_kondisi' => 'pjual_kondisi',
			'pjual_asal_batu' => 'pjual_asal_batu',
			'pjual_alamat_asal_batu' => 'pjual_alamat_asal_batu',
			'pjual_jumlah_muatan' => 'pjual_jumlah_muatan',
			'pjual_harga_perton' => 'pjual_harga_perton',
			'pjual_toleransi' => 'pjual_toleransi',
			'pjual_pelabuhan_muat' => 'pjual_pelabuhan_muat',
			'pjual_pelabuhan_bongkar' => 'pjual_pelabuhan_bongkar',
			'pjual_pembayaran1' => 'pjual_pembayaran1',
			'pjual_pembayaran2' => 'pjual_pembayaran2',
			'pjual_pembayaran3' => 'pjual_pembayaran3',
			'pjual_status' => 'pjual_status',
			'pjual_ttd' => 'pjual_ttd',
			'created_by' => 'created_by',
			'created_on' => 'created_on',
			'updated_by' => 'updated_by',
			'updated_on' => 'updated_on'
		);
    }

    function get_data($key, $limit = 0, $offset = 0){
		$month = (!empty($key['month']) && !empty($key['year']))?" Month(created_on) = ".$key['month']." && Year(created_on) = ".$key['year'].(!empty($key['pjual_nama'])?"||":"") : "";
		$buyer = (!empty($key['pjual_nama']))?" pjual_nama = '".$key['pjual_nama']."'" : "";
    	
    	$sql = "select * from pjbb_jual ".(!empty($key)?"where":"").$month.$buyer." limit ".$offset.",".$limit;
    	// print_r($sql);die();
        $query = $this->ci->db->query($sql);
        return $query->result();
    }

    public function __construct() {
        parent::__construct();
    }

}

?>