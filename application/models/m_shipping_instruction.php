<?php
class M_shipping_instruction extends Generic_dao {

    public function table_name() {
        return Tables::$shipping_instruction;
    }

    public function field_map() {
		return array(
			'si_id' => 'si_id',
			'si_no' => 'si_no',
			'si_pjbb_no' => 'si_pjbb_no',
			'si_to' => 'si_to',
			'si_shipper' => 'si_shipper',
			'si_consignee' => 'si_consignee',
			'si_notif_address' => 'si_notif_address',
			'si_pol' => 'si_pol',
			'si_pod' => 'si_pod',
			'si_date_load' => 'si_date_load',
			'si_vessel' => 'si_vessel',
			'si_cargo' => 'si_cargo',
			'si_desc' => 'si_desc',
			'si_mark' => 'si_mark',
			'si_agen_kapal' => 'si_agen_kapal',
			'si_status' => 'si_status',
			'created_by' => 'created_by',
			'created_on' => 'created_on',
			'updated_by' => 'updated_by',
			'updated_on' => 'updated_on'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>