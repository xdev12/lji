<?php
class M_roa extends Generic_dao {

    public function table_name() {
        return Tables::$roa;
    }

    public function field_map() {
		return array(
			'roa_id' => 'roa_id',
			'roa_desc' => 'roa_desc',
			'roa_file' => 'roa_file',
			'roa_status' => 'roa_status',
			'created_by' => 'created_by',
			'created_on' => 'created_on',
			'updated_by' => 'updated_by',
			'updated_on' => 'updated_on'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>