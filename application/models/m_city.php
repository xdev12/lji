<?php
class M_city extends Generic_dao {

    public function table_name() {
        return Tables::$city;
    }

    public function field_map() {
		return array(
			'province_id' => 'province_id',
			'city_id' => 'city_id',
			'city_name' => 'city_name',
			'created_by' => 'created_by',
			'created_on' => 'created_on',
			'updated_by' => 'updated_by',
			'updated_on' => 'updated_on'
		);
    }

    public function __construct() {
        parent::__construct();
    }


 
 	public function joined_table() {
        return array(
            array(
                'table_name' => Tables::$province,
                'condition' => Tables::$province . '.province_id = ' . $this->table_name() . '.province_id',
                'field' => 'province_name'
            )
        );
    }
}

?>