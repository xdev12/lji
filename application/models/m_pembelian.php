<?php
class M_pembelian extends Generic_dao {

    public function table_name() {
        return Tables::$pembelian;
    }

    public function field_map() {
		return array(
			'pembelian_id' => 'pembelian_id',
			'pembelian_jenis' => 'pembelian_jenis',
			'pembelian_tgl_invoice' => 'pembelian_tgl_invoice',
			'pembelian_no_invoice' => 'pembelian_no_invoice',
			'pembelian_ket' => 'pembelian_ket',
			'pembelian_total' => 'pembelian_total',
			'created_by' => 'created_by',
			'created_on' => 'created_on',
			'updated_by' => 'updated_by',
			'updated_on' => 'updated_on'
		);
    }

    public function __construct() {
        parent::__construct();
    }

    function get_data($key, $limit = 0, $offset = 0){
		$month = (!empty($key['pembelian_bulan']) && !empty($key['pembelian_tahun']))?" Month(pembelian_tgl_invoice) = ".$key['pembelian_bulan']." && Year(pembelian_tgl_invoice) = ".$key['pembelian_tahun'].(!empty($key['pembelian_jenis'])?"&&":"") : "";
		$jenis = (!empty($key['pembelian_jenis']))?" pembelian_jenis = '".$key['pembelian_jenis']."'" : "";
    	
    	$sql = "select * from pembelian ".(!empty($key)?"where":"").$month.$jenis." limit ".$offset.",".$limit;
    	// print_r($sql);die();
        $query = $this->ci->db->query($sql);
        return $query->result();
    }

}

?>