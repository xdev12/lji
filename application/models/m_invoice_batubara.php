<?php
class M_invoice_batubara extends Generic_dao {

    public function table_name() {
        return Tables::$invoice_batubara;
    }

    public function field_map() {
		return array(
			'inbb_id' => 'inbb_id',
			'inbb_tgl_invoice' => 'inbb_tgl_invoice',
			'inbb_no_invoice' => 'inbb_no_invoice',
			'inbb_nama_perusahaan' => 'inbb_nama_perusahaan',
			'inbb_uraian_barang' => 'inbb_uraian_barang',
			'inbb_tonase' => 'inbb_tonase',
			'inbb_harga_satuan' => 'inbb_harga_satuan',
			'inbb_jumlah_total' => 'inbb_jumlah_total',
			'inbb_foto_invoice' => 'inbb_foto_invoice',
			'inbb_bukti_bayar' => 'inbb_bukti_bayar',
			'created_by' => 'created_by',
			'created_on' => 'created_on',
			'updated_by' => 'updated_by',
			'updated_on' => 'updated_on'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>