<?php
class M_invoice_sewa_tongkang extends Generic_dao {

    public function table_name() {
        return Tables::$invoice_sewa_tongkang;
    }

    public function field_map() {
		return array(
			'inst_id' => 'inst_id',
			'inst_tgl_invoice' => 'inst_tgl_invoice',
			'inst_no_invoice' => 'inst_no_invoice',
			'inst_no_spal' => 'inst_no_spal',
			'inst_nama_perusahaan' => 'inst_nama_perusahaan',
			'inst_nama_tug_boat' => 'inst_nama_tug_boat',
			'inst_nama_tongkang' => 'inst_nama_tongkang',
			'inst_tonase' => 'inst_tonase',
			'inst_harga' => 'inst_harga',
			'inst_jumlah_total' => 'inst_jumlah_total',
			'inst_foto_invoice' => 'inst_foto_invoice',
			'inst_bukti_bayar' => 'inst_bukti_bayar',
			'created_by' => 'created_by',
			'created_on' => 'created_on',
			'updated_by' => 'updated_by',
			'updated_on' => 'updated_on'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>