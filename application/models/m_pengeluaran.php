<?php
class M_pengeluaran extends Generic_dao {

    public function table_name() {
        return Tables::$pengeluaran;
    }

    public function field_map() {
		return array(
			'pengeluaran_id' => 'pengeluaran_id',
			'pengeluaran_tgl' => 'pengeluaran_tgl',
			'pengeluaran_jenis' => 'pengeluaran_jenis',
			'pengeluaran_ket' => 'pengeluaran_ket',
			'pengeluaran_total' => 'pengeluaran_total',
			'pengeluaran_gambar' => 'pengeluaran_gambar',
			'created_by' => 'created_by',
			'created_on' => 'created_on',
			'updated_by' => 'updated_by',
			'updated_on' => 'updated_on'
		);
    }

    public function __construct() {
        parent::__construct();
    }

    public function joined_table() {
        return array(
            array(
                'table_name' => Tables::$jenis_pengeluaran,
                'condition' => Tables::$jenis_pengeluaran . '.jp_id = ' . $this->table_name() . '.pengeluaran_jenis',
                'field' => 'jp_nama'
            )
        );
    }

    function get_data($key, $limit = 0, $offset = 0){
		$month = (!empty($key['pengeluaran_bulan']) && !empty($key['pengeluaran_tahun']))?" Month(pengeluaran_tgl) = ".$key['pengeluaran_bulan']." && Year(pengeluaran_tgl) = ".$key['pengeluaran_tahun'].(!empty($key['pengeluaran_jenis'])?"&&":"") : "";
		$jenis = (!empty($key['pengeluaran_jenis']))?" pengeluaran_jenis = '".$key['pengeluaran_jenis']."'" : "";
    	
    	$sql = "select * from pengeluaran inner join jenis_pengeluaran on jp_id = pengeluaran_jenis ".(!empty($key)?"where":"").$month.$jenis." limit ".$offset.",".$limit;
    	// print_r($sql);die();
        $query = $this->ci->db->query($sql);
        return $query->result();
    }

}

?>