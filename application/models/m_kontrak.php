<?php
class M_kontrak extends Generic_dao {

    public function table_name() {
        return Tables::$kontrak;
    }

    public function field_map() {
		return array(
			'kontrak_id' => 'kontrak_id',
			'kontrak_jenis' => 'kontrak_jenis',
			'kontrak_isi' => 'kontrak_isi',
			'kontrak_ket' => 'kontrak_ket',
			'created_by' => 'created_by',
			'created_on' => 'created_on',
			'updated_by' => 'updated_by',
			'updated_on' => 'updated_on'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>