/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 100129
 Source Host           : localhost:3306
 Source Schema         : db_lji

 Target Server Type    : MySQL
 Target Server Version : 100129
 File Encoding         : 65001

 Date: 06/07/2018 16:13:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city`  (
  `province_id` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `city_id` varchar(9) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `city_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`province_id`, `city_id`) USING BTREE,
  CONSTRAINT `city_ibfk_1` FOREIGN KEY (`province_id`) REFERENCES `province` (`province_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO `city` VALUES ('1', '114', 'Denpasar', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('1', '128', 'Gianyar', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('1', '161', 'Jembrana', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('1', '17', 'Badung', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('1', '170', 'Karangasem', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('1', '197', 'Klungkung', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('1', '32', 'Bangli', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('1', '447', 'Tabanan', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('1', '94', 'Buleleng', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('10', '105', 'Cilacap', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('10', '113', 'Demak', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('10', '134', 'Grobogan', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('10', '163', 'Jepara', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('10', '169', 'Karanganyar', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('10', '177', 'Kebumen', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('10', '181', 'Kendal', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('10', '196', 'Klaten', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('10', '209', 'Kudus', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('10', '249', 'Magelang', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('10', '250', 'Magelang', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('10', '344', 'Pati', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('10', '348', 'Pekalongan', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('10', '349', 'Pekalongan', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('10', '352', 'Pemalang', 'Admin', '2016-08-15 05:34:10', NULL, NULL);
INSERT INTO `city` VALUES ('10', '37', 'Banjarnegara', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('10', '375', 'Purbalingga', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('10', '377', 'Purworejo', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('10', '380', 'Rembang', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('10', '386', 'Salatiga', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('10', '398', 'Semarang', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('10', '399', 'Semarang', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('10', '41', 'Banyumas', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('10', '427', 'Sragen', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('10', '433', 'Sukoharjo', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('10', '445', 'Surakarta (Solo)', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('10', '472', 'Tegal', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('10', '473', 'Tegal', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('10', '476', 'Temanggung', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('10', '49', 'Batang', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('10', '497', 'Wonogiri', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('10', '498', 'Wonosobo', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('10', '76', 'Blora', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('10', '91', 'Boyolali', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('10', '92', 'Brebes', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('11', '133', 'Gresik', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('11', '160', 'Jember', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('11', '164', 'Jombang', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('11', '178', 'Kediri', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('11', '179', 'Kediri', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('11', '222', 'Lamongan', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('11', '243', 'Lumajang', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('11', '247', 'Madiun', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('11', '248', 'Madiun', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('11', '251', 'Magetan', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('11', '255', 'Malang', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('11', '256', 'Malang', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('11', '289', 'Mojokerto', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('11', '290', 'Mojokerto', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('11', '305', 'Nganjuk', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('11', '306', 'Ngawi', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('11', '31', 'Bangkalan', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('11', '317', 'Pacitan', 'Admin', '2016-08-15 05:34:08', NULL, NULL);
INSERT INTO `city` VALUES ('11', '330', 'Pamekasan', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('11', '342', 'Pasuruan', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('11', '343', 'Pasuruan', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('11', '363', 'Ponorogo', 'Admin', '2016-08-15 05:34:10', NULL, NULL);
INSERT INTO `city` VALUES ('11', '369', 'Probolinggo', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('11', '370', 'Probolinggo', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('11', '390', 'Sampang', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('11', '409', 'Sidoarjo', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('11', '418', 'Situbondo', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('11', '42', 'Banyuwangi', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('11', '441', 'Sumenep', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('11', '444', 'Surabaya', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('11', '487', 'Trenggalek', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('11', '489', 'Tuban', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('11', '492', 'Tulungagung', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('11', '51', 'Batu', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('11', '74', 'Blitar', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('11', '75', 'Blitar', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('11', '80', 'Bojonegoro', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('11', '86', 'Bondowoso', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('12', '168', 'Kapuas Hulu', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('12', '176', 'Kayong Utara', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('12', '195', 'Ketapang', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('12', '208', 'Kubu Raya', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('12', '228', 'Landak', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('12', '279', 'Melawi', 'Admin', '2016-08-15 05:34:06', NULL, NULL);
INSERT INTO `city` VALUES ('12', '364', 'Pontianak', 'Admin', '2016-08-15 05:34:10', NULL, NULL);
INSERT INTO `city` VALUES ('12', '365', 'Pontianak', 'Admin', '2016-08-15 05:34:10', NULL, NULL);
INSERT INTO `city` VALUES ('12', '388', 'Sambas', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('12', '391', 'Sanggau', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('12', '395', 'Sekadau', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('12', '415', 'Singkawang', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('12', '417', 'Sintang', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('12', '61', 'Bengkayang', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('13', '143', 'Hulu Sungai Selatan', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('13', '144', 'Hulu Sungai Tengah', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('13', '145', 'Hulu Sungai Utara', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('13', '18', 'Balangan', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('13', '203', 'Kotabaru', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('13', '33', 'Banjar', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('13', '35', 'Banjarbaru', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('13', '36', 'Banjarmasin', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('13', '43', 'Barito Kuala', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('13', '446', 'Tabalong', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('13', '452', 'Tanah Bumbu', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('13', '454', 'Tanah Laut', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('13', '466', 'Tapin', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('14', '136', 'Gunung Mas', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('14', '167', 'Kapuas', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('14', '174', 'Katingan', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('14', '205', 'Kotawaringin Barat', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('14', '206', 'Kotawaringin Timur', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('14', '221', 'Lamandau', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('14', '296', 'Murung Raya', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('14', '326', 'Palangka Raya', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('14', '371', 'Pulang Pisau', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('14', '405', 'Seruyan', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('14', '432', 'Sukamara', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('14', '44', 'Barito Selatan', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('14', '45', 'Barito Timur', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('14', '46', 'Barito Utara', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('15', '19', 'Balikpapan', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('15', '214', 'Kutai Barat', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('15', '215', 'Kutai Kartanegara', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('15', '216', 'Kutai Timur', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('15', '341', 'Paser', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('15', '354', 'Penajam Paser Utara', 'Admin', '2016-08-15 05:34:10', NULL, NULL);
INSERT INTO `city` VALUES ('15', '387', 'Samarinda', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('15', '66', 'Berau', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('15', '89', 'Bontang', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('16', '257', 'Malinau', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('16', '311', 'Nunukan', 'Admin', '2016-08-15 05:34:08', NULL, NULL);
INSERT INTO `city` VALUES ('16', '450', 'Tana Tidung', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('16', '467', 'Tarakan', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('16', '96', 'Bulungan (Bulongan)', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('17', '172', 'Karimun', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('17', '184', 'Kepulauan Anambas', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('17', '237', 'Lingga', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('17', '302', 'Natuna', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('17', '462', 'Tanjung Pinang', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('17', '48', 'Batam', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('17', '71', 'Bintan', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('18', '21', 'Bandar Lampung', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('18', '223', 'Lampung Barat', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('18', '224', 'Lampung Selatan', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('18', '225', 'Lampung Tengah', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('18', '226', 'Lampung Timur', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('18', '227', 'Lampung Utara', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('18', '282', 'Mesuji', 'Admin', '2016-08-15 05:34:06', NULL, NULL);
INSERT INTO `city` VALUES ('18', '283', 'Metro', 'Admin', '2016-08-15 05:34:06', NULL, NULL);
INSERT INTO `city` VALUES ('18', '355', 'Pesawaran', 'Admin', '2016-08-15 05:34:10', NULL, NULL);
INSERT INTO `city` VALUES ('18', '356', 'Pesisir Barat', 'Admin', '2016-08-15 05:34:10', NULL, NULL);
INSERT INTO `city` VALUES ('18', '368', 'Pringsewu', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('18', '458', 'Tanggamus', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('18', '490', 'Tulang Bawang', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('18', '491', 'Tulang Bawang Barat', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('18', '496', 'Way Kanan', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('19', '100', 'Buru Selatan', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('19', '14', 'Ambon', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('19', '185', 'Kepulauan Aru', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('19', '258', 'Maluku Barat Daya', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('19', '259', 'Maluku Tengah', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('19', '260', 'Maluku Tenggara', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('19', '261', 'Maluku Tenggara Barat', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('19', '400', 'Seram Bagian Barat', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('19', '401', 'Seram Bagian Timur', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('19', '488', 'Tual', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('19', '99', 'Buru', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('2', '27', 'Bangka', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('2', '28', 'Bangka Barat', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('2', '29', 'Bangka Selatan', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('2', '30', 'Bangka Tengah', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('2', '334', 'Pangkal Pinang', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('2', '56', 'Belitung', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('2', '57', 'Belitung Timur', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('20', '138', 'Halmahera Barat', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('20', '139', 'Halmahera Selatan', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('20', '140', 'Halmahera Tengah', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('20', '141', 'Halmahera Timur', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('20', '142', 'Halmahera Utara', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('20', '191', 'Kepulauan Sula', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('20', '372', 'Pulau Morotai', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('20', '477', 'Ternate', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('20', '478', 'Tidore Kepulauan', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('21', '1', 'Aceh Barat', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('21', '10', 'Aceh Timur', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('21', '11', 'Aceh Utara', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('21', '127', 'Gayo Lues', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('21', '2', 'Aceh Barat Daya', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('21', '20', 'Banda Aceh', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('21', '230', 'Langsa', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('21', '235', 'Lhokseumawe', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('21', '3', 'Aceh Besar', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('21', '300', 'Nagan Raya', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('21', '358', 'Pidie', 'Admin', '2016-08-15 05:34:10', NULL, NULL);
INSERT INTO `city` VALUES ('21', '359', 'Pidie Jaya', 'Admin', '2016-08-15 05:34:10', NULL, NULL);
INSERT INTO `city` VALUES ('21', '384', 'Sabang', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('21', '4', 'Aceh Jaya', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('21', '414', 'Simeulue', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('21', '429', 'Subulussalam', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('21', '5', 'Aceh Selatan', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('21', '59', 'Bener Meriah', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('21', '6', 'Aceh Singkil', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('21', '7', 'Aceh Tamiang', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('21', '72', 'Bireuen', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('21', '8', 'Aceh Tengah', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('21', '9', 'Aceh Tenggara', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('22', '118', 'Dompu', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('22', '238', 'Lombok Barat', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('22', '239', 'Lombok Tengah', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('22', '240', 'Lombok Timur', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('22', '241', 'Lombok Utara', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('22', '276', 'Mataram', 'Admin', '2016-08-15 05:34:06', NULL, NULL);
INSERT INTO `city` VALUES ('22', '438', 'Sumbawa', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('22', '439', 'Sumbawa Barat', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('22', '68', 'Bima', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('22', '69', 'Bima', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('23', '122', 'Ende', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('23', '125', 'Flores Timur', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('23', '13', 'Alor', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('23', '212', 'Kupang', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('23', '213', 'Kupang', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('23', '234', 'Lembata', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('23', '269', 'Manggarai', 'Admin', '2016-08-15 05:34:06', NULL, NULL);
INSERT INTO `city` VALUES ('23', '270', 'Manggarai Barat', 'Admin', '2016-08-15 05:34:06', NULL, NULL);
INSERT INTO `city` VALUES ('23', '271', 'Manggarai Timur', 'Admin', '2016-08-15 05:34:06', NULL, NULL);
INSERT INTO `city` VALUES ('23', '301', 'Nagekeo', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('23', '304', 'Ngada', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('23', '383', 'Rote Ndao', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('23', '385', 'Sabu Raijua', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('23', '412', 'Sikka', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('23', '434', 'Sumba Barat', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('23', '435', 'Sumba Barat Daya', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('23', '436', 'Sumba Tengah', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('23', '437', 'Sumba Timur', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('23', '479', 'Timor Tengah Selatan', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('23', '480', 'Timor Tengah Utara', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('23', '58', 'Belu', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('24', '111', 'Deiyai (Deliyai)', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('24', '117', 'Dogiyai', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('24', '150', 'Intan Jaya', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('24', '157', 'Jayapura', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('24', '158', 'Jayapura', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('24', '159', 'Jayawijaya', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('24', '16', 'Asmat', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('24', '180', 'Keerom', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('24', '193', 'Kepulauan Yapen (Yapen Waropen)', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('24', '231', 'Lanny Jaya', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('24', '263', 'Mamberamo Raya', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('24', '264', 'Mamberamo Tengah', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('24', '274', 'Mappi', 'Admin', '2016-08-15 05:34:06', NULL, NULL);
INSERT INTO `city` VALUES ('24', '281', 'Merauke', 'Admin', '2016-08-15 05:34:06', NULL, NULL);
INSERT INTO `city` VALUES ('24', '284', 'Mimika', 'Admin', '2016-08-15 05:34:06', NULL, NULL);
INSERT INTO `city` VALUES ('24', '299', 'Nabire', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('24', '303', 'Nduga', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('24', '335', 'Paniai', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('24', '347', 'Pegunungan Bintang', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('24', '373', 'Puncak', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('24', '374', 'Puncak Jaya', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('24', '392', 'Sarmi', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('24', '443', 'Supiori', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('24', '484', 'Tolikara', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('24', '495', 'Waropen', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('24', '499', 'Yahukimo', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('24', '500', 'Yalimo', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('24', '67', 'Biak Numfor', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('24', '90', 'Boven Digoel', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('25', '124', 'Fakfak', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('25', '165', 'Kaimana', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('25', '272', 'Manokwari', 'Admin', '2016-08-15 05:34:06', NULL, NULL);
INSERT INTO `city` VALUES ('25', '273', 'Manokwari Selatan', 'Admin', '2016-08-15 05:34:06', NULL, NULL);
INSERT INTO `city` VALUES ('25', '277', 'Maybrat', 'Admin', '2016-08-15 05:34:06', NULL, NULL);
INSERT INTO `city` VALUES ('25', '346', 'Pegunungan Arfak', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('25', '378', 'Raja Ampat', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('25', '424', 'Sorong', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('25', '425', 'Sorong', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('25', '426', 'Sorong Selatan', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('25', '449', 'Tambrauw', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('25', '474', 'Teluk Bintuni', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('25', '475', 'Teluk Wondama', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('26', '120', 'Dumai', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('26', '147', 'Indragiri Hilir', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('26', '148', 'Indragiri Hulu', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('26', '166', 'Kampar', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('26', '187', 'Kepulauan Meranti', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('26', '207', 'Kuantan Singingi', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('26', '350', 'Pekanbaru', 'Admin', '2016-08-15 05:34:10', NULL, NULL);
INSERT INTO `city` VALUES ('26', '351', 'Pelalawan', 'Admin', '2016-08-15 05:34:10', NULL, NULL);
INSERT INTO `city` VALUES ('26', '381', 'Rokan Hilir', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('26', '382', 'Rokan Hulu', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('26', '406', 'Siak', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('26', '60', 'Bengkalis', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('27', '253', 'Majene', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('27', '262', 'Mamasa', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('27', '265', 'Mamuju', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('27', '266', 'Mamuju Utara', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('27', '362', 'Polewali Mandar', 'Admin', '2016-08-15 05:34:10', NULL, NULL);
INSERT INTO `city` VALUES ('28', '123', 'Enrekang', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('28', '132', 'Gowa', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('28', '162', 'Jeneponto', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('28', '244', 'Luwu', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('28', '245', 'Luwu Timur', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('28', '246', 'Luwu Utara', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('28', '254', 'Makassar', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('28', '275', 'Maros', 'Admin', '2016-08-15 05:34:06', NULL, NULL);
INSERT INTO `city` VALUES ('28', '328', 'Palopo', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('28', '333', 'Pangkajene Kepulauan', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('28', '336', 'Parepare', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('28', '360', 'Pinrang', 'Admin', '2016-08-15 05:34:10', NULL, NULL);
INSERT INTO `city` VALUES ('28', '38', 'Bantaeng', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('28', '396', 'Selayar (Kepulauan Selayar)', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('28', '408', 'Sidenreng Rappang/Rapang', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('28', '416', 'Sinjai', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('28', '423', 'Soppeng', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('28', '448', 'Takalar', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('28', '451', 'Tana Toraja', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('28', '47', 'Barru', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('28', '486', 'Toraja Utara', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('28', '493', 'Wajo', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('28', '87', 'Bone', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('28', '95', 'Bulukumba', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('29', '119', 'Donggala', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('29', '25', 'Banggai', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('29', '26', 'Banggai Kepulauan', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('29', '291', 'Morowali', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('29', '329', 'Palu', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('29', '338', 'Parigi Moutong', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('29', '366', 'Poso', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('29', '410', 'Sigi', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('29', '482', 'Tojo Una-Una', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('29', '483', 'Toli-Toli', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('29', '98', 'Buol', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('3', '106', 'Cilegon', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('3', '232', 'Lebak', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('3', '331', 'Pandeglang', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('3', '402', 'Serang', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('3', '403', 'Serang', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('3', '455', 'Tangerang', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('3', '456', 'Tangerang', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('3', '457', 'Tangerang Selatan', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('30', '101', 'Buton', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('30', '102', 'Buton Utara', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('30', '182', 'Kendari', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('30', '198', 'Kolaka', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('30', '199', 'Kolaka Utara', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('30', '200', 'Konawe', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('30', '201', 'Konawe Selatan', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('30', '202', 'Konawe Utara', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('30', '295', 'Muna', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('30', '494', 'Wakatobi', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('30', '53', 'Bau-Bau', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('30', '85', 'Bombana', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('31', '188', 'Kepulauan Sangihe', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('31', '190', 'Kepulauan Siau Tagulandang Biaro (Sitaro)', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('31', '192', 'Kepulauan Talaud', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('31', '204', 'Kotamobagu', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('31', '267', 'Manado', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('31', '285', 'Minahasa', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('31', '286', 'Minahasa Selatan', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('31', '287', 'Minahasa Tenggara', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('31', '288', 'Minahasa Utara', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('31', '485', 'Tomohon', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('31', '73', 'Bitung', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('31', '81', 'Bolaang Mongondow (Bolmong)', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('31', '82', 'Bolaang Mongondow Selatan', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('31', '83', 'Bolaang Mongondow Timur', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('31', '84', 'Bolaang Mongondow Utara', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('32', '116', 'Dharmasraya', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('32', '12', 'Agam', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('32', '186', 'Kepulauan Mentawai', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('32', '236', 'Lima Puluh Koto/Kota', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('32', '318', 'Padang', 'Admin', '2016-08-15 05:34:08', NULL, NULL);
INSERT INTO `city` VALUES ('32', '321', 'Padang Panjang', 'Admin', '2016-08-15 05:34:08', NULL, NULL);
INSERT INTO `city` VALUES ('32', '322', 'Padang Pariaman', 'Admin', '2016-08-15 05:34:08', NULL, NULL);
INSERT INTO `city` VALUES ('32', '337', 'Pariaman', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('32', '339', 'Pasaman', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('32', '340', 'Pasaman Barat', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('32', '345', 'Payakumbuh', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('32', '357', 'Pesisir Selatan', 'Admin', '2016-08-15 05:34:10', NULL, NULL);
INSERT INTO `city` VALUES ('32', '394', 'Sawah Lunto', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('32', '411', 'Sijunjung (Sawah Lunto Sijunjung)', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('32', '420', 'Solok', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('32', '421', 'Solok', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('32', '422', 'Solok Selatan', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('32', '453', 'Tanah Datar', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('32', '93', 'Bukittinggi', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('33', '121', 'Empat Lawang', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('33', '220', 'Lahat', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('33', '242', 'Lubuk Linggau', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('33', '292', 'Muara Enim', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('33', '297', 'Musi Banyuasin', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('33', '298', 'Musi Rawas', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('33', '312', 'Ogan Ilir', 'Admin', '2016-08-15 05:34:08', NULL, NULL);
INSERT INTO `city` VALUES ('33', '313', 'Ogan Komering Ilir', 'Admin', '2016-08-15 05:34:08', NULL, NULL);
INSERT INTO `city` VALUES ('33', '314', 'Ogan Komering Ulu', 'Admin', '2016-08-15 05:34:08', NULL, NULL);
INSERT INTO `city` VALUES ('33', '315', 'Ogan Komering Ulu Selatan', 'Admin', '2016-08-15 05:34:08', NULL, NULL);
INSERT INTO `city` VALUES ('33', '316', 'Ogan Komering Ulu Timur', 'Admin', '2016-08-15 05:34:08', NULL, NULL);
INSERT INTO `city` VALUES ('33', '324', 'Pagar Alam', 'Admin', '2016-08-15 05:34:08', NULL, NULL);
INSERT INTO `city` VALUES ('33', '327', 'Palembang', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('33', '367', 'Prabumulih', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('33', '40', 'Banyuasin', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('34', '110', 'Dairi', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('34', '112', 'Deli Serdang', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('34', '137', 'Gunungsitoli', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('34', '146', 'Humbang Hasundutan', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('34', '15', 'Asahan', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('34', '173', 'Karo', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('34', '217', 'Labuhan Batu', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('34', '218', 'Labuhan Batu Selatan', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('34', '219', 'Labuhan Batu Utara', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('34', '229', 'Langkat', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('34', '268', 'Mandailing Natal', 'Admin', '2016-08-15 05:34:06', NULL, NULL);
INSERT INTO `city` VALUES ('34', '278', 'Medan', 'Admin', '2016-08-15 05:34:06', NULL, NULL);
INSERT INTO `city` VALUES ('34', '307', 'Nias', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('34', '308', 'Nias Barat', 'Admin', '2016-08-15 05:34:08', NULL, NULL);
INSERT INTO `city` VALUES ('34', '309', 'Nias Selatan', 'Admin', '2016-08-15 05:34:08', NULL, NULL);
INSERT INTO `city` VALUES ('34', '310', 'Nias Utara', 'Admin', '2016-08-15 05:34:08', NULL, NULL);
INSERT INTO `city` VALUES ('34', '319', 'Padang Lawas', 'Admin', '2016-08-15 05:34:08', NULL, NULL);
INSERT INTO `city` VALUES ('34', '320', 'Padang Lawas Utara', 'Admin', '2016-08-15 05:34:08', NULL, NULL);
INSERT INTO `city` VALUES ('34', '323', 'Padang Sidempuan', 'Admin', '2016-08-15 05:34:08', NULL, NULL);
INSERT INTO `city` VALUES ('34', '325', 'Pakpak Bharat', 'Admin', '2016-08-15 05:34:08', NULL, NULL);
INSERT INTO `city` VALUES ('34', '353', 'Pematang Siantar', 'Admin', '2016-08-15 05:34:10', NULL, NULL);
INSERT INTO `city` VALUES ('34', '389', 'Samosir', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('34', '404', 'Serdang Bedagai', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('34', '407', 'Sibolga', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('34', '413', 'Simalungun', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('34', '459', 'Tanjung Balai', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('34', '463', 'Tapanuli Selatan', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('34', '464', 'Tapanuli Tengah', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('34', '465', 'Tapanuli Utara', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('34', '470', 'Tebing Tinggi', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('34', '481', 'Toba Samosir', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('34', '52', 'Batu Bara', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('34', '70', 'Binjai', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('4', '175', 'Kaur', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('4', '183', 'Kepahiang', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('4', '233', 'Lebong', 'Admin', '2016-08-15 05:34:04', NULL, NULL);
INSERT INTO `city` VALUES ('4', '294', 'Muko Muko', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('4', '379', 'Rejang Lebong', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('4', '397', 'Seluma', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('4', '62', 'Bengkulu', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('4', '63', 'Bengkulu Selatan', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('4', '64', 'Bengkulu Tengah', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('4', '65', 'Bengkulu Utara', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('5', '135', 'Gunung Kidul', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('5', '210', 'Kulon Progo', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('5', '39', 'Bantul', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('5', '419', 'Sleman', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('5', '501', 'Yogyakarta', 'Admin', '2016-08-15 05:34:16', NULL, NULL);
INSERT INTO `city` VALUES ('6', '151', 'Jakarta Barat', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('6', '152', 'Jakarta Pusat', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('6', '153', 'Jakarta Selatan', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('6', '154', 'Jakarta Timur', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('6', '155', 'Jakarta Utara', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('6', '189', 'Kepulauan Seribu', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('7', '129', 'Gorontalo', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('7', '130', 'Gorontalo', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('7', '131', 'Gorontalo Utara', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('7', '361', 'Pohuwato', 'Admin', '2016-08-15 05:34:10', NULL, NULL);
INSERT INTO `city` VALUES ('7', '77', 'Boalemo', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('7', '88', 'Bone Bolango', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('8', '156', 'Jambi', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('8', '194', 'Kerinci', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('8', '280', 'Merangin', 'Admin', '2016-08-15 05:34:06', NULL, NULL);
INSERT INTO `city` VALUES ('8', '293', 'Muaro Jambi', 'Admin', '2016-08-15 05:34:07', NULL, NULL);
INSERT INTO `city` VALUES ('8', '393', 'Sarolangun', 'Admin', '2016-08-15 05:34:12', NULL, NULL);
INSERT INTO `city` VALUES ('8', '442', 'Sungaipenuh', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('8', '460', 'Tanjung Jabung Barat', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('8', '461', 'Tanjung Jabung Timur', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('8', '471', 'Tebo', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('8', '50', 'Batang Hari', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('8', '97', 'Bungo', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('9', '103', 'Ciamis', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('9', '104', 'Cianjur', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('9', '107', 'Cimahi', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('9', '108', 'Cirebon', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('9', '109', 'Cirebon', 'Admin', '2016-08-15 05:33:59', NULL, NULL);
INSERT INTO `city` VALUES ('9', '115', 'Depok', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('9', '126', 'Garut', 'Admin', '2016-08-15 05:34:00', NULL, NULL);
INSERT INTO `city` VALUES ('9', '149', 'Indramayu', 'Admin', '2016-08-15 05:34:01', NULL, NULL);
INSERT INTO `city` VALUES ('9', '171', 'Karawang', 'Admin', '2016-08-15 05:34:02', NULL, NULL);
INSERT INTO `city` VALUES ('9', '211', 'Kuningan', 'Admin', '2016-08-15 05:34:03', NULL, NULL);
INSERT INTO `city` VALUES ('9', '22', 'Bandung', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('9', '23', 'Bandung', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('9', '24', 'Bandung Barat', 'Admin', '2016-08-15 05:33:56', NULL, NULL);
INSERT INTO `city` VALUES ('9', '252', 'Majalengka', 'Admin', '2016-08-15 05:34:05', NULL, NULL);
INSERT INTO `city` VALUES ('9', '332', 'Pangandaran', 'Admin', '2016-08-15 05:34:09', NULL, NULL);
INSERT INTO `city` VALUES ('9', '34', 'Banjar', 'Admin', '2016-08-15 05:33:57', NULL, NULL);
INSERT INTO `city` VALUES ('9', '376', 'Purwakarta', 'Admin', '2016-08-15 05:34:11', NULL, NULL);
INSERT INTO `city` VALUES ('9', '428', 'Subang', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('9', '430', 'Sukabumi', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('9', '431', 'Sukabumi', 'Admin', '2016-08-15 05:34:13', NULL, NULL);
INSERT INTO `city` VALUES ('9', '440', 'Sumedang', 'Admin', '2016-08-15 05:34:14', NULL, NULL);
INSERT INTO `city` VALUES ('9', '468', 'Tasikmalaya', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('9', '469', 'Tasikmalaya', 'Admin', '2016-08-15 05:34:15', NULL, NULL);
INSERT INTO `city` VALUES ('9', '54', 'Bekasi', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('9', '55', 'Bekasi', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('9', '78', 'Bogor', 'Admin', '2016-08-15 05:33:58', NULL, NULL);
INSERT INTO `city` VALUES ('9', '79', 'Bogor', 'Admin', '2016-08-15 05:33:58', NULL, NULL);

-- ----------------------------
-- Table structure for invoice_asuransi
-- ----------------------------
DROP TABLE IF EXISTS `invoice_asuransi`;
CREATE TABLE `invoice_asuransi`  (
  `inac_id` int(11) NOT NULL AUTO_INCREMENT,
  `inac_tgl_invoice` date NULL DEFAULT NULL,
  `inac_no_invoice` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inac_nama_perusahaan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inac_jumlah` float NULL DEFAULT NULL,
  `inac_foto_invoice` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inac_bukti_bayar` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`inac_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of invoice_asuransi
-- ----------------------------
INSERT INTO `invoice_asuransi` VALUES (1, '2018-07-06', '13334', 'PT. ABC', 3000000, 'upload/invoice_asuransi/b3d9c149e2980a70df245ec15dd133d4.jpg', 'upload/buktibayar_asuransi/6b8e3e9f9971cc80419c07ef152e4f8b.jpg', '2018-07-06 08:51:34', 'super', NULL, NULL);
INSERT INTO `invoice_asuransi` VALUES (2, '2018-07-06', '435345345', 'PT. DEV', 1000000, 'upload/invoice_asuransi/f7cb222947ab019c648776a858d99f16.jpg', 'upload/buktibayar_asuransi/e57d6f76508b66e3f7f511073f746a4e.jpg', '2018-07-06 14:06:50', 'super', NULL, NULL);
INSERT INTO `invoice_asuransi` VALUES (3, '2018-07-06', '435345345', 'PT. DEV', 1000000, 'upload/invoice_asuransi/c5e5ead243ef1abc6d4f4cf11cb7a8cb.jpg', 'upload/buktibayar_asuransi/154c25db5726c5f9272e108dde66dbd9.jpg', '2018-07-06 14:07:48', 'super', NULL, NULL);

-- ----------------------------
-- Table structure for invoice_batubara
-- ----------------------------
DROP TABLE IF EXISTS `invoice_batubara`;
CREATE TABLE `invoice_batubara`  (
  `inbb_id` int(11) NOT NULL AUTO_INCREMENT,
  `inbb_tgl_invoice` date NULL DEFAULT NULL,
  `inbb_no_invoice` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inbb_nama_perusahaan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inbb_uraian_barang` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `inbb_tonase` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inbb_harga_satuan` float NULL DEFAULT NULL,
  `inbb_jumlah_total` float NULL DEFAULT NULL,
  `inbb_foto_invoice` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inbb_bukti_bayar` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`inbb_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of invoice_batubara
-- ----------------------------
INSERT INTO `invoice_batubara` VALUES (1, '2018-07-06', '234234', 'PT. DEF', 'test', 'test', 10000, 1000000, 'upload/invoice_batubara/2fc7a301f9b17fc24237014d6ad471c5.jpg', 'upload/buktibayar_batubara/5cd018b885271e4b6c86bdd490081afe.jpg', '2018-07-06 11:46:52', 'super', NULL, NULL);

-- ----------------------------
-- Table structure for invoice_sewa_tongkang
-- ----------------------------
DROP TABLE IF EXISTS `invoice_sewa_tongkang`;
CREATE TABLE `invoice_sewa_tongkang`  (
  `inst_id` int(11) NOT NULL AUTO_INCREMENT,
  `inst_tgl_invoice` date NULL DEFAULT NULL,
  `inst_no_invoice` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inst_no_spal` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inst_nama_perusahaan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inst_nama_tug_boat` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inst_nama_tongkang` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inst_tonase` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inst_harga` float NULL DEFAULT NULL,
  `inst_jumlah_total` float NULL DEFAULT NULL,
  `inst_foto_invoice` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `inst_bukti_bayar` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`inst_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of invoice_sewa_tongkang
-- ----------------------------
INSERT INTO `invoice_sewa_tongkang` VALUES (1, '2018-06-06', '345345', '345345', 'PT. TEST', 'test', 'test', 'test', 10000, 1000000, 'upload/invoice_sewa_tongkang/c8b916ee23e31702531d3dee83c55764.jpg', 'upload/buktibayar_sewa_tongkang/7cd31ff897417ca8f6571bff329eb9e9.jpg', '2018-07-06 14:02:07', 'super', NULL, NULL);

-- ----------------------------
-- Table structure for jenis_pengeluaran
-- ----------------------------
DROP TABLE IF EXISTS `jenis_pengeluaran`;
CREATE TABLE `jenis_pengeluaran`  (
  `jp_id` int(11) NOT NULL,
  `jp_nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jp_deskripsi` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`jp_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of jenis_pengeluaran
-- ----------------------------
INSERT INTO `jenis_pengeluaran` VALUES (1, 'Installment', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `jenis_pengeluaran` VALUES (2, 'Salary', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `jenis_pengeluaran` VALUES (3, 'Office Expenses', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `jenis_pengeluaran` VALUES (4, 'Marketing Fee', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `jenis_pengeluaran` VALUES (5, 'Internal Fee', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `jenis_pengeluaran` VALUES (6, 'Others Fee', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `jenis_pengeluaran` VALUES (7, 'Surveyor', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `jenis_pengeluaran` VALUES (8, 'Overseas Travel Hotel', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `jenis_pengeluaran` VALUES (9, 'Overseas Travel Transport', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `jenis_pengeluaran` VALUES (10, 'Other Expenses', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `jenis_pengeluaran` VALUES (11, 'Entertaint', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for jenis_penjualan
-- ----------------------------
DROP TABLE IF EXISTS `jenis_penjualan`;
CREATE TABLE `jenis_penjualan`  (
  `jpj_id` int(11) NOT NULL,
  `jpj_nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jpj_deskripsi` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`jpj_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of jenis_penjualan
-- ----------------------------
INSERT INTO `jenis_penjualan` VALUES (1, 'Batubara FOB', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `jenis_penjualan` VALUES (2, 'Batubara CNF', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `jenis_penjualan` VALUES (3, 'Batubara CIF', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `jenis_penjualan` VALUES (4, 'Batubara FOT', NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for kontrak
-- ----------------------------
DROP TABLE IF EXISTS `kontrak`;
CREATE TABLE `kontrak`  (
  `kontrak_id` int(11) NOT NULL AUTO_INCREMENT,
  `kontrak_jenis` int(11) NULL DEFAULT NULL COMMENT '1 = pjbb_jual, 2 =pjbb_beli, 3 = si, 4 = offering',
  `kontrak_isi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `kontrak_ket` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kontrak_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kontrak
-- ----------------------------
INSERT INTO `kontrak` VALUES (1, 1, 'kjkjhksdfd sdfsdfds dfdfffdddf', 'pjbb jual', '2018-05-02 07:21:05', 'admin', '2018-05-02 08:20:46', 'super');
INSERT INTO `kontrak` VALUES (2, 2, NULL, 'pjbb beli', '2018-05-02 07:26:22', 'admin', NULL, NULL);
INSERT INTO `kontrak` VALUES (3, 3, NULL, 'shipping instruction', '2018-05-02 07:26:52', 'admin', NULL, NULL);
INSERT INTO `kontrak` VALUES (4, 4, NULL, 'offering', '2018-05-02 07:27:01', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `men_menu_id` int(11) NULL DEFAULT NULL,
  `menu_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `menu_link` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `menu_status` tinyint(1) NOT NULL,
  `menu_ismaster` tinyint(1) NOT NULL,
  `menu_order` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_category` smallint(6) NULL DEFAULT NULL COMMENT '1 = coal division, 2 = truck division, 3 = data master',
  `is_deleted` smallint(6) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`) USING BTREE,
  INDEX `fk_parent_id`(`men_menu_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 57 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, NULL, 'Dashboard', 'dashboard/coal', 1, 0, 1, '2018-04-14 12:27:56', 'super', NULL, NULL, 1, NULL);
INSERT INTO `menu` VALUES (2, NULL, 'Dashboard', 'dashboard/master', 1, 0, 2, '2018-04-14 12:27:56', 'super', NULL, NULL, 3, NULL);
INSERT INTO `menu` VALUES (3, NULL, 'PJBB', '#', 1, 0, 3, '2018-04-14 12:27:56', 'super', NULL, NULL, 1, NULL);
INSERT INTO `menu` VALUES (4, 3, 'PJBB Jual', 'pjbb_jual', 1, 0, 4, '2018-04-14 12:27:56', 'super', NULL, NULL, 1, NULL);
INSERT INTO `menu` VALUES (5, 3, 'PJBB Beli', 'pjbb_beli', 1, 0, 5, '2018-04-14 12:27:56', 'super', NULL, NULL, 1, NULL);
INSERT INTO `menu` VALUES (6, NULL, 'Shipping Instruction', 'shipping_instruction', 1, 0, 6, '2018-04-14 12:27:56', 'super', NULL, NULL, 1, NULL);
INSERT INTO `menu` VALUES (7, NULL, 'Offering', 'offering', 1, 0, 7, '2018-04-14 12:27:56', 'super', NULL, NULL, 1, NULL);
INSERT INTO `menu` VALUES (8, NULL, 'Report of Analysis', 'roa', 1, 0, 8, '2018-04-14 12:27:56', 'super', NULL, NULL, 1, NULL);
INSERT INTO `menu` VALUES (9, NULL, 'Accounting', '#', 1, 0, 10, '2018-04-14 12:27:56', 'super', NULL, NULL, 1, NULL);
INSERT INTO `menu` VALUES (10, 9, 'Pembelian', 'pembelian', 0, 0, 11, '2018-04-14 12:27:56', 'super', NULL, NULL, 1, NULL);
INSERT INTO `menu` VALUES (11, 9, 'Penjualan', 'penjualan', 1, 0, 12, '2018-04-14 12:27:56', 'super', NULL, NULL, 1, NULL);
INSERT INTO `menu` VALUES (12, 9, 'Pengeluaran', 'pengeluaran', 1, 0, 13, '2018-04-14 12:27:56', 'super', NULL, NULL, 1, NULL);
INSERT INTO `menu` VALUES (13, NULL, 'Invoice', 'invoice', 1, 0, 9, '2018-04-14 12:27:56', 'super', NULL, NULL, 1, NULL);
INSERT INTO `menu` VALUES (14, 24, 'Neraca Laba Rugi', 'neraca', 1, 0, 28, '2018-04-14 12:27:56', 'super', NULL, NULL, 1, NULL);
INSERT INTO `menu` VALUES (15, NULL, 'Data user', 'user', 1, 0, 15, '2018-04-14 12:27:56', 'super', NULL, NULL, 3, NULL);
INSERT INTO `menu` VALUES (16, NULL, 'Hak Akses', 'role', 1, 0, 16, '2018-04-14 12:27:56', 'super', NULL, NULL, 3, NULL);
INSERT INTO `menu` VALUES (17, NULL, 'Menu', 'menu', 1, 0, 17, '2018-04-14 12:27:56', 'super', NULL, NULL, 3, NULL);
INSERT INTO `menu` VALUES (18, NULL, 'Divisi', 'divisi', 1, 0, 18, '2018-04-14 12:27:56', 'super', NULL, NULL, 3, NULL);
INSERT INTO `menu` VALUES (19, 13, 'Asuransi', 'invoice_asuransi', 1, 0, 19, '2018-05-14 09:26:38', 'super', NULL, NULL, 1, NULL);
INSERT INTO `menu` VALUES (20, 13, 'Batu Bara', 'invoice_batubara', 1, 0, 20, '2018-05-14 09:36:16', 'super', NULL, NULL, 1, NULL);
INSERT INTO `menu` VALUES (21, 13, 'Sewa Tongkang', 'invoice_sewa_tongkang', 1, 0, 21, '2018-05-14 09:36:47', 'super', NULL, NULL, 1, NULL);
INSERT INTO `menu` VALUES (22, NULL, 'Kategori Penjualan', 'jenis_penjualan', 1, 0, 22, NULL, NULL, NULL, NULL, 3, NULL);
INSERT INTO `menu` VALUES (23, NULL, 'Kategori Pengeluaran', 'jenis_pengeluaran', 1, 0, 23, NULL, NULL, NULL, NULL, 3, NULL);
INSERT INTO `menu` VALUES (24, NULL, 'Report', '#', 1, 0, 24, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `menu` VALUES (25, 24, 'Pembelian', 'report_pembelian', 1, 0, 25, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `menu` VALUES (26, 24, 'Penjualan', 'report_penjualan', 1, 0, 26, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `menu` VALUES (27, 24, 'Pengeluaran', 'report_pengeluaran', 1, 0, 27, NULL, NULL, NULL, NULL, 1, NULL);

-- ----------------------------
-- Table structure for menu_role
-- ----------------------------
DROP TABLE IF EXISTS `menu_role`;
CREATE TABLE `menu_role`  (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `CREATED_ON` datetime(0) NULL DEFAULT NULL,
  `UPDATED_BY` int(11) NULL DEFAULT NULL,
  `CREATED_BY` int(11) NULL DEFAULT NULL,
  `UPDATED_ON` datetime(0) NULL DEFAULT NULL,
  `IS_DELETED` smallint(6) NULL DEFAULT NULL,
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE,
  INDEX `fk_menu_role2`(`menu_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of menu_role
-- ----------------------------
INSERT INTO `menu_role` VALUES (1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 10, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 11, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 12, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 13, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 14, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 15, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 16, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 17, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 18, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 19, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 20, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 21, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 22, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 23, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 24, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 25, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 26, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `menu_role` VALUES (1, 27, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for offering
-- ----------------------------
DROP TABLE IF EXISTS `offering`;
CREATE TABLE `offering`  (
  `offer_id` int(11) NOT NULL,
  `offer_no` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `offer_to` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `offer_attn` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `offer_calarie_value` int(11) NULL DEFAULT NULL,
  `offer_ash_content` float NULL DEFAULT NULL,
  `offer_sulfur` float NULL DEFAULT NULL,
  `offer_size` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `offer_qty` int(11) NULL DEFAULT NULL,
  `offer_delivery` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `offer_price` float NULL DEFAULT NULL,
  `offer_top1` float NULL DEFAULT NULL COMMENT 'term of payment',
  `offer_top2` float NULL DEFAULT NULL,
  `offer_date` date NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`offer_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of offering
-- ----------------------------
INSERT INTO `offering` VALUES (1, '01/offer/25/2018', 'PT. ABCD', 'test', 12, 12, 12, '12', 12, 'test', 10000, 10, 10, '2018-04-25', '2018-04-25 15:29:55', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for pembelian
-- ----------------------------
DROP TABLE IF EXISTS `pembelian`;
CREATE TABLE `pembelian`  (
  `pembelian_id` int(11) NOT NULL AUTO_INCREMENT,
  `pembelian_jenis` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pembelian_tgl_invoice` date NULL DEFAULT NULL,
  `pembelian_no_invoice` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pembelian_ket` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pembelian_total` float NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`pembelian_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pembelian
-- ----------------------------
INSERT INTO `pembelian` VALUES (1, 'Asuransi', '2018-07-06', '13334', 'Asuransi Cargo', 3000000, '2018-07-06 13:33:16', NULL, NULL, NULL);
INSERT INTO `pembelian` VALUES (2, 'Batu Bara', '2018-07-06', '234234', 'Pembelian Batubara', 1000000, '2018-06-01 13:33:20', NULL, NULL, NULL);
INSERT INTO `pembelian` VALUES (3, 'Sewa Tongkang', '2018-06-06', '345345', 'Sewa Tongkang', 1000000, '2018-07-31 14:04:49', NULL, NULL, NULL);
INSERT INTO `pembelian` VALUES (4, 'Asuransi', '2018-07-06', '435345345', 'Asuransi Cargo', 1000000, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for pengeluaran
-- ----------------------------
DROP TABLE IF EXISTS `pengeluaran`;
CREATE TABLE `pengeluaran`  (
  `pengeluaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `pengeluaran_jenis` int(11) NULL DEFAULT NULL,
  `pengeluaran_ket` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pengeluaran_total` float NULL DEFAULT NULL,
  `pengeluaran_gambar` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pengeluaran_tgl` date NULL DEFAULT NULL,
  PRIMARY KEY (`pengeluaran_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pengeluaran
-- ----------------------------
INSERT INTO `pengeluaran` VALUES (1, 2, 'test', 10000000, 'upload/bukti_pengeluaran/ecc17cbc1fb0816d7d06841552299c33.jpg', '2018-07-06 09:43:41', 'super', NULL, NULL, '2018-06-01');
INSERT INTO `pengeluaran` VALUES (2, 1, 'test', 10000000, 'upload/bukti_pengeluaran/424b1ffda9bdb492d1fbcd44fb81dbe6.jpg', '2018-07-06 14:50:26', 'super', NULL, NULL, '2018-07-06');

-- ----------------------------
-- Table structure for penjualan
-- ----------------------------
DROP TABLE IF EXISTS `penjualan`;
CREATE TABLE `penjualan`  (
  `penjualan_id` int(11) NOT NULL AUTO_INCREMENT,
  `penjualan_jenis` int(11) NULL DEFAULT NULL,
  `penjualan_tgl_invoice` date NULL DEFAULT NULL,
  `penjualan_no_invoice` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `penjualan_nama_perusahaan` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `penjualan_uraian_barang` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `penjualan_nama_tug_boat` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `penjualan_nama_tongkang` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `penjualan_tonase` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `penjualan_harga_satuan` float NULL DEFAULT NULL,
  `penjualan_jumlah_total` float NULL DEFAULT NULL,
  `penjualan_foto_invoice` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `penjualan_bukti_bayar` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`penjualan_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of penjualan
-- ----------------------------
INSERT INTO `penjualan` VALUES (1, 1, '2018-07-06', '324', 'PT. ABC', 'test', 'test', 'test', 'test', 100000, 10000000, 'upload/invoice_penjualan/33f933fcc71f0e0387e0a623564be61e.jpg', 'upload/buktibayar_penjualan/7442214646cf32a407d4c904fd8fccac.jpg', '2018-07-06 09:42:22', 'super', NULL, NULL);
INSERT INTO `penjualan` VALUES (2, 1, '2018-07-06', '234', 'PT. MNK', 'test', 'test', 'test', 'test', 100000, 100000000, 'upload/invoice_penjualan/dd9521e3fc7ec2685873cf8ec1c5d3ce.jpg', 'upload/buktibayar_penjualan/5d3fb85c969a4c8e2fe921b3ffae07ec.jpg', '2018-07-06 14:34:30', 'super', NULL, NULL);
INSERT INTO `penjualan` VALUES (3, 2, '2018-07-06', '543534', 'PT. AOB', 'test', 'test', 'test', 'test', 200000, 2000000, 'upload/invoice_penjualan/3f15ea9439af0a855276177221416191.jpg', 'upload/buktibayar_penjualan/96f7f49590e630e5d7e8e633bc451fbd.jpg', '2018-07-06 14:39:19', 'super', NULL, NULL);

-- ----------------------------
-- Table structure for pjbb_beli
-- ----------------------------
DROP TABLE IF EXISTS `pjbb_beli`;
CREATE TABLE `pjbb_beli`  (
  `pbeli_id` int(11) NOT NULL AUTO_INCREMENT,
  `pbeli_no` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pbeli_nama` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pbeli_perusahaan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pbeli_alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pbeli_spek` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pbeli_kondisi` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pbeli_asal_batu` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pbeli_alamat_asal_batu` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pbeli_jumlah_muatan` int(11) NULL DEFAULT NULL,
  `pbeli_harga_perton` float NULL DEFAULT NULL,
  `pbeli_toleransi` int(11) NULL DEFAULT NULL,
  `pbeli_pelabuhan_muat` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pbeli_pelabuhan_bongkar` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pbeli_pembayaran1` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pbeli_pembayaran2` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pbeli_pembayaran3` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pbeli_status` smallint(6) NULL DEFAULT NULL,
  `pbeli_ttd` smallint(6) NULL DEFAULT NULL,
  `pbeli_nama_rek` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pbeli_no_rek` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pbeli_bank` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`pbeli_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pjbb_beli
-- ----------------------------
INSERT INTO `pjbb_beli` VALUES (1, '01/PJBB/02/2018', 'Henny Alfianti', 'PT. Xdev', 'Jl. Makam Caringin', 'ABC', 'AB', 'Jl. ABC', 'Jl. dsjfhg', 12, 10000, 10, 'PL. APK', 'PL. MKO', '1000000', '1000000', '10000000', 1, NULL, NULL, NULL, NULL, '2018-04-24 21:28:38', 'admin', NULL, NULL);
INSERT INTO `pjbb_beli` VALUES (2, '02/PJBB/05/2018', 'Abcd', 'ABC', 'Jl. ABC', '12', '12', '12', '12', 12, 12, 12, '12', '12', '12', '121', '12', 0, 12, NULL, '12', '12', '2018-05-03 03:57:42', 'super', NULL, NULL);

-- ----------------------------
-- Table structure for pjbb_jual
-- ----------------------------
DROP TABLE IF EXISTS `pjbb_jual`;
CREATE TABLE `pjbb_jual`  (
  `pjual_id` int(11) NOT NULL AUTO_INCREMENT,
  `pjual_no` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pjual_nama` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pjual_perusahaan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pjual_alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pjual_spek` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pjual_kondisi` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pjual_asal_batu` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pjual_alamat_asal_batu` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pjual_jumlah_muatan` int(11) NULL DEFAULT NULL,
  `pjual_harga_perton` float NULL DEFAULT NULL,
  `pjual_toleransi` int(11) NULL DEFAULT NULL,
  `pjual_pelabuhan_muat` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pjual_pelabuhan_bongkar` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pjual_pembayaran1` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pjual_pembayaran2` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pjual_pembayaran3` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pjual_status` smallint(6) NULL DEFAULT NULL,
  `pjual_ttd` smallint(6) NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`pjual_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pjbb_jual
-- ----------------------------
INSERT INTO `pjbb_jual` VALUES (1, '01/PJBB/02/2018', 'Henny Alfianti', 'PT. Xdev', 'Jl. Makam Caringin', 'ABC', 'AB', 'Jl. ABC', 'Jl. dsjfhg', 12, 10000, 10, 'PL. APK', 'PL. MKO', '1000000', '1000000', '10000000', 1, NULL, '2018-04-24 21:28:38', 'admin', NULL, NULL);
INSERT INTO `pjbb_jual` VALUES (2, '10/PJBB/05/2018', 'kdgh', 'kjh', 'kjhkjh', 'kjh', 'kjh', 'kjh', 'kjh', 87, 87, 897, 'kjhjkh', 'jkh', 'kjdf', 'dfg', 'dfg', 0, 0, '2018-05-01 01:40:34', 'super', '2018-05-02 21:19:20', 'super');

-- ----------------------------
-- Table structure for province
-- ----------------------------
DROP TABLE IF EXISTS `province`;
CREATE TABLE `province`  (
  `province_id` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `province_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`province_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of province
-- ----------------------------
INSERT INTO `province` VALUES ('1', 'Bali', 'Admin', '2016-08-15 05:28:38', NULL, NULL);
INSERT INTO `province` VALUES ('10', 'Jawa Tengah', 'Admin', '2016-08-15 05:28:38', NULL, NULL);
INSERT INTO `province` VALUES ('11', 'Jawa Timur', 'Admin', '2016-08-15 05:28:38', NULL, NULL);
INSERT INTO `province` VALUES ('12', 'Kalimantan Barat', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('13', 'Kalimantan Selatan', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('14', 'Kalimantan Tengah', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('15', 'Kalimantan Timur', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('16', 'Kalimantan Utara', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('17', 'Kepulauan Riau', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('18', 'Lampung', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('19', 'Maluku', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('2', 'Bangka Belitung', 'Admin', '2016-08-15 05:28:38', NULL, NULL);
INSERT INTO `province` VALUES ('20', 'Maluku Utara', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('21', 'Nanggroe Aceh Darussalam (NAD)', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('22', 'Nusa Tenggara Barat (NTB)', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('23', 'Nusa Tenggara Timur (NTT)', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('24', 'Papua', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('25', 'Papua Barat', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('26', 'Riau', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('27', 'Sulawesi Barat', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('28', 'Sulawesi Selatan', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('29', 'Sulawesi Tengah', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('3', 'Banten', 'Admin', '2016-08-15 05:28:38', NULL, NULL);
INSERT INTO `province` VALUES ('30', 'Sulawesi Tenggara', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('31', 'Sulawesi Utara', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('32', 'Sumatera Barat', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('33', 'Sumatera Selatan', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('34', 'Sumatera Utara', 'Admin', '2016-08-15 05:28:39', NULL, NULL);
INSERT INTO `province` VALUES ('4', 'Bengkulu', 'Admin', '2016-08-15 05:28:38', NULL, NULL);
INSERT INTO `province` VALUES ('5', 'DI Yogyakarta', 'Admin', '2016-08-15 05:28:38', NULL, NULL);
INSERT INTO `province` VALUES ('6', 'DKI Jakarta', 'Admin', '2016-08-15 05:28:38', NULL, NULL);
INSERT INTO `province` VALUES ('7', 'Gorontalo', 'Admin', '2016-08-15 05:28:38', NULL, NULL);
INSERT INTO `province` VALUES ('8', 'Jambi', 'Admin', '2016-08-15 05:28:38', NULL, NULL);
INSERT INTO `province` VALUES ('9', 'Jawa Barat', 'Admin', '2016-08-15 05:28:38', NULL, NULL);

-- ----------------------------
-- Table structure for roa
-- ----------------------------
DROP TABLE IF EXISTS `roa`;
CREATE TABLE `roa`  (
  `roa_id` int(11) NOT NULL AUTO_INCREMENT,
  `roa_desc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `roa_file` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `roa_status` smallint(6) NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varbinary(100) NULL DEFAULT NULL,
  PRIMARY KEY (`roa_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of roa
-- ----------------------------
INSERT INTO `roa` VALUES (1, 'test abc', 'aaaa', 1, '2018-04-25 15:47:32', 'admin', NULL, NULL);
INSERT INTO `roa` VALUES (2, 'sdsdfsdfsdf', 'upload/roa_file/fb356e0bc1e36c5ac0f01ab03b169591.png', NULL, '2018-05-03 17:20:05', 'super', NULL, NULL);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `role_status` tinyint(1) NOT NULL,
  `role_canlogin` tinyint(1) NOT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_deleted` smallint(6) NULL DEFAULT NULL,
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, 'Super Admin', 1, 1, '0000-00-00 00:00:00', NULL, '2016-05-21 19:13:08', 'admin', NULL);
INSERT INTO `role` VALUES (7, 'Administrator', 1, 1, '0000-00-00 00:00:00', 'super', '2016-05-21 19:13:35', 'admin', NULL);

-- ----------------------------
-- Table structure for shipping_instruction
-- ----------------------------
DROP TABLE IF EXISTS `shipping_instruction`;
CREATE TABLE `shipping_instruction`  (
  `si_id` int(11) NOT NULL AUTO_INCREMENT,
  `si_no` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `si_pjbb_no` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `si_to` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `si_shipper` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `si_consignee` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `si_notif_address` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `si_pol` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `si_pod` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `si_date_load` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `si_vessel` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `si_cargo` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `si_desc` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `si_mark` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `si_agen_kapal` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `si_status` smallint(6) NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_on` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`si_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of shipping_instruction
-- ----------------------------
INSERT INTO `shipping_instruction` VALUES (1, '01/SI/25/2018', '01/PJBB/02/2018', 'PT. ABC', 'PT. DEF', 'Xyz zzz', 'Jl. ABC, Bandung', '12', '12', '03/04 2018', 'test', 'test', 'test desc', 'test', 'test', 1, '2018-04-25 14:56:56', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `username` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `full_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `created_by` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_deleted` smallint(6) NULL DEFAULT NULL,
  `id_clinic` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_user`) USING BTREE,
  INDEX `fk_user_role`(`role_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 1, 'super', '1b3231655cebb7a1f783eddf27d254ca', 'Administrator Super', '2016-04-22 13:54:09', 'super', '0000-00-00 00:00:00', '', NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
