<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Pjbb Beli
    <small><?php echo ($edit)?'Edit':'Insert'; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?php echo $current_context; ?>">pjbb beli</a></li>
    <li class="active"><?php echo ($edit)?'Edit':'Insert'; ?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <?php
        $message = $this->session->flashdata('message');
        $type_message = $this->session->flashdata('type_message');
        echo (!empty($message) && $type_message=="success") ? ' <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-check"></i> Great!</h4>'.$message.'</div></div>': '';
        echo (!empty($message) && $type_message=="error") ? '   <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-exclamation-triangle"></i> Uh-Oh!</h4>'.$message.'</div></div>': '';
    ?>
    <!-- right column -->
    <div class="col-md-12">
      <!-- general form elements disabled -->
      <div class="box">
        <div class="box-header with-border text-right">
          <h3 class="box-title">Add</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <form role="form" method="POST" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-2">
                <dl class="text-right">
                  <dt><h5>Pjbb beli</h5></dt>
                  <dd>pjbb information</dd>
                </dl>
              </div>
              <div class="col-md-8">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pbeli_no') != "") ? "has-error" : "" ?>">
                        <label>No. PJBB <req>*</req></label>
                        <input class="form-control" name="pbeli_no" value="<?php echo set_value('pbeli_no', $pjbb_beli->pbeli_no); ?>" placeholder="No. PJBB">
                        <p class="help-block"><?php echo form_error('pbeli_no'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pbeli_nama') != "") ? "has-error" : "" ?>">
                        <label>Nama Penjual </label>
                        <input class="form-control" name="pbeli_nama" value="<?php echo set_value('pbeli_nama', $pjbb_beli->pbeli_nama); ?>" placeholder="Nama Pembeli">
                        <p class="help-block"><?php echo form_error('pbeli_nama'); ?></p>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pbeli_perusahaan') != "") ? "has-error" : "" ?>">
                        <label>Perusahaan <req>*</req></label>
                        <input class="form-control" name="pbeli_perusahaan" value="<?php echo set_value('pbeli_perusahaan', $pjbb_beli->pbeli_perusahaan); ?>" placeholder="Perusahaan">
                        <p class="help-block"><?php echo form_error('pbeli_perusahaan'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pbeli_alamat') != "") ? "has-error" : "" ?>">
                        <label>Alamat <req>*</req></label>
                        <textarea class="form-control" name="pbeli_alamat" placeholder="Alamat"><?php echo set_value('pbeli_alamat', $pjbb_beli->pbeli_alamat); ?></textarea>
                        <p class="help-block"><?php echo form_error('pbeli_alamat'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pbeli_spek') != "") ? "has-error" : "" ?>">
                        <label>Spesifikasi <req>*</req></label>
                        <input class="form-control" name="pbeli_spek" value="<?php echo set_value('pbeli_spek', $pjbb_beli->pbeli_spek); ?>" placeholder="Spesifikasi">
                        <p class="help-block"><?php echo form_error('pbeli_spek'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pbeli_kondisi') != "") ? "has-error" : "" ?>">
                        <label>Kondisi </label>
                        <input class="form-control" name="pbeli_kondisi" value="<?php echo set_value('pbeli_kondisi', $pjbb_beli->pbeli_kondisi); ?>" placeholder="Kondisi">
                        <p class="help-block"><?php echo form_error('pbeli_kondisi'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pbeli_asal_batu') != "") ? "has-error" : "" ?>">
                        <label>Asal Batu <req>*</req></label>
                        <textarea class="form-control" name="pbeli_asal_batu" placeholder="Asal Batu"><?php echo set_value('pbeli_asal_batu', $pjbb_beli->pbeli_asal_batu); ?></textarea>
                        <p class="help-block"><?php echo form_error('pbeli_asal_batu'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pbeli_alamat_asal_batu') != "") ? "has-error" : "" ?>">
                        <label>Alamat Asal Batu </label>
                        <textarea class="form-control" name="pbeli_alamat_asal_batu" placeholder="Alamat Asal Batu"><?php echo set_value('pbeli_alamat_asal_batu', $pjbb_beli->pbeli_alamat_asal_batu); ?></textarea>
                        <p class="help-block"><?php echo form_error('pbeli_alamat_asal_batu'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pbeli_jumlah_muatan') != "") ? "has-error" : "" ?>">
                        <label>Jumlah Muatan <req>*</req></label>
                        <input class="form-control" type="number" name="pbeli_jumlah_muatan" value="<?php echo set_value('pbeli_jumlah_muatan', $pjbb_beli->pbeli_jumlah_muatan); ?>" placeholder="Jumlah Muatan">
                        <p class="help-block"><?php echo form_error('pbeli_jumlah_muatan'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pbeli_harga_perton') != "") ? "has-error" : "" ?>">
                        <label>Harga/ton </label>
                        <input class="form-control" type="number" name="pbeli_harga_perton" value="<?php echo set_value('pbeli_harga_perton', $pjbb_beli->pbeli_harga_perton); ?>" placeholder="Harga/ton">
                        <p class="help-block"><?php echo form_error('pbeli_harga_perton'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pbeli_toleransi') != "") ? "has-error" : "" ?>">
                        <label>Toleransi <req>*</req></label>
                        <input class="form-control" type="number" name="pbeli_toleransi" value="<?php echo set_value('pbeli_toleransi', $pjbb_beli->pbeli_toleransi); ?>" placeholder="Toleransi">
                        <p class="help-block"><?php echo form_error('pbeli_toleransi'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pbeli_pelabuhan_muat') != "") ? "has-error" : "" ?>">
                        <label>Pelabuhan Muat <req>*</req></label>
                        <input class="form-control" name="pbeli_pelabuhan_muat" value="<?php echo set_value('pbeli_pelabuhan_muat', $pjbb_beli->pbeli_pelabuhan_muat); ?>" placeholder="Pelabuhan Muat">
                        <p class="help-block"><?php echo form_error('pbeli_pelabuhan_muat'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pbeli_pelabuhan_bongkar') != "") ? "has-error" : "" ?>">
                        <label>Pelabuhan Bongkar <req>*</req></label>
                        <input class="form-control" name="pbeli_pelabuhan_bongkar" value="<?php echo set_value('pbeli_pelabuhan_bongkar', $pjbb_beli->pbeli_pelabuhan_bongkar); ?>" placeholder="Pelabuhan Bongkar">
                        <p class="help-block"><?php echo form_error('pbeli_pelabuhan_bongkar'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pbeli_pembayaran1') != "") ? "has-error" : "" ?>">
                        <label>Pembayaran 1 <req>*</req></label>
                        <input class="form-control" name="pbeli_pembayaran1" value="<?php echo set_value('pbeli_pembayaran1', $pjbb_beli->pbeli_pembayaran1); ?>" placeholder="Pembayaran 1">
                        <p class="help-block"><?php echo form_error('pbeli_pembayaran1'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pbeli_pembayaran2') != "") ? "has-error" : "" ?>">
                        <label>Pembayaran 2 <req>*</req></label>
                        <input class="form-control" name="pbeli_pembayaran2" value="<?php echo set_value('pbeli_pembayaran2', $pjbb_beli->pbeli_pembayaran2); ?>" placeholder="Pembayaran 2">
                        <p class="help-block"><?php echo form_error('pbeli_pembayaran2'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pbeli_pembayaran3') != "") ? "has-error" : "" ?>">
                        <label>Pembayaran 3 <req>*</req></label>
                        <input class="form-control" name="pbeli_pembayaran3" value="<?php echo set_value('pbeli_pembayaran3', $pjbb_beli->pbeli_pembayaran3); ?>" placeholder="Pembayaran 3">
                        <p class="help-block"><?php echo form_error('pbeli_pembayaran3'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pbeli_ttd') != "") ? "has-error" : "" ?>">
                        <label>Tanda Tangan <req>*</req></label>
                        <input class="form-control" name="pbeli_ttd" value="<?php echo set_value('pbeli_ttd', $pjbb_beli->pbeli_ttd); ?>" placeholder="Tanda Tangan">
                        <p class="help-block"><?php echo form_error('pbeli_ttd'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pbeli_no_rek') != "") ? "has-error" : "" ?>">
                        <label>No. Rekening <req>*</req></label>
                        <input class="form-control" name="pbeli_no_rek" value="<?php echo set_value('pbeli_no_rek', $pjbb_beli->pbeli_no_rek); ?>" placeholder="No. Rekening">
                        <p class="help-block"><?php echo form_error('pbeli_no_rek'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pbeli_bank') != "") ? "has-error" : "" ?>">
                        <label>Bank <req>*</req></label>
                        <input class="form-control" name="pbeli_bank" value="<?php echo set_value('pbeli_bank', $pjbb_beli->pbeli_bank); ?>" placeholder="Bank">
                        <p class="help-block"><?php echo form_error('pbeli_bank'); ?></p>
                    </div>
                  </div>
                </div>

              </div>
              <div class="col-md-12">
                <hr>
              </div>
            </div>
      
            <div class="box-footer">
              <div class="clearfix pull-right">
                <a href="<?php echo $current_context; ?>" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </div><!-- /.box-footer -->
          </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!--/.col (right) -->
  </div>   <!-- /.row -->
  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-md-offset-3">
      <p></p>
      <p class="text-center icon-page"><i class="fa fa-home fa-5x"></i></p>
      <br>
      <div class="callout callout-info">
        <h4><i class="fa fa-bullhorn fa-fw"></i> Learn More</h4>
        <p>Learn More about user <a href="">here</a>. Or <a href="">contact us</a> for more information</p>
      </div>
    </div>
  </div>
</section><!-- /.content -->