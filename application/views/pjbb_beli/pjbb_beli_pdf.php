<div style="margin: 40px;text-align: justify;">
	<div align="center"><h3><u>PERJANJIAN JUAL – BELI BATUBARA</u><br><span style="font-size: 12px">Nomor : 01/PJBB/05/2018</span></h3>
	<br></div>


	Pada hari ini, Rabu  Tanggal  Satu Bulan Mei Tahun Dua Ribu Delapan Belas (01/05/2018) yang bertanda tangan dibawah ini:
	<br><br>
	<ol style="margin-left: -17px">
		<li>ABCDEF :<br>
		Selaku Direktur PT. ABC, suatu perusahaan yang didirikan berdasarkan Hukum Republik Indonesia yang bertempat kedudukan di Bandung dalam hal ini bertindak untuk dan atas nama PT. ABC selanjutnya  disebut PEMBELI dan atau PIHAK KEDUA<br><br></li>
		<li>Bambang Heryanto :<br> Selaku Direktur PT. LUMBUNG JAYA INTERNASIONAL, suatu perusahaan yang didirikan berdasarkan Hukum Republik Indonesia yang bertempat kedudukan di Komplek Perkantoran Cirebon Binsis Center (CBC) Jl. Tuparev,  Cirebon Jawa Barat  dalam hal ini bertindak  untuk dan atas nama PT. LUMBUNG JAYA INTERNASIONAL selanjutnya  disebut PEMBELI dan atau PIHAK KEDUA<br><br></li>
	</ol>

	PIHAK PENJUAL dan PIHAK PEMBELI (secara bersama-sama disebut “PARA PIHAK” dalam kedudukannya seperti tersebut di atas, dengan ini menerangkan terlebih dahulu hal-hal berikut :<br><br>
	a.	PIHAK PERTAMA bermaksud menjual Batubara resmi dan sah menurut Hukum dan Undang-Undang Negara Republik Indonesia.<br>
	b.	PIHAK KEDUA membeli Batubara untuk keperluan usaha perdagangan yang dilakukannya maupun untuk keperluannya sendiri.<br><br>

	Dengan tetap mengindahkan peraturan perundang-undangan yang berlaku bagi PARA PIHAK dan dalam batas – batas kewenganan yang ada, dengan ini KEDUA BELAH PIHAK menyatakan sepakat untuk melakukan kerjasama JUAL – BELI  BATUBARA dimana PARA PIHAK tunduk dan patuh terhadap ketentuan yang tertuang dalam pasal-pasal sebagai  berikut :<br>

	<div align="center">Pasal 1.<br>SIFAT PERJANJIAN</div>
	<ol style="margin-left: -17px">
		<li>PIHAK PERTAMA menjual Batubara kepada PIHAK KEDUA dengan kuantitas dan kualitas sebagaimana dimaksud dalam Pasal-pasal dalam perjanjian ini. Batubara yang diperjual-belikan menurut Perjanjian ini adalah Batubara yang AMAN tidak dalam KONDISI Sita jaminan SERTA TIDAK dalam penguasaan dan persengketaan dengan pihak manapun.</li>
		<li>PIHAK KEDUA bersedia dan sanggup membeli Batubara kepada PIHAK PERTAMA sebagaimana dimaksud pada Pasal 1 ayat 1 dan Pasal 2 Perjanjian ini.</li>
	</ol>
	<br>

	<div align="center">Pasal 2.<br>KOMODITI</div>
	<ol style="margin-left: -17px">
		<li>Komoditi : Batubara Steam Coal.</li>
		<li>Spesifikasi	: NON SPEC (Typical GAR 20 Kcal/Kg)</li>
		<li>Kondisi	: ABC</li>
		<li>Asal Batubara	: Batubara yang diperjual-belikan sebagaimana dimaksud berasal (sumber) dari Tambang
			  PT. ABC  Jetty ABC  Kalimatan ABC</li>
	</ol>
	<br>
	<br>
	<div align="center">Pasal 3.<br>KUANTITAS<</div>
	<ol style="margin-left: -17px">
		<li>Kuantitas Batubara yang diperjual-belikan sebagaimana dimaksud dalam perjanjian ini adalah sebanyak 12 MT (12 metrik ton) dengan toleransi +- 5 %.</li>
		<li>Kuantitas batubara yang diperjualbelikan berdasarkan Bill of Lading (BL) dan Cargo Manifest (CM) yang diterbitkan oleh Surveyor independen yang telah disepakati oleh Para Pihak.</li>
		<li>Pelaksanaan pengapalan ini, PIHAK KEDUA akan memberitahukan kepada PIHAK PERTAMA secara tertulis dengan menerbitkan "Shipping Instruction" (SI) dan dikirim melalui facsimile dan atau email mengenai kesiapan pengapalan batubara. </li>
	</ol>
	<br>

	<div align="center">Pasal 4.<br>HARGA DAN CARA PEMBAYARAN</div>
	<ol style="margin-left: -17px">
		<li>PARA PIHAK sepakat dan setuju bahwa Harga Batubara yang diperjual-belikan sebagaimana dimaksud dalam perjanjian ini adalah Rp 200.000 per metric ton 123</li>
		<li>PARA PIHAK setuju untuk persyaratan dan atau cara pembayaran transaksi ini adalah PEMBELI membayar kepada PENJUAL secara tunai/transfer  melalui bank,  sesuai dengan kuantitas   yang diperjanjikan dengan termin sebagai berikut:
			<ol type="a">
				<li>Pembayaran I : ................................................</li>
				<li>Pembayaran II :	................................................</li>
				<li>Pembayaran III : .................................................</li>
			</ol>
		</li>
		<li>PIHAK KEDUA (PEMBELI) bersedia melakukan pembayaran tersebut diatas dengan cara Transfer ke rekening Bank PIHAK PERTAMA (PENJUAL),  adalah sebagai berikut :<br>Atas Nama	:  PT. ABC <br>Bank 	:  MANDIRI (Persero) Tbk<br>No Rekening	:  13.000.668.13.000
		</li>
	</ol>
	<br>

	<div align="center">Pasal 5<br>JADWAL PENGIRIMAN, PELABUHAN MUAT</div>
	<ol style="margin-left: -17px">
		<li>Pemuatan/loading ke atas tongkang akan dilakukan  oleh PIHAK PENJUAL yang disetujui oleh PIHAK PEMBELI.</li>
		<li>Pemuatan Batubara keatas tongkang (Barge) yang akan dilaksanakan sebagaimana dimaksud dalam perjanjian ini adalah di Jetty Kalimantan</li>
	</ol>
	<br>

	<div align="center">Pasal 6<br>PENETAPAN KUANTITAS</div>
	<ol style="margin-left: -17px">
		<li>Penentuan kuantitas batubara dimaksud dalam ketentuan ini harus telah selesai dilakukan setelah batubara yang dimuat ke atas Tongkang (Barge).</li>
		<li>Kuantitas batubara disepakati berdasarkan surat hasil Draught Survey Report yang diterbitkan oleh Surveyor Independen dan disetujui oleh PARA PIHAK.</li>
		<li>Biaya yang timbul akibat pelaksanaan tersebut sebagaimana dimaksud menjadi beban dan tanggung jawab PIHAK PENJUAL.</li>
	</ol>
	<br>

	<div align="center">Pasal 7<br>DOKUMEN</div>
	Pihak PENJUAL berkewajiban menyiapkan batubara tersebut full document dan sudah termasuk pajak, royalti dan fasilitasnya, berikut disertakan dokumen batubara lengkap adalah sebagai berikut :
	<ol type="a">
		<li>Rekomendasi pengangkutan dan penjualan mineral dan batubara dari Dinas Pertambangan dan Energi setempat</li>
		<li>Surat Keterangan Dokumen, RKBM, PnBP, IUP OP, Ijin Jetty (TUKS), COW, DSR</li>
		<li>Surat Kirim Barang (SKB), IBM</li>
		<li>Surat Keterangan Asal Barang (SKAB)</li>
		<li>Draft Survey Report, Certificate of Draft Survey, Certificate of Weight, Laporan Hasil Verifikasi dari Surveyor.</li>
	</ol>
	<br>
	<br>
	<br>
	<br>
	<div align="center">Pasal 8<br>KEADAAN MEMAKSA (FORCE MAJEURE)</div>
	<ol style="margin-left: -17px">
		<li>PARA PIHAK dapat dibebaskan dari kewajiban untuk melaksanakan Perjanjian ini ini, baik sebagian maupun keseluruhan apabila hal tersebut disebabkan karena Force Majeure.</li>
		<li>Dengan Force Majeure dimaksud suatu keadaan diluar kemampuan KEDUA BELAH PIHAK seperti bencana alam, pemogokan masal, perang dan tindakan pemerintah.</li>
		<li>Apabila terjadi Force Majeure pihak yang bersangkutan akan memberitahukan kepada pihak lainnya secara tertulis selambat-lambatnya 7 x 24 (tujuh kali dua puluh empat) jam dari Force Majeure ini dinyatakan oleh Pejabat yang berwenang ditempat terjadinya Force Majeure.</li>
	</ol>
	<br>

	<div align="center">Pasal  9<br>ADDENDUM</div>
	<p>Hal-hal lain yang belum diatur dalam perjanjian ini, diputuskan dengan persetujuan KEDUA BELAH PIHAK dan dimuat dalam ADDENDUM yang merupakan bagian yang tidak terpisahkan dari perjanjian ini.</p>
	<br>

	<div align="center">Pasal 10<br>PERSELISIHAN</div>
	<ol style="margin-left: -17px">
		<li>PARA PIHAK sepakat bahwa apabila terjadi perselisihan dalam melaksanakan Perjanjian ini, maka terlebih dahulu akan diselesaikan dengan cara musyawarah untuk mufakat.</li>
		<li>Dan apabila dalam waktu 10 (sepuluh) hari kerja sejak timbulnya permintaan untuk menyelesaikan perselisihan dan penyelesaian tidak dapat dicapai melalui musyawarah untuk mufakat, maka PARA PIHAK sepakat untuk menyelesaikan perselisihan ini melalui Pengadilan Negeri Cirebon – Jawa Barat.</li>
	</ol>

	<div align="center">Pasal 11<br>PENUTUP</div>
	<p>Demikian perjanjian ini dibuat dalam 2 (dua) rangkap asli, masing-masing mempunyai bunyi dan kekuatan hukum yang sama, setelah ditandatangani oleh wakil-wakil sah  para pihak diatas kertas bermaterai cukup pada hari dan tanggal sebagaimana disebutkan pada bagian awal perjanjian ini dan mengikat para pihak.</p>
	<br><br><br><br>
	<table>
		<tr>
			<td style="width: 700px">PIHAK PENJUAL<br>PT. ABC<br><br><br><br><u>ABCDEF</u><br>Direktur</td>
			<td>PIHAK PEMBELI<br>PT. LUMBUNG JAYA INTERNASIONAL<br><br><br><br><u>Bambang Heryanto</u><br>Direktur</td>
		</tr>
	</table>
</div>
