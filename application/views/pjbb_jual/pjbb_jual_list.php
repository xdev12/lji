<!--Content Header (Page header) -->
<section class="content-header">
  <h1>
    Pjbb jual
    <small>list page</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Pjbb jual</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <?php
        $message = $this->session->flashdata('message');
        $type_message = $this->session->flashdata('type_message');
        echo (!empty($message) && $type_message=="success") ? ' <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-check"></i> Great!</h4>'.$message.'</div></div>': '';
        echo (!empty($message) && $type_message=="error") ? '   <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-exclamation-triangle"></i> Uh-Oh!</h4>'.$message.'</div></div>': '';
    ?>
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data Pjbb jual <span class="badge"><?php echo $total_rows; ?> Data</span></h3>
          <div class="box-tools pull-right">
            <i class="fa fa-refresh fa-spin fa-lg fa-fw loading-icon hide" aria-hidden="true"></i>
            <span class="sr-only">Refreshing...</span>
            <a href="<?php echo $current_context . '/add/'; ?>" class="btn btn-xs bg-light-blue">
                <i class="fa fa-plus"></i>&nbsp; Add
            </a>
            <a href="javascript:;" class="btn btn-xs bg-orange refresh-page"><i class="fa fa-refresh"></i> Refresh</a>
            <button class="btn btn-box-tool btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div><!-- /.box-header -->

         <div class="box-body">
          <div class="row">
            <div class="col-md-2">
              <dl class="text-right">
                <dt><h5>Filter Data</h5></dt>
                <dd>type/ select to find or filter data</dd>
              </dl>
            </div>
          <div class="col-md-10">
            <form method="post" action="<?php echo $current_context . 'search'; ?>">
                <?php $key = (object) $this->session->userdata('filter_pjbb_jual'); ?>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Month</label>
                      <select class="form-control select2me" name="month" id="bulan" data-placeholder="Select..." style="width: 100%;">
                          <option value=""></option>
                          <?php
                          foreach ($this->public_function->ref_bulan() as $bulan => $value) {
                              if ($bulan == $key->month) {
                                  echo "<option value='$bulan' selected>$value</option>";
                              } else {
                                  echo "<option value='$bulan'>$value</option>";
                              }
                          }
                          ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Year</label>
                        <?php $year = range(date('Y'), 2000); ?>
                        <select class="form-control select2me" name="year" id="tahun" data-placeholder="Select..." style="width: 100%;">
                            <option value=""></option>
                            <?php
                            foreach ($year as $y => $value) {
                                if ($value == $key->year) {
                                    echo "<option value='$value' selected>$value</option>";
                                } else {
                                    echo "<option value='$value'>$value</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>
                  </div>
                 <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Nama Pembeli</label>
                      <input class="form-control " name="pjual_nama" value="<?php echo $key->pjual_nama; ?>" placeholder="Nama Pembeli">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="clearfix pull-left">
                        <input type="hidden" id="search" name="search" value="true">
                        <button type="submit" class="btn btn-sm btn-primary">Search</button>
                        <button type="button" class="btn btn-sm btn-default" onclick="location.href='<?php echo $current_context; ?>'">Clear</button>
                    </div>
                  </div>
                </div> 
              </form>
          </div>
         </div>
        </div><!-- /.box-body -->

      <div class="box-body">
        <div class="row">
          <div class="col-md-2">
              <dl class="text-right">
                <dt><h5>Data</h5></dt>
                <dd>data table,all data restore here</dd>
              </dl>
              <div class="clearfix text-right">
                <p><a href="<?php echo $current_context . 'add/'; ?>" class="btn btn-sm bg-light-blue btn-block">
                    <i class="fa fa-plus"></i>&nbsp; Add
                </a></p>
                <p><a href="<?php echo $current_context . 'edit_kontrak/1'; ?>" class="btn btn-sm bg-light-blue btn-block">
                    <i class="fa fa-pencil"></i>&nbsp; Edit Kontrak
                </a></p>
                <p>
                  <span class="slctd-dta hide"></span>
                </p>
              </div>
            </div>  

            <div class="col-md-10">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped" id="table_data">
                        <thead>
                            <tr>
                                <th class="table-checkbox">No.</th>
                                <th>Nama Pembeli</th>
                                <th>Perusahaan Pembeli</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <?php $i = $offset + 1;
                            foreach ($pjbb_jual as $row) { ?>
                            <tr>
                                <td><?= $row->pjual_no; ?></td>
                                <td><?= $row->pjual_nama; ?></td>
                                <td><?= $row->pjual_perusahaan; ?></td>
                                <td><?= ($row->pjual_status == '1')?'Approve':'Waiting for Approve'; ?></td>
                                <td class="td-btn">
                                    <a href="<?php echo $current_context . '/edit'  .'/'. $row->pjual_id ?>" class="badge bg-green"><i class="fa fa-edit fa-fw"></i>View</a>
                                    <a href="#" data-href="<?php echo $current_context . 'delete'  .'/'. $row->pjual_id ?>" data-toggle="modal" data-target="#deleteModal"  class="badge bg-red"><i class="fa fa-trash-o fa-fw"></i>Delete</a>
                                    <a href="<?php echo $current_context . '/export_pdf/'. $row->pjual_id ?>" class="badge bg-green"><i class="fa fa-edit fa-fw"></i>Export To Pdf</a>
                                </td>
                            </tr>
                        <?php $i++; } ?>
                        <tfoot> 
                            <tr>
                                <th class="table-checkbox">No.</th>
                                <th>Nama Pembeli</th>
                                <th>Perusahaan Pembeli</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>    
                    </table>
                </div>
            </div>
        </div>
       </div> <!-- /.box-body -->
       <div class="box-footer clearfix">
            <div class="col-sm-6">
              
            </div>
            <div class="col-sm-6">
              <div class="pull-right">
              <?php echo $pagination; ?>
              </div>
            </div>
        </div> 
      </div><!-- /.box -->
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-md-offset-3">
      <p></p>
      <p class="text-center icon-page"><i class="fa fa-users fa-5x"></i></p>
      <br>
      <div class="callout callout-info">
        <h4><i class="fa fa-bullhorn fa-fw"></i> Learn More</h4>
        <p>Learn more about Pjbb jual <a href="">here</a>. Or <a href="">contact us</a> for more information</p>
      </div>
    </div>
  </div>
</section><!-- /.content