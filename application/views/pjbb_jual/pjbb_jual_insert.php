<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Pjbb jual
    <small><?php echo ($edit)?'Edit':'Insert'; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?php echo $current_context; ?>">pjbb jual</a></li>
    <li class="active"><?php echo ($edit)?'Edit':'Insert'; ?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <?php
        $message = $this->session->flashdata('message');
        $type_message = $this->session->flashdata('type_message');
        echo (!empty($message) && $type_message=="success") ? ' <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-check"></i> Great!</h4>'.$message.'</div></div>': '';
        echo (!empty($message) && $type_message=="error") ? '   <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-exclamation-triangle"></i> Uh-Oh!</h4>'.$message.'</div></div>': '';
    ?>
    <!-- right column -->
    <div class="col-md-12">
      <!-- general form elements disabled -->
      <div class="box">
        <div class="box-header with-border text-right">
          <h3 class="box-title">Add</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <form role="form" method="POST" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-2">
                <dl class="text-right">
                  <dt><h5>Pjbb jual</h5></dt>
                  <dd>pjbb information</dd>
                </dl>
              </div>
              <div class="col-md-8">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pjual_no') != "") ? "has-error" : "" ?>">
                        <label>No. PJBB <req>*</req></label>
                        <input class="form-control" name="pjual_no" value="<?php echo ($edit)?'$pjbb_jual->pjual_no':$code; ?>" placeholder="No. PJBB" readonly required>
                        <p class="help-block"><?php echo form_error('pjual_no'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pjual_nama') != "") ? "has-error" : "" ?>">
                        <label>Nama Pembeli <req>*</req></label>
                        <input class="form-control" name="pjual_nama" value="<?php echo set_value('pjual_nama', $pjbb_jual->pjual_nama); ?>" placeholder="Nama Pembeli" required>
                        <p class="help-block"><?php echo form_error('pjual_nama'); ?></p>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pjual_perusahaan') != "") ? "has-error" : "" ?>">
                        <label>Perusahaan <req>*</req></label>
                        <input class="form-control" name="pjual_perusahaan" value="<?php echo set_value('pjual_perusahaan', $pjbb_jual->pjual_perusahaan); ?>" placeholder="Perusahaan" required>
                        <p class="help-block"><?php echo form_error('pjual_perusahaan'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pjual_alamat') != "") ? "has-error" : "" ?>">
                        <label>Alamat <req>*</req></label>
                        <textarea class="form-control" name="pjual_alamat" placeholder="Alamat" required><?php echo set_value('pjual_alamat', $pjbb_jual->pjual_alamat); ?></textarea>
                        <p class="help-block"><?php echo form_error('pjual_alamat'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pjual_spek') != "") ? "has-error" : "" ?>">
                        <label>Spesifikasi <req>*</req></label>
                        <input class="form-control" name="pjual_spek" value="<?php echo set_value('pjual_spek', $pjbb_jual->pjual_spek); ?>" placeholder="Spesifikasi" required>
                        <p class="help-block"><?php echo form_error('pjual_spek'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pjual_kondisi') != "") ? "has-error" : "" ?>">
                        <label>Kondisi <req>*</req></label>
                        <input class="form-control" name="pjual_kondisi" value="<?php echo set_value('pjual_kondisi', $pjbb_jual->pjual_kondisi); ?>" placeholder="Kondisi" required>
                        <p class="help-block"><?php echo form_error('pjual_kondisi'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pjual_asal_batu') != "") ? "has-error" : "" ?>">
                        <label>Asal Batu <req>*</req></label>
                        <textarea class="form-control" name="pjual_asal_batu" placeholder="Asal Batu" required><?php echo set_value('pjual_asal_batu', $pjbb_jual->pjual_asal_batu); ?></textarea>
                        <p class="help-block"><?php echo form_error('pjual_asal_batu'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pjual_alamat_asal_batu') != "") ? "has-error" : "" ?>">
                        <label>Alamat Asal Batu <req>*</req></label>
                        <textarea class="form-control" name="pjual_alamat_asal_batu" placeholder="Alamat Asal Batu" required><?php echo set_value('pjual_alamat_asal_batu', $pjbb_jual->pjual_alamat_asal_batu); ?></textarea>
                        <p class="help-block"><?php echo form_error('pjual_alamat_asal_batu'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pjual_jumlah_muatan') != "") ? "has-error" : "" ?>">
                        <label>Jumlah Muatan <req>*</req></label>
                        <input class="form-control" type="number" name="pjual_jumlah_muatan" value="<?php echo set_value('pjual_jumlah_muatan', $pjbb_jual->pjual_jumlah_muatan); ?>" placeholder="Jumlah Muatan" required>
                        <p class="help-block"><?php echo form_error('pjual_jumlah_muatan'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pjual_harga_perton') != "") ? "has-error" : "" ?>">
                        <label>Harga/ton <req>*</req></label>
                        <input class="form-control" type="number" name="pjual_harga_perton" value="<?php echo set_value('pjual_harga_perton', $pjbb_jual->pjual_harga_perton); ?>" placeholder="Harga/ton" required>
                        <p class="help-block"><?php echo form_error('pjual_harga_perton'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pjual_toleransi') != "") ? "has-error" : "" ?>">
                        <label>Toleransi <req>*</req></label>
                        <input class="form-control" type="number" name="pjual_toleransi" value="<?php echo set_value('pjual_toleransi', $pjbb_jual->pjual_toleransi); ?>" placeholder="Toleransi" required>
                        <p class="help-block"><?php echo form_error('pjual_toleransi'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pjual_pelabuhan_muat') != "") ? "has-error" : "" ?>">
                        <label>Pelabuhan Muat <req>*</req></label>
                        <input class="form-control" name="pjual_pelabuhan_muat" value="<?php echo set_value('pjual_pelabuhan_muat', $pjbb_jual->pjual_pelabuhan_muat); ?>" placeholder="Pelabuhan Muat" required>
                        <p class="help-block"><?php echo form_error('pjual_pelabuhan_muat'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pjual_pelabuhan_bongkar') != "") ? "has-error" : "" ?>">
                        <label>Pelabuhan Bongkar <req>*</req></label>
                        <input class="form-control" name="pjual_pelabuhan_bongkar" value="<?php echo set_value('pjual_pelabuhan_bongkar', $pjbb_jual->pjual_pelabuhan_bongkar); ?>" placeholder="Pelabuhan Bongkar" required>
                        <p class="help-block"><?php echo form_error('pjual_pelabuhan_bongkar'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pjual_pembayaran1') != "") ? "has-error" : "" ?>">
                        <label>Pembayaran 1 <req>*</req></label>
                        <input class="form-control" name="pjual_pembayaran1" value="<?php echo set_value('pjual_pembayaran1', $pjbb_jual->pjual_pembayaran1); ?>" placeholder="Pembayaran 1" required>
                        <p class="help-block"><?php echo form_error('pjual_pembayaran1'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pjual_pembayaran2') != "") ? "has-error" : "" ?>">
                        <label>Pembayaran 2 <req>*</req></label>
                        <input class="form-control" name="pjual_pembayaran2" value="<?php echo set_value('pjual_pembayaran2', $pjbb_jual->pjual_pembayaran2); ?>" placeholder="Pembayaran 2" required>
                        <p class="help-block"><?php echo form_error('pjual_pembayaran2'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pjual_pembayaran3') != "") ? "has-error" : "" ?>">
                        <label>Pembayaran 3 <req>*</req></label>
                        <input class="form-control" name="pjual_pembayaran3" value="<?php echo set_value('pjual_pembayaran3', $pjbb_jual->pjual_pembayaran3); ?>" placeholder="Pembayaran 3" required>
                        <p class="help-block"><?php echo form_error('pjual_pembayaran3'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pjual_ttd') != "") ? "has-error" : "" ?>">
                        <label>Tanda Tangan <req>*</req></label>
                        <input class="form-control" name="pjual_ttd" value="<?php echo set_value('pjual_ttd', $pjbb_jual->pjual_ttd); ?>" placeholder="Tanda Tangan" required>
                        <p class="help-block"><?php echo form_error('pjual_ttd'); ?></p>
                    </div>
                  </div>
                </div>

              </div>
              <div class="col-md-12">
                <hr>
              </div>
            </div>
      
            <div class="box-footer">
              <div class="clearfix pull-right">
                <a href="<?php echo $current_context; ?>" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </div><!-- /.box-footer -->
          </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!--/.col (right) -->
  </div>   <!-- /.row -->
  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-md-offset-3">
      <p></p>
      <p class="text-center icon-page"><i class="fa fa-home fa-5x"></i></p>
      <br>
      <div class="callout callout-info">
        <h4><i class="fa fa-bullhorn fa-fw"></i> Learn More</h4>
        <p>Learn More about user <a href="">here</a>. Or <a href="">contact us</a> for more information</p>
      </div>
    </div>
  </div>
</section><!-- /.content -->