<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Shipping Instruction
    <small><?php echo ($edit)?'Edit':'Insert'; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?php echo $current_context; ?>">Shipping Instruction</a></li>
    <li class="active"><?php echo ($edit)?'Edit':'Insert'; ?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <?php
        $message = $this->session->flashdata('message');
        $type_message = $this->session->flashdata('type_message');
        echo (!empty($message) && $type_message=="success") ? ' <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-check"></i> Great!</h4>'.$message.'</div></div>': '';
        echo (!empty($message) && $type_message=="error") ? '   <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-exclamation-triangle"></i> Uh-Oh!</h4>'.$message.'</div></div>': '';
    ?>
    <!-- right column -->
    <div class="col-md-12">
      <!-- general form elements disabled -->
      <div class="box">
        <div class="box-header with-border text-right">
          <h3 class="box-title">Add</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <form role="form" method="POST" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-2">
                <dl class="text-right">
                  <dt><h5>Shipping Instruction</h5></dt>
                  <dd>Shipping information</dd>
                </dl>
              </div>
              <div class="col-md-8">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('si_no') != "") ? "has-error" : "" ?>">
                        <label>No. Shipping <req>*</req></label>
                        <input class="form-control" name="si_no" value="<?php echo set_value('si_no', $shipping_instruction->si_no); ?>" placeholder="No. Shipping">
                        <p class="help-block"><?php echo form_error('si_no'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('si_pjbb_no') != "") ? "has-error" : "" ?>">
                        <label>No. PJBB <req>*</req></label>
                        <select class="form-control" name="si_pjbb_no" required onchange="get_data(this)" data-placeholder="Pilih..." >
                            <option value="">--Pilih--</option>
                            <?php
                            foreach ($pjbb_jual as $pb) {
                                if ($pb->pjual_no == set_value('si_pjbb_no', $pembayaran_utang->si_pjbb_no)) {
                                    echo "<option value='$pb->pjual_no' selected>$pb->pjual_no</option>";
                                } else {
                                    echo "<option value='$pb->pjual_no'>$pb->pjual_no</option>";
                                }
                            }
                            ?>
                        </select>
                        <p class="help-block"><?php echo form_error('si_pjbb_no'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('si_to') != "") ? "has-error" : "" ?>">
                        <label>Kepada </label>
                        <textarea class="form-control" name="si_to" placeholder="Kepada">
                            <?php echo set_value('si_to', $shipping_instruction->si_to); ?>
                        </textarea>
                        <p class="help-block"><?php echo form_error('si_to'); ?></p>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('si_shipper') != "") ? "has-error" : "" ?>">
                        <label>Shipper <req>*</req></label>
                        <input class="form-control pjual_perusahaan" name="si_shipper" value="<?php echo set_value('si_shipper', $shipping_instruction->si_shipper); ?>" placeholder="Perusahaan">
                        <p class="help-block"><?php echo form_error('si_shipper'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('si_consignee') != "") ? "has-error" : "" ?>">
                        <label>Consignee <req>*</req></label>
                        <textarea class="form-control" name="si_consignee" placeholder="Consignee"><?php echo set_value('si_consignee', $shipping_instruction->si_consignee); ?></textarea>
                        <p class="help-block"><?php echo form_error('si_consignee'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('si_notif_address') != "") ? "has-error" : "" ?>">
                        <label>Notify Address <req>*</req></label>
                        <textarea class="form-control" name="si_notif_address" placeholder="Notification Address">
                            <?php echo set_value('si_notif_address', $shipping_instruction->si_notif_address); ?>
                        </textarea>
                        <p class="help-block"><?php echo form_error('si_notif_address'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('si_pol') != "") ? "has-error" : "" ?>">
                        <label>Port Of Loading <req>*</req></label>
                        <input class="form-control" name="si_pol" value="<?php echo set_value('si_pol', $shipping_instruction->si_pol); ?>" placeholder="Port Of Loading">
                        <p class="help-block"><?php echo form_error('si_pol'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('si_pod') != "") ? "has-error" : "" ?>">
                        <label>Port Of Discharge <req>*</req></label>
                        <input class="form-control" name="si_pod" value="<?php echo set_value('si_pod', $shipping_instruction->si_pod); ?>" placeholder="Port Of Discharge">
                        <p class="help-block"><?php echo form_error('si_pod'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('si_date_load') != "") ? "has-error" : "" ?>">
                        <label>Date Of Loading </label>
                        <input class="form-control" name="si_date_load" value="<?php echo set_value('si_date_load', $shipping_instruction->si_date_load); ?>" placeholder="Date Of Loading">
                        <p class="help-block"><?php echo form_error('si_date_load'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('si_vessel') != "") ? "has-error" : "" ?>">
                        <label>Vessel <req>*</req></label>
                        <input class="form-control" name="si_vessel" value="<?php echo set_value('si_vessel', $shipping_instruction->si_vessel); ?>" placeholder="Vessel">
                        <p class="help-block"><?php echo form_error('si_vessel'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('si_cargo') != "") ? "has-error" : "" ?>">
                        <label>Cargo </label>
                        <input class="form-control" name="si_cargo" value="<?php echo set_value('si_cargo', $shipping_instruction->si_cargo); ?>" placeholder="Cargo">
                        <p class="help-block"><?php echo form_error('si_cargo'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('si_desc') != "") ? "has-error" : "" ?>">
                        <label>Description Of Good <req>*</req></label>
                        <input class="form-control" name="si_desc" value="<?php echo set_value('si_desc', $shipping_instruction->si_desc); ?>" placeholder="Description Of Good">
                        <p class="help-block"><?php echo form_error('si_desc'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('si_mark') != "") ? "has-error" : "" ?>">
                        <label>Mark <req>*</req></label>
                        <input class="form-control" name="si_mark" value="<?php echo set_value('si_mark', $shipping_instruction->si_mark); ?>" placeholder="Mark">
                        <p class="help-block"><?php echo form_error('si_mark'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('si_agen_kapal') != "") ? "has-error" : "" ?>">
                        <label>Agen Kapal <req>*</req></label>
                        <input class="form-control" name="si_agen_kapal" value="<?php echo set_value('si_agen_kapal', $shipping_instruction->si_agen_kapal); ?>" placeholder="Agen Kapal">
                        <p class="help-block"><?php echo form_error('si_agen_kapal'); ?></p>
                    </div>
                  </div>
                </div>
<!-- 
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('si_status') != "") ? "has-error" : "" ?>">
                        <label>Status <req>*</req></label>
                        <input class="form-control" name="si_status" value="<?php echo set_value('si_status', $shipping_instruction->si_status); ?>" placeholder="Pembayaran 1">
                        <p class="help-block"><?php echo form_error('si_status'); ?></p>
                    </div>
                  </div>
                </div> -->
              </div>
              <div class="col-md-12">
                <hr>
              </div>
            </div>
      
            <div class="box-footer">
              <div class="clearfix pull-right">
                <a href="<?php echo $current_context; ?>" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </div><!-- /.box-footer -->
          </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!--/.col (right) -->
  </div>   <!-- /.row -->
  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-md-offset-3">
      <p></p>
      <p class="text-center icon-page"><i class="fa fa-home fa-5x"></i></p>
      <br>
      <div class="callout callout-info">
        <h4><i class="fa fa-bullhorn fa-fw"></i> Learn More</h4>
        <p>Learn More about user <a href="">here</a>. Or <a href="">contact us</a> for more information</p>
      </div>
    </div>
  </div>
</section><!-- /.content -->
<script type="text/javascript">
  function get_data(e) {
        var id = $(e).val();
        $.ajax({
            url: '<?php echo base_url() ?>shipping_instruction/get_data',
            dataType: 'json',
            type: 'POST',
            data: {id: id},
            success: function (result) {
                $('.pjual_perusahaan').val(result.pjual_perusahaan);
            }
        });

    }
</script>