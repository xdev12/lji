<div style="margin: 40px;text-align: justify;">
<div align="center"><h3><u>SHIPPING INSTRUCTION</u><br></h3></div>
<br><br><br>
<table>
	<tr>
		<td>No. :</td>
		<td>005/SI-LJI/RR-RUI/IV/18</td>
	</tr>
	<tr>
		<td>To. :</td>
		<td> 1. PT. BINUANG JAYA MULIA<br>2. CV. RAHMA RAHMAN<br>3. PT. BARACUDA SAMUDRA PERKASA<br>4. CV. ARPISUDA</td>
	</tr>
</table>
<br><br>
Dear Sir,<br>
Please kindly arrange our Bill Of Lading as follows:<br>
1. Shipper : CV. RAHMA RAHMAN<br>
2. Consignee : PT. LUMBUNG JAYA INTERNASIONAL<br>
3. Notify Address : PT. DIANTAMA CIPTA SEJAHTERA<br>
4. Port Of Loading : Jetty RUI – Sungai Danau Kalimantan Selatan<br>
5. Port Of Discharge : Jetty Pelindo Cirebon Jawa Barat<br>
6. Date Of Loading : 03-04 April 2018<br>
7. Vessel : TB. Pendekar Laksana 100/ BG. Pendekar 3010 or Sub *)<br>
8. Cargo : +/- 7.500 MT<br>
9. Description Of Good : Coal in Bulk<br>
10. Mark : CLEAN ON BOARD , Freight Payable as Per Charter Party<br>
11. Agen Kapal :<br>
<br>
<br>
*) sewaktu - waktu bisa berubah<br>
Please also prepare accordingly the shipping document as follows:<br>
• Original clean on board Bill Of Lading and Three non negotiable copies<br>
• Cargo Manifest: Three original and Three copies<br>
• SKAB (Surat Keterangan Asal Barang) Dinas Pertambangan, LHV (Laporan Hasil Verifikasi) & IUP<br>
• Certificate of Weight<br>
• Certificate of Draft Survey<br><br>
Thank you for kind attention and great cooperation<br><br><br><br>

Cirebon , Maret 2018<br>
PT. LUMBUNG JAYA INTERNASIONAL<br><br><br><br>
Bambang Heryanto<br>

</div>