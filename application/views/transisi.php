<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>LJI Internal System</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <META Http-Equiv="Cache-Control" Content="no-cache">
    <META Http-Equiv="Pragma" Content="no-cache">
    <META Http-Equiv="Expires" Content="0">
    
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datetimepicker/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/toastr/build/toastr.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/dist/css/select2.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/app.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dropzone/dist/dropzone.css">
        
      <style type="text/css">
          .btn-trans{
              background-color: #3c8dbc;
              border-color: #3c8dbc;
              border-radius: 0;
              color: white;
              width: 200px;
              font-size: 14pt; 
          }
      </style>
  </head>
  <body class="hold-transition skin-red sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo base_url(); ?>dashboard" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">
            <img src="<?= base_url().'assets/images/logo.png'?>" style="padding-bottom: 10px;">
          </span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">
            <img src="<?= base_url().'assets/images/logo.png'?>" style="padding-bottom: 10px;">
          </span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Notifications: style can be found in dropdown.less -->
              
              <!-- User Account: style can be found in dropdown.less -->
              <?php
                $tanggal = date("Y-m-d");
              ?>
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- <img src="<?php // echo $photo ?>" class="user-image" alt="User Image"> -->
                  <span class="hidden-xs" style="text-transform: uppercase;"><?= $this->session->userdata('username'); ?> &nbsp;&nbsp;</span>
                  <i class="fa fa-caret-down" aria-hidden="true"></i>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                    <span class="logo-lg">
                      LJI Internal System
                    </span>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo  base_url() . "auth/logout" ?>" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>

      <!-- Content Wrapper. Contains page content -->
      <div style="background-color: white;text-align: center;">
        <p style="font-size: 15pt;padding-top: 30px;">Select Page To Manage</p>
        <a href="<?= base_url().'dashboard/coal' ?>" class="btn btn-trans">Coal Division</a><br><br>
        <a href="#" class="btn btn-trans">Truck Division</a><br><br>
        <a href="<?= base_url().'dashboard/master' ?>" class="btn btn-trans">Data Master</a><br><br>
        <a href="<?= base_url().'auth/logout' ?>" class="btn btn-trans">Logout</a><br><br>
      </div><!-- /.content-wrapper -->
      <!-- <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2017. Onlineshop.</strong> All rights reserved.
      </footer> -->
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/select2/dist/js/select2.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.min.js"></script>
    <!-- Datepicker -->
    <script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datetimepicker/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <!-- Timepicker -->
    <script src="<?php echo base_url(); ?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.js"></script>
    <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/plugins/dropzone/dist/dropzone.js"></script>
    <script src="<?php echo base_url(); ?>assets/dist/js/pages/template.js"></script>
  
    <!-- AdminLTE for demo purposes -->
    <!-- <script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script> -->
    <!-- <script src="<?php echo base_url(); ?>assets/plugins/uniform/jquery.uniform.min.js"></script> -->
    <!-- <script src="<?php echo base_url(); ?>assets/dist/js/template.js"></script> -->
    <?php
    if($page_title == "Dashboard"){
        ?>
        <!-- Sparkline -->
        <script src="<?php echo base_url(); ?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
        <!-- jvectormap -->
        <script src="<?php echo base_url(); ?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- ChartJS 1.0.1 -->
        <script src="<?php echo base_url(); ?>assets/plugins/chartjs/Chart.min.js"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="<?php echo base_url(); ?>assets/dist/js/pages/dashboard2.js"></script>
        <?php
    }
    ?>
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Delete Data?</h4>
                </div>
                <div class="modal-body">
                    Do you want to delete this data?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <button type="button" class="btn btn-danger danger">Yes</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="deleteAll" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Delete Data?</h4>
                </div>
                <div class="modal-body">
                    Do you want to delete selected data?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <button type="button" class="btn btn-danger danger2">Yes</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add_member" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
              <form role="form" method="POST" enctype="multipart/form-data" action="<?php echo base_url() ?>member/add_ajax" id="add_member_form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Add Member</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group ">
                      <label>Name</label><input class="form-control " name="member_name" placeholder="Name"  required  maxlength=50>
                  </div>
                  <div class="form-group">
                      <label>Gender</label><br>
                      <select class="form-control" name="member_gender" data-placeholder="Pilih..." style="width: 100%;">
                        <option value="1">Male</option>
                        <option value="0">Female</option>
                      </select>
                  </div>
                  <div class="form-group">
                      <label>Address</label><textarea class="form-control" name="member_address" ></textarea>
                  </div>
                  <div class="form-group">
                      <label>Contact</label><input class="form-control " name="member_contact" placeholder="Contact"  maxlength=12>
                  </div>
                  <div class="form-group <?php echo (form_error('member_bbm') != "") ? "has-error" : "" ?>">
                      <label>Bbm</label><input class="form-control " name="member_bbm" placeholder="Bbm"  maxlength=10>
                  </div>
                  
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-primary">Yes</button>
                </div>
              </form>
            </div>
        </div>
    </div>
    <script>
      jQuery(document).ready(function() {    
         // initiate layout and plugins
        $('#deleteModal').on('show.bs.modal', function(e) {
            $(this).find('.danger').attr('onclick', 'location.href=\"' + $(e.relatedTarget).data('href') + '\"');
        });
        $('#deleteAll').on('show.bs.modal', function (e) {
            $(this).find('.danger2').attr('onclick', 'go_delete(\"' + $(e.relatedTarget).data('href') + '\")');
        });
         $(".timepicker").timepicker({
          showInputs: false
        });
         $(".datepicker").datepicker({
          showInputs: false,
          format:'yyyy-mm-dd'
        });
         $(".datetimepicker").datetimepicker({
          format:'YYYY-MM-DD LT',
        });
        $(".select2me").select2({
        });
        $(".select-nosearch").select2({
            minimumResultsForSearch: -1
        });
        $('select:not(.select2me)').select2({
          minimumResultsForSearch: -1
        });

        $("#start_date").datepicker({
            todayBtn:  1,
            showInputs: false,
            format:'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true
        }).on('changeDate', function (selected) {
            var minDate = new Date(selected.date.valueOf());
            $('#end_date').datepicker('setStartDate', minDate);
        });

        $("#end_date").datepicker({
            showInputs: false,
            format:'yyyy-mm-dd',
            autoclose: true,
            todayHighlight: true 
        }).on('changeDate', function (selected) {
                var minDate = new Date(selected.date.valueOf());
                $('#start_date').datepicker('setEndDate', minDate);
        });

        $('#add_member_form').validate({
            rules: {
                "member_name": {
                    required: true
                },
                "member_gender": {
                    required: true
                },
                "member_address": {
                    required: true
                },
                "member_contact": {
                    required: true,
                    digits: true
                }
            },
            highlight: function (e) {
                $(e).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (e) {
                $(e).closest('.form-group').removeClass('has-error');
            },
            errorClass: "help-block",
            errorElement: "p",
            submitHandler: function (form) {
                var formd = form;
                $('.overlay').show();
                $.ajax({
                    url: $(form).attr('action'),
                    type: $(form).attr('method'),
                    data: $(form).serialize(),
                    dataType: 'json'
                }).done(function (result) {
                    if (result) {
                        $('#add_member').modal('hide');
                        toastr.options = {
                            "preventDuplicates": true,
                            "positionClass": "toast-bottom-right"
                        }
                        $('.overlay').hide();
                        toastr["success"]("Data has been inserted");
                    } else {
                        toastr.options = {
                            "preventDuplicates": true,
                            "positionClass": "toast-bottom-right"
                        }
                        toastr["error"]("Something wrong with the server. Please Try Again Later.");
                    }
                });
                formd.reset();
                return false;
            }
        });

      });
      function go_delete(p_url) {
            id_obj = [];
            $('.checkboxes:checked').each(function ()
            {
                ids = $(this).data();
                var id_array = {};
                $.each(ids,function(index, value){
                    if(index !== 'uniformed'){
                        id_array[index] = value;
                    }
                });
                id_obj.push(id_array);
            }).get();
            var post = {ids: id_obj, updated_by: '<?php echo $login_user->username ?>', updated_on:'<?php echo  date("Y-m-d H:i:s") ?> "'};
            $.ajax({
                url: p_url,
                type: 'post',
                dataType: 'json',
                data: JSON.stringify(post),
                success: function (data) {
                    // console.log(data);
                    location.reload();
                }
            });
            $('#del_All').modal('hide');
        }

      // $().ready(function() {
      //     if(document.referrer != <?php echo base_url().'admin/dashboard'; ?>){ 
      //         history.pushState(null, null, 'login');
      //         window.addEventListener('popstate', function () {
      //             history.pushState(null, null, 'login');
      //         });
      //     }
      // });
   </script>
  </body>
</html>

