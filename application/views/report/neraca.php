<!--Content Header (Page header) -->
<section class="content-header">
  <h1>
    Laporan Neraca Profit Loss
    <small>list page</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">pengeluaran</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <?php
        $message = $this->session->flashdata('message');
        $type_message = $this->session->flashdata('type_message');
        echo (!empty($message) && $type_message=="success") ? ' <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-check"></i> Great!</h4>'.$message.'</div></div>': '';
        echo (!empty($message) && $type_message=="error") ? '   <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-exclamation-triangle"></i> Uh-Oh!</h4>'.$message.'</div></div>': '';
    ?>
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data pengeluaran <span class="badge"><?php echo $total_rows; ?> Data</span></h3>
          <div class="box-tools pull-right">
            <i class="fa fa-refresh fa-spin fa-lg fa-fw loading-icon hide" aria-hidden="true"></i>
            <span class="sr-only">Refreshing...</span>
            
            <a href="javascript:;" class="btn btn-xs bg-orange refresh-page"><i class="fa fa-refresh"></i> Refresh</a>
            <button class="btn btn-box-tool btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div><!-- /.box-header -->

         <div class="box-body">
          <div class="row">
            <div class="col-md-2">
              <dl class="text-right">
                <dt><h5>Filter Data</h5></dt>
                <dd>type/ select to find or filter data</dd>
              </dl>
            </div>
          <div class="col-md-10">
            <form method="post" action="<?php echo $current_context . 'search'; ?>">
                <?php $key = (object) $this->session->userdata('filter_pengeluaran'); ?>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Bulan</label>
                      <select class="form-control select2me" name="pengeluaran_bulan" id="bulan" data-placeholder="Select..." style="width: 100%;">
                          <option value=""></option>
                          <?php
                          foreach ($this->public_function->ref_bulan() as $bulan => $value) {
                              if ($bulan == $key->pengeluaran_bulan) {
                                  echo "<option value='$bulan' selected>$value</option>";
                              } else {
                                  echo "<option value='$bulan'>$value</option>";
                              }
                          }
                          ?>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Tahun</label>
                       <?php $year = range(date('Y'), 2000); ?>
                        <select class="form-control select2me" name="pengeluaran_tahun" id="tahun" data-placeholder="Select..." style="width: 100%;">
                            <option value=""></option>
                            <?php
                            foreach ($year as $y => $value) {
                                if ($value == $key->pengeluaran_tahun) {
                                    echo "<option value='$value' selected>$value</option>";
                                } else {
                                    echo "<option value='$value'>$value</option>";
                                }
                            }
                            ?>
                        </select>
                    </div>
                  </div>
                  
                  <div class="col-md-12">
                    <div class="clearfix pull-left">
                        <input type="hidden" id="search" name="search" value="true">
                        <button type="submit" class="btn btn-sm btn-primary">Search</button>
                        <button type="button" class="btn btn-sm btn-default" onclick="location.href='<?php echo $current_context; ?>'">Clear</button>
                    </div>
                  </div>
                </div> 
              </form>
          </div>
         </div>
        </div><!-- /.box-body -->

      <div class="box-body">
        <div class="row">
          <div class="col-md-2">
              <dl class="text-right">
                <dt><h5>Data</h5></dt>
                <dt><h5>Juli - 2018</h5></dt>
              </dl>
              <div class="clearfix text-right">
                
                <p>
                  <span class="slctd-dta hide"></span>
                </p>
              </div>
            </div>  

            <div class="col-md-10">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped" id="table_data">
                        <tr style="background-color: #337ab775;">
                            <td width="4%"></td>
                            <td width="66%"><b>SALDO BULAN SEBELUMNYA</b></td>
                            <td>Rp 0</td>
                        </tr>
                        <tr>
                            <td>A</td>
                            <td><b>Total Penjualan</b></td>
                            <td>Rp 0</td>
                        </tr>
                        <tr style="background-color: #337ab775;">
                            <td width="4%"></td>
                            <td><b>TOTAL PENJUALAN</b></td>
                            <td>Rp 0</td>
                        </tr>
                        <tr>
                            <td>B</td>
                            <td><b>Total Pembelian</b></td>
                            <td>Rp 0</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Batu Bara</td>
                            <td>Rp 0</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Sewa Tongkang</td>
                            <td>Rp 0</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Asuransi Cargo</td>
                            <td>Rp 0</td>
                        </tr>
                        <tr style="background-color: #337ab775;">
                            <td width="4%"></td>
                            <td><b>TOTAL PEMBELIAN</b></td>
                            <td>Rp 0</td>
                        </tr>
                        <tr>
                            <td>C</td>
                            <td><b>Total Pengeluaran</b></td>
                            <td>Rp 0</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Installment</td>
                            <td>Rp 0</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Salary</td>
                            <td>Rp 0</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Office Expenses</td>
                            <td>Rp 0</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Marketing Fee</td>
                            <td>Rp 0</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Internal Fee</td>
                            <td>Rp 0</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>Others Fee</td>
                            <td>Rp 0</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>Surveyor</td>
                            <td>Rp 0</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>Overseas Travel Hotel</td>
                            <td>Rp 0</td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>Overseas Travel Transport</td>
                            <td>Rp 0</td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>Other Expenses</td>
                            <td>Rp 0</td>
                        </tr>
                        <tr>
                            <td>11</td>
                            <td>Entertaint</td>
                            <td>Rp 0</td>
                        </tr>
                        <tr style="background-color: #337ab775;">
                            <td width="4%"></td>
                            <td><b>TOTAL PENGELUARAN</b></td>
                            <td>Rp 0</td>
                        </tr>
                        <tr>
                            <td><b>NPL</b></td>
                            <td><b>Profit / Loss (A - B - C)</b></td>
                            <td>Rp 0</td>
                        </tr>
                        <tr style="background-color: #337ab775;">
                            <td width="4%"></td>
                            <td><center><b>GRAND TOTAL</b></center></td>
                            <td>Rp 0</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
       </div> <!-- /.box-body -->
       <div class="box-footer clearfix">
            <div class="col-sm-6">
              
            </div>
            <div class="col-sm-6">
              <div class="pull-right">
              <?php echo $pagination; ?>
              </div>
            </div>
        </div> 
      </div><!-- /.box -->
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-md-offset-3">
      <p></p>
      <p class="text-center icon-page"><i class="fa fa-users fa-5x"></i></p>
      <br>
      <div class="callout callout-info">
        <h4><i class="fa fa-bullhorn fa-fw"></i> Learn More</h4>
        <p>Learn more about pengeluaran <a href="">here</a>. Or <a href="">contact us</a> for more information</p>
      </div>
    </div>
  </div>
</section><!-- /.content