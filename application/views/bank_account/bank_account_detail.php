<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Bank Account
    <small>Detail</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?php echo $current_context; ?>">Bank Account</a></li>
    <li class="active">Detail</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- right column -->
    <div class="col-md-12">
      <!-- general form elements disabled -->
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Bank Account</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            
			<div class="form-group">
					<label>Bank Name:</label>
					<p><?php echo $bank_account->bank_name; ?></p>
			</div>
			<div class="form-group">
					<label>Account Name:</label>
					<p><?php echo $bank_account->account_name; ?></p>
			</div>
			<div class="form-group">
					<label>Account No:</label>
					<p><?php echo $bank_account->account_no; ?></p>
			</div>
			<div class="form-group">
					<label>Branch:</label>
					<p><?php echo $bank_account->account_branch; ?></p>
			</div>
			<div class="form-group">
					<label>Status:</label>
					<p><?php 
                      $status = $bank_account->account_status;
                        if($status == 1){
                          ?>
                            <span class="label label-success">Active</span>
                        <?php }
                        if($status == 0){
                          ?>
                            <span class="label label-danger">Not Active</span>
                        <?php }
                        ?></p>
			</div>
            <div class="box-footer">
               <a href="<?php echo $current_context; ?>" class="btn btn-default">Back</a>
               <a href="<?php echo $current_context .'edit' ; ?>/<?php echo $this->uri->segment('4') ?>" class="btn btn-primary pull-right">Edit</a>
            </div><!-- /.box-footer -->
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!--/.col (right) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->