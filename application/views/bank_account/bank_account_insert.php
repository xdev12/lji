<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Bank Account
    <small>Insert</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?php echo $current_context; ?>">Bank Account</a></li>
    <li class="active">Insert</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <?php
        $message = $this->session->flashdata('message');
        $type_message = $this->session->flashdata('type_message');
        echo (!empty($message) && $type_message=="success") ? ' <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-check"></i> Great!</h4>'.$message.'</div></div>': '';
        echo (!empty($message) && $type_message=="error") ? '   <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-exclamation-triangle"></i> Uh-Oh!</h4>'.$message.'</div></div>': '';
    ?>
    <!-- right column -->
    <div class="col-md-12">
      <!-- general form elements disabled -->
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Bank Account</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <form role="form" method="POST" enctype="multipart/form-data">
            
			<div class="form-group <?php echo (form_error('bank_id') != "") ? "has-error" : "" ?>">
					<label>Bank Name</label><select class="form-control select2me" name="bank_id" data-placeholder="Pilih..." >
					<option>--Select--</option>
					<?php
					foreach($bank_list as $bank){
						if($bank->bank_id == set_value('bank_id',$bank_account->bank_id)){
							echo "<option value='$bank->bank_id' selected>$bank->bank_name</option>";
						} else {
							echo "<option value='$bank->bank_id'>$bank->bank_name</option>";
						}
					}
					?>
				</select>
				<?php echo form_error('bank_id'); ?>
			</div>
			<div class="form-group <?php echo (form_error('account_name') != "") ? "has-error" : "" ?>">
					<label>Account Name</label><input class="form-control " name="account_name" value="<?php echo set_value('account_name', $bank_account->account_name); ?>" placeholder="Account Name"  required  maxlength=100>
				<?php echo form_error('account_name'); ?>
			</div>
			<div class="form-group <?php echo (form_error('account_no') != "") ? "has-error" : "" ?>">
					<label>Account No</label><input class="form-control " name="account_no" value="<?php echo set_value('account_no', $bank_account->account_no); ?>" placeholder="Account No"  required  maxlength=50>
				<?php echo form_error('account_no'); ?>
			</div>
			<div class="form-group <?php echo (form_error('account_branch') != "") ? "has-error" : "" ?>">
					<label>Branch</label><input class="form-control " name="account_branch" value="<?php echo set_value('account_branch', $bank_account->account_branch); ?>" placeholder="Branch"  required  maxlength=50>
				<?php echo form_error('account_branch'); ?>
			</div>
      <div class="form-group <?php echo (form_error('account_status') != "") ? "has-error" : "" ?>">
          <div class="radio">
            <b>Status</b>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="account_status" value="1" <?php echo ($bank_account->account_status !== '0')?'checked':'';?>>Active
            </label>
          </div>
          <div class="radio">
            <label>
              <input type="radio" name="account_status" value="0" <?php echo ($bank_account->account_status === '0')?'checked':'';?>>Not Active
            </label>
          </div>
          <?php echo form_error('account_status'); ?>
      </div>
            <div class="box-footer">
               <a href="<?php echo $current_context; ?>" class="btn btn-default">Cancel</a>
               <button type="submit" class="btn btn-primary pull-right">Save</button>
            </div><!-- /.box-footer -->
          </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!--/.col (right) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->