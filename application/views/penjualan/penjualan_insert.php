<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Penjualan
    <small><?php echo ($edit)?'Edit':'Insert'; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?php echo $current_context; ?>">Penjualan</a></li>
    <li class="active"><?php echo ($edit)?'Edit':'Insert'; ?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <?php
        $message = $this->session->flashdata('message');
        $type_message = $this->session->flashdata('type_message');
        echo (!empty($message) && $type_message=="success") ? ' <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-check"></i> Great!</h4>'.$message.'</div></div>': '';
        echo (!empty($message) && $type_message=="error") ? '   <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-exclamation-triangle"></i> Uh-Oh!</h4>'.$message.'</div></div>': '';
    ?>
    <!-- right column -->
    <div class="col-md-12">
      <!-- general form elements disabled -->
      <div class="box">
        <div class="box-header with-border text-right">
          <h3 class="box-title">Add</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <form role="form" method="POST" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-2">
                <dl class="text-right">
                  <dt><h5>Penjualan</h5></dt>
                  <dd>Penjualan</dd>
                </dl>
              </div>
              <div class="col-md-8">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('penjualan_jenis') != "") ? "has-error" : "" ?>">
                        <label>Kategori Penjualan <req>*</req></label><select class="form-control select2" name="penjualan_jenis" data-placeholder="Select..." data-rule-required="true" required>
                        <option></option>
                        <?php
                        foreach($jenis_penjualan_list as $jp){
                          if($jp->jpj_id == set_value('jpj_id',$penjualan->penjualan_jenis)){
                            echo "<option value='$jp->jpj_id' selected>$jp->jpj_nama</option>";
                          } else {
                            echo "<option value='$jp->jpj_id'>$jp->jpj_nama</option>";
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('penjualan_tgl_invoice') != "") ? "has-error" : "" ?>">
                        <label>Tgl. invoice <req>*</req></label>
                        <input class="form-control" name="penjualan_tgl_invoice" value="<?php echo ($edit)?$penjualan->penjualan_tgl_invoice:date('Y-m-d'); ?>" placeholder="Tgl. invoice" required>
                        <p class="help-block"><?php echo form_error('penjualan_tgl_invoice'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('penjualan_no_invoice') != "") ? "has-error" : "" ?>">
                        <label>No. invoice <req>*</req></label>
                        <input class="form-control" name="penjualan_no_invoice" value="<?php echo set_value('penjualan_no_invoice', $penjualan->penjualan_no_invoice); ?>" placeholder="No. invoice" required>
                        <p class="help-block"><?php echo form_error('penjualan_no_invoice'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('penjualan_nama_perusahaan') != "") ? "has-error" : "" ?>">
                        <label>Nama Perusahaan <req>*</req></label>
                        <input class="form-control" name="penjualan_nama_perusahaan" value="<?php echo set_value('penjualan_nama_perusahaan', $penjualan->penjualan_nama_perusahaan); ?>" placeholder="Nama Perusahaan" required>
                        <p class="help-block"><?php echo form_error('penjualan_nama_perusahaan'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('penjualan_uraian_barang') != "") ? "has-error" : "" ?>">
                        <label>Uraian Barang <req>*</req></label>
                        <textarea class="form-control" name="penjualan_uraian_barang" placeholder="Uraian Barang" required><?php echo set_value('penjualan_uraian_barang', $penjualan->penjualan_uraian_barang); ?></textarea>
                        <p class="help-block"><?php echo form_error('penjualan_uraian_barang'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('penjualan_nama_tug_boat') != "") ? "has-error" : "" ?>">
                        <label>Tug Boat <req>*</req></label>
                        <input class="form-control" name="penjualan_nama_tug_boat" value="<?php echo set_value('penjualan_nama_tug_boat', $penjualan->penjualan_nama_tug_boat); ?>" placeholder="Tug Boat" required>
                        <p class="help-block"><?php echo form_error('penjualan_nama_tug_boat'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('penjualan_nama_tongkang') != "") ? "has-error" : "" ?>">
                        <label>Nama Tongkang <req>*</req></label>
                        <input class="form-control" name="penjualan_nama_tongkang" value="<?php echo set_value('penjualan_nama_tongkang', $penjualan->penjualan_nama_tongkang); ?>" placeholder="Nama Tongkang" required>
                        <p class="help-block"><?php echo form_error('penjualan_nama_tongkang'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('penjualan_tonase') != "") ? "has-error" : "" ?>">
                        <label>Tonase <req>*</req></label>
                        <input class="form-control" name="penjualan_tonase" value="<?php echo set_value('penjualan_tonase', $penjualan->penjualan_tonase); ?>" placeholder="Tonase" required>
                        <p class="help-block"><?php echo form_error('penjualan_tonase'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('penjualan_harga_satuan') != "") ? "has-error" : "" ?>">
                        <label>Harga Satuan</label>
                        <input class="form-control" type="number" name="penjualan_harga_satuan" placeholder="Harga Satuan" value="<?php echo set_value('penjualan_harga_satuan', $penjualan->penjualan_harga_satuan); ?>" required>
                        <p class="help-block"><?php echo form_error('penjualan_harga_satuan'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('penjualan_jumlah_total') != "") ? "has-error" : "" ?>">
                        <label>Jumlah </label>
                        <input class="form-control" type="number" name="penjualan_jumlah_total" placeholder="Jumlah" value="<?php echo set_value('penjualan_jumlah_total', $penjualan->penjualan_jumlah_total); ?>" required>
                        <p class="help-block"><?php echo form_error('penjualan_jumlah_total'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('penjualan_foto_invoice') != "") ? "has-error" : "" ?>">
                        <label>Upload Foto Invoice <req>*</req></label>
                        <input class="form-control" type="file" name="penjualan_foto_invoice" value="<?php echo set_value('penjualan_foto_invoice', $penjualan->penjualan_foto_invoice); ?>" placeholder="Upload Foto Invoice" required>
                        <p class="help-block"><?php echo form_error('penjualan_foto_invoice'); ?></p>
                        <?php if(!empty($penjualan->penjualan_foto_invoice)){?>
                            <img src="<?= base_url(). $penjualan->penjualan_foto_invoice?>" width="300">
                        <?php }?>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('penjualan_bukti_bayar') != "") ? "has-error" : "" ?>">
                        <label>Upload Bukti Bayar <req>*</req></label>
                        <input class="form-control" type="file" name="penjualan_bukti_bayar" value="<?php echo set_value('penjualan_bukti_bayar', $penjualan->penjualan_bukti_bayar); ?>" placeholder="Upload Bukti Bayar" required>
                        <p class="help-block"><?php echo form_error('penjualan_bukti_bayar'); ?></p>
                        <?php if(!empty($penjualan->penjualan_bukti_bayar)){?>
                            <img src="<?= base_url(). $penjualan->penjualan_bukti_bayar?>" width="300">
                        <?php }?>
                    </div>
                  </div>

                </div>


              </div>
              <div class="col-md-12">
                <hr>
              </div>
            </div>
      
            <div class="box-footer">
              <div class="clearfix pull-right">
                <a href="<?php echo $current_context; ?>" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </div><!-- /.box-footer -->
          </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!--/.col (right) -->
  </div>   <!-- /.row -->
  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-md-offset-3">
      <p></p>
      <p class="text-center icon-page"><i class="fa fa-home fa-5x"></i></p>
      <br>
      <div class="callout callout-info">
        <h4><i class="fa fa-bullhorn fa-fw"></i> Learn More</h4>
        <p>Learn More about user <a href="">here</a>. Or <a href="">contact us</a> for more information</p>
      </div>
    </div>
  </div>
</section><!-- /.content -->