<!-- Content Header (Page header) -->
<?php $uri=$this->uri->segment(2) ?>
<section class="content-header">
  <h1>
    <?php echo $page_title; ?>
    <small>List</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active"><?php echo $page_title; ?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

  <div class="row">
    <?php
        $message = $this->session->flashdata('message');
        $type_message = $this->session->flashdata('type_message');
        echo (!empty($message) && $type_message=="success") ? ' <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-check"></i> Great!</h4>'.$message.'</div></div>': '';
        echo (!empty($message) && $type_message=="error") ? '   <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-exclamation-triangle"></i> Uh-Oh!</h4>'.$message.'</div></div>': '';
    ?>
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data <?php echo $page_title; ?> <span class="badge"><?php echo ($uri == 'reseller' && $info[0]->reseller_activation == 'e' || $uri=='member')?$total_rows:0; ?> Data</span></h3>
          <div class="box-tools pull-right">
          <?php if ($uri == 'reseller' && $info[0]->reseller_activation == 'e') {
            ?>
            <a href="<?php echo $current_context . 'add/'; ?>" class="btn btn-xs bg-light-blue">
                <i class="fa fa-plus"></i>&nbsp; Add
            </a>
          <?php 
          } ?>
            <a href="javascript:;" class="btn btn-xs bg-orange"><i class="fa fa-refresh"></i> Refresh</a>
            <button class="btn btn-box-tool btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div><!-- /.box-header -->
        <?php if (($uri == 'reseller' && $info[0]->reseller_activation == 'e') || $uri == 'member') {?>
        <div class="box-body">
            <div class="row">
              <div class="col-md-2">
                <dl class="text-right">
                  <dt><h5>Filter Data</h5></dt>
                  <dd>type/ select to find or filter data</dd>
                </dl>
              </div>

              <div class="col-md-10">
                <form method="post" action="<?php echo $current_context . 'search'; ?>">
                    <?php //print_r( $current_context. 'search');die(); ?>
                  <?php
                  if ($uri == 'member') {
                    $key = (object) $this->session->userdata('filter_member');
                  } elseif ($uri == 'reseller') {
                    $key = (object) $this->session->userdata('filter');
                  }
                    
                         ?>
                  <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="control-label">Email</label>
                          <input class="form-control " name="member_email" value="<?php echo $key->member_email; ?>" placeholder="Email">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="control-label">Full Name</label>
                          <input class="form-control " name="member_fullname" value="<?php echo $key->member_fullname; ?>" placeholder="Full Name">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <input type="hidden" id="search" name="search" value="true">
                        <div class="clearfix">
                            <button type="submit" class="btn btn-sm btn-primary">Search</button>
                            <button type="button" class="btn btn-sm btn-default" onclick="location.href='<?php echo $current_context; ?>'">Clear</button>
                        </div>
                      </div>
                  </div>
                </form>
              </div>

            </div>
        </div><!-- /.box-body -->
        <?php } ?>
        <div class="box-body">
          <div class="row">
            <div class="col-md-2">
              <dl class="text-right">

                <dt><h5>Data</h5></dt> 
                <dd>data table, all data restore here</dd>

              </dl>
              <div class="clearfix text-right">
              <?php if ($uri == 'reseller' && $info[0]->reseller_activation == 'e') {
               ?>
                <p><a href="<?php echo $current_context . 'add/'; ?>" class="btn btn-sm bg-light-blue btn-block">
                    <i class="fa fa-plus"></i>&nbsp; Add Reseller
                </a></p>
              
                <p><button id="deleteall" class="btn bg-red btn-block btn-sm" data-href="<?php echo $current_context . 'delete_multiple'; ?>" data-toggle="modal" data-target="#deleteAll"><i class="fa fa-trash-o"></i> Delete Selected</button></p>
                <?php } ?>
              </div>
            </div>
            <div class="col-md-10">
              <div class="table-responsive">
                  <table class="table table-bordered table-hover table-striped" id="table_data">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Identity</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if($uri == 'member' || $info[0]->reseller_activation == 'e'){
                      if(!empty($member)) { 
                      $i = $offset + 1;
                      foreach ($member as $row) { 
                          ?>
                          <tr>
                              <td>
                                <div class="checkbox">
                                  <label>
                                    <?php if ($uri == 'reseller'): ?>
                                      <input type="checkbox" class="checkboxes"  data-member_id="<?php echo $row->member_id; ?>"  />  
                                    <?php endif ?>
                                    <?php echo $i;?>
                                 </label>
                                </div>
                              </td>
                              <td class="no-padi-p">
                                <p><strong><?php echo $row->member_fullname; ?></strong></p>
                                <p><i class="fa fa-envelope-o fa-fw" aria-hidden="true"></i> <?php echo (!empty($row->member_email)?$row->member_email:'<i>(empty)</i>'); ?></p>
                                <p>
                                  <i class="fa fa-phone fa-fw" aria-hidden="true"></i> 
                                    <?php echo $row->member_phone; ?>
                                </p>
                                <p>
                                 </span>
                                </p>
                              </td>
                              <td>
                              <?php if($row->member_status == '1'){?>
                                          <span class="label label-success"><?php echo "Active";?></span>
                                      <?php
                                        }elseif($row->member_status == '0'){?>
                                          <span class="label label-danger"><?php echo "Not Active";?></span>
                                        <?php
                                        }else { ?>
                                          <span class="label label-danger"><?php echo "Banned";?></span>
                                        <?php } ?>


                              </td>
                              <td class="td-btn">
                                <a href="<?php echo $current_context . 'detail'  .'/'. $row->member_id . '/' . @$status . (!empty($status)?'/':'') . @$page ?>" class="badge bg-green"><i class="fa fa-eye fa-fw"></i>View</a>
                                <a href="<?php echo $current_context . 'banned'  .'/'. $row->member_id . '/' . @$status . (!empty($status)?'/':'') . @$page ?>" class="badge bg-blue"><i class="fa fa-minus-circle fa-fw"></i>Banned</a>
                                <!-- <a href="<?php echo $current_context . 'delete'  .'/'. $row->member_id . '/' . @$status . (!empty($status)?'/':'') . @$page ?>" class="badge bg-red"><i class="fa fa-trash fa-fw"></i>Delete</a> -->
                              <?php if ($uri == 'reseller') { 
                                $status = $this->uri->segment(3);
                                $page = $this->uri->segment(4);
                              ?>
                                <p><a href="#" data-href="<?php echo $current_context . 'delete'  .'/'. $row->member_id ?>" data-toggle="modal" data-target="#deleteModal"  class="badge bg-red"><i class="fa fa-trash-o fa-fw"></i> delete</a></p>
                              </td>
                              <?php } ?>
                          </tr>
                      <?php 
                      $i++; 
                        }

                      } else { ?>
                        <tr>

                        <td colspan="6" class="empty-table">
                          <br>
                            <p class="text-center"><i class="fa fa-users fa-3x"></i></p>
                          <?php if($this->uri->segment(3) == 'search') { ?>
                            <p class="text-center"><i>We couldn't find the data you're looking for.</p></i>
                          <?php } else if ($uri == 'reseller' && $info[0]->reseller_activation == 'e') { ?>
                            <p class="text-center"><i><a href="<?php echo $current_context . 'add/'; ?>">Add</a> your reseller member now.</p></i>
                          <?php } else { ?>
                            <p class="text-center"><i>Alibaba start small.</p></i>
                          <?php } ?>
                          <br>
                        </td>
                      </tr>
                      <?php
                      }
                       } else { ?>
                        <tr>

                        <td colspan="6" class="empty-table">
                          <br>
                            <p class="text-center"><i class="fa fa-users fa-3x"></i></p>
                            <p class="text-center"><i>Reseller feature is disabled. Please go to <a href="<?php echo base_url(). 'admin/info/'; ?>">Info Page</a> to set your reseller feature on.</p></i>
                          <br>
                        </td>
                      </tr>
                       <?php } ?>

                    </tbody>
                    <tfoot>
                      <tr>
                        <th class="table-checkbox">
                        
                        </th>
                        <th>Identity</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </tfoot>
                  </table>

              </div> 
            </div> 
          </div>
        </div>  <!-- /.box-body -->
        <div class="box-footer clearfix">
            <div class="col-sm-6">
              
            </div>
            <div class="col-sm-6">
              <div class="pull-right">
              <?php echo $pagination; ?> 
              </div>
            </div>
        </div>
      </div><!-- /.box -->
    </div>
  </div>

  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-md-offset-3">
      <p></p>
      <p class="text-center icon-page"><i class="fa fa-users fa-5x"></i></p>
      <br>
      <div class="callout callout-info">
        <h4><i class="fa fa-bullhorn fa-fw"></i> Learn More</h4>
        <p>Learn more about member <a href="">here</a>. Or <a href="">contact us</a> for more information</p>
      </div>
    </div>
  </div>
</section>
