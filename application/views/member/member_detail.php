 <!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Member
    <small>Detail</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?php echo $current_context; ?>">Member</a></li>
    <li class="active">Detail</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- right column -->
    <div class="col-md-12">
      <!-- general form elements disabled -->
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Member</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
        	<center>
	            
					<p><img class="img-responsive img-rounded" src="<?php echo base_url() ?>assets/images/member.png" width="200" height="200">
					</p>
				
				<div class="form-group">
						<h1><p><?php echo $member->member_fullname; ?></p></h1>
				</div>
				<div class="form-group">
						<p><i class="fa fa-envelope-o" aria-hidden="true"></i>
						<?php echo $member->member_email; ?></p>
				</div>
				<div class="form-group">
						<p><i class="fa fa-home" aria-hidden="true"></i>
						<?php echo $member->member_address; ?></p>
				</div>
				<div class="form-group">
					<p>
						
	                   
	                </p>
				</div>
				<div class="form-group">
						<?php if($member->member_status == '1'){?>
	                                <span class="label label-primary"><?php echo "ACTIVE MEMBER";?></span>
	                    <?php
		                  		}elseif($member->member_status == ''){?>
		                    		<span class="label label-danger"><?php echo "NOT ACTIVE MEMBER";?></span>
		                <?php
		                } ?>
				</div>
			</center>
            <div class="box-footer">
               <a href="<?php echo $current_context; ?>" class="btn btn-default">Back</a>
               
            </div><!-- /.box-footer -->
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!--/.col (right) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->