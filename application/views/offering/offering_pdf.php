<div style="margin: 40px;text-align: justify;">
<div align="center"><h3><u>FULL CORPORATE OFFER</u></h3></div>
<br><br><br>

<table>
	<tr>
		<td>Nomor</td>
		<td>: 005/FCO-BB/IV/18</td>
	</tr>
	<tr>
		<td>Kepada</td>
		<td>: PT. ABC</td>
	</tr>
	<tr>
		<td>Attn</td>
		<td>: ABCDEF <br> Di tempat</td>
	</tr>
	<tr>
		<td>Perihal</td>
		<td>: Penawaran Batubara</td>
	</tr>
</table>
<br><br><br>	

Dengan hormat,<br>

Bersama ini kami mengajukan penawaran harga batubara dengan sepesifikasi sebagai berikut :<br>

<table>
	<tr>
		<td>-	Calarie Value (GAR)</td>
		<td>: 20 Kcal/kg</td>
	</tr>
	<tr>
		<td>-	Ash Content (Adb)</td>
		<td>: 20 %</td>
	</tr>
	<tr>
		<td>-	Sulfur (Adb)</td>
		<td>: 20 %</td>
	</tr>
	<tr>
		<td>-	Size</td>
		<td>: 20</td>
	</tr>
	<tr>
		<td>-	Quantity</td>
		<td>: 100 MT</td>
	</tr>
	<tr>
		<td>-	Delivery</td>
		<td>: ABCD</td>
	</tr>
	<tr>
		<td>-	Harga</td>
		<td>: Rp. 20.000.000,-/MT</td>
	</tr>
	<tr>
		<td>-	Term of Payment</td>
		<td>: 30 % - 2 Minggu (Dari Tanggal Invoice)<br>70 % - 4 Minggu (Dari Tanggal Invoice)</td>
	</tr>
	<tr>
		<td>-	Delivery</td>
		<td>ABCD</td>
	</tr>
</table>
<br><br><br>


Kami menunggu kesempatan yang diberikan, atas waktu serta perhatiannya kami ucapkan terima kasih<br><br><br>



Bandung, 01 Mei 2018<br>
PT. LUMBUNG JAYA INTERNASIONAL<br><br><br><br>

  
<ul>Bambang Heryanto</ul><br>
Direktur<br>
</div>

