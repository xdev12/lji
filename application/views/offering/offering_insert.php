<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Offering
    <small><?php echo ($edit)?'Edit':'Insert'; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?php echo $current_context; ?>">Offering</a></li>
    <li class="active"><?php echo ($edit)?'Edit':'Insert'; ?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <?php
        $message = $this->session->flashdata('message');
        $type_message = $this->session->flashdata('type_message');
        echo (!empty($message) && $type_message=="success") ? ' <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-check"></i> Great!</h4>'.$message.'</div></div>': '';
        echo (!empty($message) && $type_message=="error") ? '   <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-exclamation-triangle"></i> Uh-Oh!</h4>'.$message.'</div></div>': '';
    ?>
    <!-- right column -->
    <div class="col-md-12">
      <!-- general form elements disabled -->
      <div class="box">
        <div class="box-header with-border text-right">
          <h3 class="box-title">Add</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <form role="form" method="POST" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-2">
                <dl class="text-right">
                  <dt><h5>Offering</h5></dt>
                  <dd>Offering</dd>
                </dl>
              </div>
              <div class="col-md-8">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('offer_no') != "") ? "has-error" : "" ?>">
                        <label>No. Offering <req>*</req></label>
                        <input class="form-control" name="offer_no" value="<?php echo set_value('offer_no', $offering->offer_no); ?>" placeholder="No. Shipping">
                        <p class="help-block"><?php echo form_error('offer_no'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('offer_to') != "") ? "has-error" : "" ?>">
                        <label>To <req>*</req></label>
                        <input class="form-control" name="offer_to" value="<?php echo set_value('offer_to', $offering->offer_to); ?>" placeholder="To">
                        <p class="help-block"><?php echo form_error('offer_to'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('offer_attn') != "") ? "has-error" : "" ?>">
                        <label>Attn </label>
                        <textarea class="form-control" name="offer_attn" placeholder="Attn">
                            <?php echo set_value('offer_attn', $offering->offer_attn); ?>
                        </textarea>
                        <p class="help-block"><?php echo form_error('offer_attn'); ?></p>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('offer_calarie_value') != "") ? "has-error" : "" ?>">
                        <label>Calarie Value (GAR) <req>*</req></label>
                        <input class="form-control" type="number" name="offer_calarie_value" value="<?php echo set_value('offer_calarie_value', $offering->offer_calarie_value); ?>" placeholder="Calarie Value (GAR)">
                        <p class="help-block"><?php echo form_error('offer_calarie_value'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('offer_ash_content') != "") ? "has-error" : "" ?>">
                        <label>Ash Content (Adb) <req>*</req></label>
                        <input class="form-control"  type="number" name="offer_ash_content" value="<?php echo set_value('offer_ash_content', $offering->offer_ash_content); ?>" placeholder="Ash Content (Adb)">
                        <p class="help-block"><?php echo form_error('offer_ash_content'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('offer_sulfur') != "") ? "has-error" : "" ?>">
                        <label>Sulfur (Adb) <req>*</req></label>
                        <input class="form-control"  type="number" name="offer_sulfur" value="<?php echo set_value('offer_sulfur', $offering->offer_sulfur); ?>" placeholder="Sulfur (Adb)">
                        <p class="help-block"><?php echo form_error('offer_sulfur'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('offer_size') != "") ? "has-error" : "" ?>">
                        <label>Size <req>*</req></label>
                        <input class="form-control" name="offer_size" value="<?php echo set_value('offer_size', $offering->offer_size); ?>" placeholder="Size">
                        <p class="help-block"><?php echo form_error('offer_size'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('offer_qty') != "") ? "has-error" : "" ?>">
                        <label>Quantity <req>*</req></label>
                        <input class="form-control" name="offer_qty" value="<?php echo set_value('offer_qty', $offering->offer_qty); ?>" placeholder="Quantity">
                        <p class="help-block"><?php echo form_error('offer_qty'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('offer_delivery') != "") ? "has-error" : "" ?>">
                        <label>Delivery </label>
                        <input class="form-control" name="offer_delivery" value="<?php echo set_value('offer_delivery', $offering->offer_delivery); ?>" placeholder="Date Of Loading">
                        <p class="help-block"><?php echo form_error('offer_delivery'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('offer_price') != "") ? "has-error" : "" ?>">
                        <label>Price <req>*</req></label>
                        <input class="form-control" type="number" name="offer_price" value="<?php echo set_value('offer_price', $offering->offer_price); ?>" placeholder="Price">
                        <p class="help-block"><?php echo form_error('offer_price'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('offer_top1') != "") ? "has-error" : "" ?>">
                        <label>Term of Payment 1 </label>
                        <input class="form-control" type="number" name="offer_top1" value="<?php echo set_value('offer_top1', $offering->offer_top1); ?>" placeholder="Term of Payment 1">
                        <p class="help-block"><?php echo form_error('offer_top1'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('offer_top2') != "") ? "has-error" : "" ?>">
                        <label>Term of Payment 2 <req>*</req></label>
                        <input class="form-control" name="offer_top2" value="<?php echo set_value('offer_top2', $offering->offer_top2); ?>" placeholder="Term of Payment 2">
                        <p class="help-block"><?php echo form_error('offer_top2'); ?></p>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('offer_date') != "") ? "has-error" : "" ?>">
                        <label>Date <req>*</req></label>
                        <input class="form-control" name="offer_date" value="<?php echo set_value('offer_date', $offering->offer_date); ?>" placeholder="Date">
                        <p class="help-block"><?php echo form_error('offer_date'); ?></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <hr>
              </div>
            </div>
      
            <div class="box-footer">
              <div class="clearfix pull-right">
                <a href="<?php echo $current_context; ?>" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </div><!-- /.box-footer -->
          </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!--/.col (right) -->
  </div>   <!-- /.row -->
  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-md-offset-3">
      <p></p>
      <p class="text-center icon-page"><i class="fa fa-home fa-5x"></i></p>
      <br>
      <div class="callout callout-info">
        <h4><i class="fa fa-bullhorn fa-fw"></i> Learn More</h4>
        <p>Learn More about user <a href="">here</a>. Or <a href="">contact us</a> for more information</p>
      </div>
    </div>
  </div>
</section><!-- /.content -->