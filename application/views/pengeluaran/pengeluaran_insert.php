<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    pengeluaran
    <small><?php echo ($edit)?'Edit':'Insert'; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?php echo $current_context; ?>">pengeluaran</a></li>
    <li class="active"><?php echo ($edit)?'Edit':'Insert'; ?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <?php
        $message = $this->session->flashdata('message');
        $type_message = $this->session->flashdata('type_message');
        echo (!empty($message) && $type_message=="success") ? ' <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-check"></i> Great!</h4>'.$message.'</div></div>': '';
        echo (!empty($message) && $type_message=="error") ? '   <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-exclamation-triangle"></i> Uh-Oh!</h4>'.$message.'</div></div>': '';
    ?>
    <!-- right column -->
    <div class="col-md-12">
      <!-- general form elements disabled -->
      <div class="box">
        <div class="box-header with-border text-right">
          <h3 class="box-title">Add</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <form role="form" method="POST" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-2">
                <dl class="text-right">
                  <dt><h5>pengeluaran</h5></dt>
                  <dd>pengeluaran</dd>
                </dl>
              </div>
              <div class="col-md-8">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pengeluaran_jenis') != "") ? "has-error" : "" ?>">
                        <label>Kategori pengeluaran <req>*</req></label><select class="form-control select2" name="pengeluaran_jenis" data-placeholder="Select..." data-rule-required="true" required>
                        <option></option>
                        <?php
                        foreach($jenis_pengeluaran_list as $jp){
                          if($jp->jp_id == set_value('jp_id',$pengeluaran->pengeluaran_jenis)){
                            echo "<option value='$jp->jp_id' selected>$jp->jp_nama</option>";
                          } else {
                            echo "<option value='$jp->jp_id'>$jp->jp_nama</option>";
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pengeluaran_tgl') != "") ? "has-error" : "" ?>">
                        <label>Tgl. Pengeluaran <req>*</req></label>
                        <input class="form-control" name="pengeluaran_tgl" value="<?php echo ($edit)?$pengeluaran->pengeluaran_tgl:date('Y-m-d'); ?>" placeholder="Tgl. Pengeluaran" required>
                        <p class="help-block"><?php echo form_error('pengeluaran_tgl'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pengeluaran_ket') != "") ? "has-error" : "" ?>">
                        <label>Keterangan Pengeluaran <req>*</req></label>
                        <textarea class="form-control" name="pengeluaran_ket" placeholder="Keterangan Pengeluaran" required><?php echo set_value('pengeluaran_ket', $pengeluaran->pengeluaran_ket); ?></textarea>
                        <p class="help-block"><?php echo form_error('pengeluaran_ket'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pengeluaran_total') != "") ? "has-error" : "" ?>">
                        <label>Total Pengeluaran </label>
                        <input class="form-control" type="number" name="pengeluaran_total" placeholder="Total Pengeluaran" value="<?php echo set_value('pengeluaran_total', $pengeluaran->pengeluaran_total); ?>" required>
                        <p class="help-block"><?php echo form_error('pengeluaran_total'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('pengeluaran_gambar') != "") ? "has-error" : "" ?>">
                        <label>Upload Bukti Pengeluaran <req>*</req></label>
                        <input class="form-control" type="file" name="pengeluaran_gambar" value="<?php echo set_value('pengeluaran_gambar', $pengeluaran->pengeluaran_gambar); ?>" placeholder="Upload Bukti Pengeluaran" required>
                        <p class="help-block"><?php echo form_error('pengeluaran_gambar'); ?></p>
                        <?php if(!empty($pengeluaran->pengeluaran_gambar)){?>
                            <img src="<?= base_url(). $pengeluaran->pengeluaran_gambar?>" width="300">
                        <?php }?>
                    </div>
                  </div>

                </div>


              </div>
              <div class="col-md-12">
                <hr>
              </div>
            </div>
      
            <div class="box-footer">
              <div class="clearfix pull-right">
                <a href="<?php echo $current_context; ?>" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </div><!-- /.box-footer -->
          </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!--/.col (right) -->
  </div>   <!-- /.row -->
  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-md-offset-3">
      <p></p>
      <p class="text-center icon-page"><i class="fa fa-home fa-5x"></i></p>
      <br>
      <div class="callout callout-info">
        <h4><i class="fa fa-bullhorn fa-fw"></i> Learn More</h4>
        <p>Learn More about user <a href="">here</a>. Or <a href="">contact us</a> for more information</p>
      </div>
    </div>
  </div>
</section><!-- /.content -->