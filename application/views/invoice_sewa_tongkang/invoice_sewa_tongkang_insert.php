<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Invoice Sewa Tongkang
    <small><?php echo ($edit)?'Edit':'Insert'; ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?php echo $current_context; ?>">Invoice Sewa Tongkang</a></li>
    <li class="active"><?php echo ($edit)?'Edit':'Insert'; ?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <?php
        $message = $this->session->flashdata('message');
        $type_message = $this->session->flashdata('type_message');
        echo (!empty($message) && $type_message=="success") ? ' <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-check"></i> Great!</h4>'.$message.'</div></div>': '';
        echo (!empty($message) && $type_message=="error") ? '   <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-exclamation-triangle"></i> Uh-Oh!</h4>'.$message.'</div></div>': '';
    ?>
    <!-- right column -->
    <div class="col-md-12">
      <!-- general form elements disabled -->
      <div class="box">
        <div class="box-header with-border text-right">
          <h3 class="box-title">Add</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <form role="form" method="POST" enctype="multipart/form-data">
            <div class="row">
              <div class="col-md-2">
                <dl class="text-right">
                  <dt><h5>Invoice Sewa Tongkang</h5></dt>
                  <dd>invoice sewa tongkang</dd>
                </dl>
              </div>
              <div class="col-md-8">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('inst_tgl_invoice') != "") ? "has-error" : "" ?>">
                        <label>Tgl. invoice <req>*</req></label>
                        <input class="form-control" name="inst_tgl_invoice" value="<?php echo ($edit)?$invoice_sewa_tongkang->inst_tgl_invoice:date('Y-m-d'); ?>" placeholder="Tgl. invoice" required>
                        <p class="help-block"><?php echo form_error('inst_tgl_invoice'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('inst_no_invoice') != "") ? "has-error" : "" ?>">
                        <label>No. invoice <req>*</req></label>
                        <input class="form-control" name="inst_no_invoice" value="<?php echo set_value('inst_no_invoice', $invoice_sewa_tongkang->inst_no_invoice); ?>" placeholder="No. invoice" required>
                        <p class="help-block"><?php echo form_error('inst_no_invoice'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('inst_no_spal') != "") ? "has-error" : "" ?>">
                        <label>No. SPAL <req>*</req></label>
                        <input class="form-control" name="inst_no_spal" value="<?php echo set_value('inst_no_spal', $invoice_sewa_tongkang->inst_no_spal); ?>" placeholder="No. SPAL" required>
                        <p class="help-block"><?php echo form_error('inst_no_spal'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('inst_nama_perusahaan') != "") ? "has-error" : "" ?>">
                        <label>Nama Perusahaan <req>*</req></label>
                        <input class="form-control" name="inst_nama_perusahaan" value="<?php echo set_value('inst_nama_perusahaan', $invoice_sewa_tongkang->inst_nama_perusahaan); ?>" placeholder="Nama Perusahaan" required>
                        <p class="help-block"><?php echo form_error('inst_nama_perusahaan'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('inst_nama_tug_boat') != "") ? "has-error" : "" ?>">
                        <label>Tug Boat <req>*</req></label>
                        <input class="form-control" name="inst_nama_tug_boat" value="<?php echo set_value('inst_nama_tug_boat', $invoice_sewa_tongkang->inst_nama_tug_boat); ?>" placeholder="Tug Boat" required>
                        <p class="help-block"><?php echo form_error('inst_nama_tug_boat'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('inst_nama_tongkang') != "") ? "has-error" : "" ?>">
                        <label>Nama Tongkang <req>*</req></label>
                        <input class="form-control" name="inst_nama_tongkang" value="<?php echo set_value('inst_nama_tongkang', $invoice_sewa_tongkang->inst_nama_tongkang); ?>" placeholder="Nama Tongkang" required>
                        <p class="help-block"><?php echo form_error('inst_nama_tongkang'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('inst_tonase') != "") ? "has-error" : "" ?>">
                        <label>Tonase <req>*</req></label>
                        <input class="form-control" name="inst_tonase" value="<?php echo set_value('inst_tonase', $invoice_sewa_tongkang->inst_tonase); ?>" placeholder="Tonase" required>
                        <p class="help-block"><?php echo form_error('inst_tonase'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('inst_harga') != "") ? "has-error" : "" ?>">
                        <label>Harga </label>
                        <input class="form-control" type="number" name="inst_harga" placeholder="Harga Satuan" value="<?php echo set_value('inst_harga', $invoice_sewa_tongkang->inst_harga); ?>" required>
                        <p class="help-block"><?php echo form_error('inst_harga'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('inst_jumlah_total') != "") ? "has-error" : "" ?>">
                        <label>Jumlah </label>
                        <input class="form-control" type="number" name="inst_jumlah_total" placeholder="Jumlah" value="<?php echo set_value('inst_jumlah_total', $invoice_sewa_tongkang->inst_jumlah_total); ?>" required>
                        <p class="help-block"><?php echo form_error('inst_jumlah_total'); ?></p>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('inst_foto_invoice') != "") ? "has-error" : "" ?>">
                        <label>Upload Foto Invoice <req>*</req></label>
                        <input class="form-control" type="file" name="inst_foto_invoice" value="<?php echo set_value('inst_foto_invoice', $invoice_sewa_tongkang->inst_foto_invoice); ?>" placeholder="Upload Foto Invoice" required>
                        <p class="help-block"><?php echo form_error('inst_foto_invoice'); ?></p>
                        <?php if(!empty($invoice_sewa_tongkang->inst_foto_invoice)){?>
                            <img src="<?= base_url(). $invoice_sewa_tongkang->inst_foto_invoice?>" width="300">
                        <?php }?>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group <?php echo (form_error('inst_bukti_bayar') != "") ? "has-error" : "" ?>">
                        <label>Upload Bukti Bayar <req>*</req></label>
                        <input class="form-control" type="file" name="inst_bukti_bayar" value="<?php echo set_value('inst_bukti_bayar', $invoice_sewa_tongkang->inst_bukti_bayar); ?>" placeholder="Upload Bukti Bayar" required>
                        <p class="help-block"><?php echo form_error('inst_bukti_bayar'); ?></p>
                        <?php if(!empty($invoice_sewa_tongkang->inst_bukti_bayar)){?>
                            <img src="<?= base_url(). $invoice_sewa_tongkang->inst_bukti_bayar?>" width="300">
                        <?php }?>
                    </div>
                  </div>

                </div>


              </div>
              <div class="col-md-12">
                <hr>
              </div>
            </div>
      
            <div class="box-footer">
              <div class="clearfix pull-right">
                <a href="<?php echo $current_context; ?>" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </div><!-- /.box-footer -->
          </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!--/.col (right) -->
  </div>   <!-- /.row -->
  <div class="row">
    <div class="col-md-6 col-md-offset-3 col-md-offset-3">
      <p></p>
      <p class="text-center icon-page"><i class="fa fa-home fa-5x"></i></p>
      <br>
      <div class="callout callout-info">
        <h4><i class="fa fa-bullhorn fa-fw"></i> Learn More</h4>
        <p>Learn More about user <a href="">here</a>. Or <a href="">contact us</a> for more information</p>
      </div>
    </div>
  </div>
</section><!-- /.content -->