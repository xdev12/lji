<?php
	class Lib_template{
		protected $_ci;
		function __construct() {
	        $this->ci = &get_instance();
	        $this->ci->load->library('session');
	        $this->ci->load->model(array('m_user','m_product_category'));
	    }

		function display($template,$data=null){
			$data['_content']=$this->ci->load->view($template,$data,true);
			$data['_menu'] = $this->get_menu_category();
			$cart = $this->ci->session->userdata('cart_products');
			$data['cart'] = (!empty($cart))?count($cart):'0';
			// echo "<pre>";
			// print_r($data['_menu']);die();
			// $data['_header']=$this->_ci->load->view('header',$data,true);
			// $data['_top_menu']=$this->_ci->load->view('menu',$data,true);
			$this->ci->load->view('/template_user.php',$data);
		}

		function get_menu_category(){
	        # menu level 0
	        $parents = $this->ci->m_product_category->get_category();
	        foreach ($parents as $parent) {
	            # menu level 1
	            $parent->childs = $this->ci->m_product_category->get_category($parent->cat_id);
	            foreach ($parent->childs as $child) {
	                # menu level 2
	                $child->childs = $this->ci->m_product_category->get_category($child->cat_id);
	            }
	        }
	        return $parents;
	    }

	}
?>