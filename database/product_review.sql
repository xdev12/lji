/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : db_olshop

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2017-07-16 09:31:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `product_review`
-- ----------------------------
DROP TABLE IF EXISTS `product_review`;
CREATE TABLE `product_review` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `review_title` varchar(255) DEFAULT NULL,
  `review_description` text NOT NULL,
  `review_value` int(11) NOT NULL,
  `review_datetime` datetime NOT NULL,
  `review_status` tinyint(1) NOT NULL,
  `review_verifiedbuyer` tinyint(1) DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`review_id`),
  KEY `fk_r_7` (`product_id`),
  KEY `fk_r_9` (`member_id`),
  CONSTRAINT `product_review_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`),
  CONSTRAINT `product_review_ibfk_2` FOREIGN KEY (`member_id`) REFERENCES `member` (`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product_review
-- ----------------------------
INSERT INTO `product_review` VALUES ('1', '76', '18', 'jelek', 'sdfsdfsdf', '3', '2017-07-12 09:50:02', '0', '1', 'alvhie.nav12@gmail.com', '2017-07-12 09:50:02', null, null);
INSERT INTO `product_review` VALUES ('8', '76', '20', 'Barang OK', 'Pengiriman cepat, respon cpt', '5', '2017-07-16 04:15:50', '0', '0', 'allen', '2017-07-16 04:15:50', null, null);
