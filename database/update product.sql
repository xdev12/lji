/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : db_olshop

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2017-05-04 04:27:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `product`
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `product_name` varchar(150) NOT NULL,
  `product_description` text NOT NULL,
  `product_price` float NOT NULL,
  `product_stock` int(11) DEFAULT NULL,
  `product_discount` float DEFAULT NULL,
  `product_sold` int(11) NOT NULL,
  `product_weight` float NOT NULL,
  `product_status` tinyint(1) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `product_material` text,
  PRIMARY KEY (`product_id`),
  KEY `fk_r_1` (`cat_id`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `product_category` (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('40', '23', 'erer', 'gfhfghgfh', '45545', '344', '12', '0', '122', '1', 'super', '2017-04-24 22:53:29', null, null, null);
INSERT INTO `product` VALUES ('55', '22', 'sendal', 'aaaaa', '0', null, null, '0', '0', '1', 'admin', '2017-03-23 11:22:24', null, null, 'aa');
INSERT INTO `product` VALUES ('57', '22', 'sendal', 'aaaaa', '0', null, null, '0', '1', '1', 'admin', '2017-03-23 11:24:02', null, null, 'aaaa');
INSERT INTO `product` VALUES ('58', '22', 'Product Test', 'deskripsi', '0', null, null, '0', '1000', '1', 'admin', '2017-03-23 12:25:24', null, null, 'Spons');
INSERT INTO `product` VALUES ('61', '22', 'fgdf', 'dfgdfg', '32234', '234', '34', '0', '324', '1', 'super', '2017-04-24 23:02:13', null, null, null);
INSERT INTO `product` VALUES ('62', '27', 'Sweater SW-34', 'sdfsdf sdfdfsf sdfs ', '200000', '100', '10', '0', '100', '1', 'super', '2017-05-04 04:15:50', null, null, null);

-- ----------------------------
-- Table structure for `product_category`
-- ----------------------------
DROP TABLE IF EXISTS `product_category`;
CREATE TABLE `product_category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_cat_id` int(11) DEFAULT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_image` varchar(255) DEFAULT NULL,
  `cat_permalink` varchar(100) DEFAULT NULL,
  `cat_status` tinyint(1) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `fk_parent_category` (`parent_cat_id`),
  CONSTRAINT `product_category_ibfk_1` FOREIGN KEY (`parent_cat_id`) REFERENCES `product_category` (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product_category
-- ----------------------------
INSERT INTO `product_category` VALUES ('22', null, 'Woman', 'upload/cat_image/a28d3a5a365a0c1f73537baf257dddb6.png', 'woman', '1', 'admin', '2017-03-23 10:56:11', 'admin', '2017-03-24 18:19:17');
INSERT INTO `product_category` VALUES ('23', null, 'Men', 'upload/cat_image/eaff1f353974a620f4b6700eee2f33b9.png', 'man', '1', 'admin', '2017-03-31 15:06:56', null, null);
INSERT INTO `product_category` VALUES ('25', null, 'Children', 'upload/cat_image/2324a70e26af5edfdcc3a48e4e9837b0.png', 'children', '1', 'admin', '2017-04-05 13:56:58', null, null);
INSERT INTO `product_category` VALUES ('26', null, 'Other', null, 'other', '1', null, null, null, null);
INSERT INTO `product_category` VALUES ('27', '22', 'Sweater', null, 'woman/sweater', '1', 'super', '2017-04-24 08:49:00', 'super', '2017-04-24 09:02:21');
INSERT INTO `product_category` VALUES ('29', '22', 'Blouse', null, 'woman/blouse', '1', 'super', '2017-04-24 09:04:57', 'super', '2017-04-24 09:05:58');
INSERT INTO `product_category` VALUES ('30', '22', 'Scarf', null, 'woman/scarf', '1', null, null, null, null);
INSERT INTO `product_category` VALUES ('31', '23', 'Sweater', null, 'men/sweater', '1', null, null, null, null);
INSERT INTO `product_category` VALUES ('32', '23', 'Jacket', null, 'men/jacket', '1', null, null, null, null);
INSERT INTO `product_category` VALUES ('33', '23', 'Hat', null, 'men/hat', '1', null, null, null, null);
INSERT INTO `product_category` VALUES ('35', '25', 'Sweater', null, 'children/sweater', '1', null, null, null, null);
INSERT INTO `product_category` VALUES ('36', '25', 'Hat', null, 'children/hat', '1', null, null, null, null);
INSERT INTO `product_category` VALUES ('37', '25', 'Socks', null, 'children/socks', '1', null, null, null, null);
INSERT INTO `product_category` VALUES ('38', '26', 'Syal', null, 'other/syal', '1', null, null, null, null);

-- ----------------------------
-- Table structure for `product_photo`
-- ----------------------------
DROP TABLE IF EXISTS `product_photo`;
CREATE TABLE `product_photo` (
  `product_id` int(11) DEFAULT NULL,
  `prodphoto_path` varchar(255) NOT NULL,
  `prodphoto_description` tinyint(1) DEFAULT NULL,
  `prodphoto_isprimary` tinyint(1) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `prodphoto_token` varchar(255) NOT NULL,
  PRIMARY KEY (`prodphoto_path`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `product_photo_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product_photo
-- ----------------------------
INSERT INTO `product_photo` VALUES ('40', 'upload/prod_photo/40/77bc087ebe9f3226fe128e96c792b80e.png', null, '1', null, null, null, null, '');
INSERT INTO `product_photo` VALUES ('61', 'upload/prod_photo/77bc087ebe9f3226fe128e96c792b80e.png', null, '0', null, null, null, null, '');
INSERT INTO `product_photo` VALUES ('62', 'upload/prod_photo/d2ecf13b0dd2d05496365805d643f5d5.png', null, '1', null, null, null, null, '');
