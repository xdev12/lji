/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : db_olshop

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2017-05-08 15:59:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `bank`
-- ----------------------------
DROP TABLE IF EXISTS `bank`;
CREATE TABLE `bank` (
  `bank_id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(100) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`bank_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bank
-- ----------------------------
INSERT INTO `bank` VALUES ('1', 'Bank BCA', 'admin', '2016-04-28 15:55:59', 'admin', '2016-11-01 12:49:46');
INSERT INTO `bank` VALUES ('2', 'Bank Mandiri', 'admin', '2016-04-28 15:56:05', null, null);
INSERT INTO `bank` VALUES ('5', 'Bank Danamon', 'super', '2017-04-15 17:42:07', null, null);

-- ----------------------------
-- Table structure for `bank_account`
-- ----------------------------
DROP TABLE IF EXISTS `bank_account`;
CREATE TABLE `bank_account` (
  `account_id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_id` int(11) NOT NULL,
  `account_name` varchar(100) NOT NULL,
  `account_no` varchar(50) NOT NULL,
  `account_branch` varchar(50) NOT NULL,
  `account_status` tinyint(1) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`account_id`),
  KEY `fk_r_13` (`bank_id`),
  CONSTRAINT `bank_account_ibfk_1` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`bank_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bank_account
-- ----------------------------
INSERT INTO `bank_account` VALUES ('1', '1', 'Henny Alfianti', '4567890876', 'Bandung', '1', 'admin', '2016-04-28 15:56:32', 'admin', '2016-10-30 17:44:26');
INSERT INTO `bank_account` VALUES ('5', '2', 'lala123', '867948759', '', '1', 'admin', '2017-04-06 16:05:15', null, null);
INSERT INTO `bank_account` VALUES ('6', '5', 'Alvhie', '4958749853495', 'Batam', '1', 'super', '2017-04-15 17:45:31', null, null);

-- ----------------------------
-- Table structure for `member`
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT,
  `member_fullname` varchar(100) DEFAULT NULL,
  `member_password` varchar(100) DEFAULT NULL,
  `member_email` varchar(100) DEFAULT NULL,
  `member_phone` varchar(12) DEFAULT NULL,
  `member_login` tinyint(1) DEFAULT NULL,
  `member_status` tinyint(1) DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `is_deleted` smallint(6) DEFAULT NULL,
  `member_banned_note` text,
  `member_address` text,
  `province_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of member
-- ----------------------------
INSERT INTO `member` VALUES ('9', 'Henny Alfianti', '331d0fa4e989437956f5ee498bed06f8', 'henny.alfianti@gmail.com', '0909898898', '1', '1', 'Henny Alfianti', '2016-11-03 12:02:13', 'septi.setiawati@gmail.com', '2017-03-13 08:08:16', null, null, 'Jl. Makam Caringin No. 28 Bandung', null, null);

-- ----------------------------
-- Table structure for `menu`
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `men_menu_id` int(11) DEFAULT NULL,
  `menu_name` varchar(50) NOT NULL,
  `menu_link` varchar(100) NOT NULL,
  `menu_status` tinyint(1) NOT NULL,
  `menu_ismaster` tinyint(1) NOT NULL,
  `menu_order` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(10) DEFAULT NULL,
  `is_deleted` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`menu_id`),
  KEY `fk_parent_id` (`men_menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('15', null, 'Pengaturan', '#', '1', '1', '15', null, null, null, null, null);
INSERT INTO `menu` VALUES ('16', '15', 'User', 'admin/user', '1', '1', '16', null, null, null, null, null);
INSERT INTO `menu` VALUES ('17', '15', 'Menu', 'admin/menu', '1', '1', '17', null, null, null, null, null);
INSERT INTO `menu` VALUES ('18', '15', 'Hak Akses', 'admin/role', '1', '1', '18', null, null, null, null, null);
INSERT INTO `menu` VALUES ('19', '15', 'Menu Role', 'menu_role', '0', '1', '19', null, null, null, null, null);
INSERT INTO `menu` VALUES ('38', null, 'Dashboard', 'admin/dashboard', '1', '0', '1', '2016-05-05 12:27:56', 'super', null, null, null);
INSERT INTO `menu` VALUES ('53', null, 'History', 'admin/order/history', '1', '0', '12', null, null, null, null, null);
INSERT INTO `menu` VALUES ('42', null, 'Order', 'admin/order', '1', '0', '5', '2016-05-05 18:44:14', 'admin', null, null, null);
INSERT INTO `menu` VALUES ('46', null, 'Data Pelanggan', 'admin/member', '1', '0', '6', null, null, null, null, null);
INSERT INTO `menu` VALUES ('47', null, 'Laporan', 'admin/order/report', '1', '0', '8', null, null, null, null, null);
INSERT INTO `menu` VALUES ('48', null, 'Kategori Produk', 'admin/product_category', '1', '1', '9', null, null, null, null, null);
INSERT INTO `menu` VALUES ('49', null, 'Bank', 'admin/bank', '1', '1', '10', null, null, null, null, null);
INSERT INTO `menu` VALUES ('51', null, 'Akun Bank', 'admin/bank_account', '1', '1', '11', null, null, null, null, null);
INSERT INTO `menu` VALUES ('52', null, 'Produk', 'admin/product', '1', '0', '7', null, null, null, null, null);

-- ----------------------------
-- Table structure for `menu_role`
-- ----------------------------
DROP TABLE IF EXISTS `menu_role`;
CREATE TABLE `menu_role` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `UPDATED_ON` datetime DEFAULT NULL,
  `IS_DELETED` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`role_id`,`menu_id`),
  KEY `fk_menu_role2` (`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu_role
-- ----------------------------
INSERT INTO `menu_role` VALUES ('1', '45', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '44', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '43', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '41', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '40', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '19', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '18', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '17', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '16', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '42', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '39', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '38', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '15', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '46', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '47', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '48', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '49', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '51', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '52', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '53', null, null, null, null, null);

-- ----------------------------
-- Table structure for `order`
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `order_id` varchar(50) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `order_recipient` varchar(200) DEFAULT NULL,
  `order_datetime` datetime NOT NULL,
  `order_total_price` float NOT NULL,
  `order_total_weight` float NOT NULL,
  `order_shipping_fee` float NOT NULL,
  `order_delivery_type` varchar(200) NOT NULL,
  `order_note` text,
  `order_status` char(1) NOT NULL COMMENT '1=new order, 2=payment verif, 3=delivered, 4=cancel',
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `delivery_province` varchar(50) DEFAULT NULL,
  `delivery_city` varchar(50) DEFAULT NULL,
  `order_prod_total` float DEFAULT NULL,
  `order_type` smallint(6) DEFAULT NULL COMMENT '1 = cod, 2 = shipping',
  `order_province_id` int(6) DEFAULT NULL,
  `order_city_id` int(6) DEFAULT NULL,
  `order_jenis` smallint(6) DEFAULT NULL COMMENT '1 = online, 2 = offline',
  `order_postalcode` varchar(15) DEFAULT NULL,
  `order_address` text,
  `order_resi` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`order_id`),
  KEY `fk_r_17` (`member_id`),
  CONSTRAINT `order_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `member` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES ('ORD-2434545', '9', 'jjhg', '2017-04-15 10:11:55', '31000', '200', '10000', '1', null, '4', null, null, 'super', '2017-04-24 01:59:54', null, null, null, null, null, null, null, null, 'JL.MAKAM CARINGIN', '5656546');
INSERT INTO `order` VALUES ('ORD-3434345', '9', 'fdf', '2017-04-15 01:49:48', '30000', '1000', '10000', '1', null, '1', '', null, 'super', '2017-04-24 01:54:08', null, null, null, null, null, null, null, null, 'Jl. Pasadena', '');

-- ----------------------------
-- Table structure for `order_product`
-- ----------------------------
DROP TABLE IF EXISTS `order_product`;
CREATE TABLE `order_product` (
  `product_id` int(11) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `order_qty` int(11) NOT NULL,
  `order_prod_price` float NOT NULL,
  `order_prod_discount` float DEFAULT NULL,
  `order_prod_ispercent` tinyint(1) DEFAULT NULL,
  `order_prod_weight` float NOT NULL,
  `attr_value` varchar(50) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `order_prod_total` float DEFAULT NULL,
  PRIMARY KEY (`product_id`,`order_id`,`attr_value`),
  KEY `fk_r_20` (`order_id`),
  CONSTRAINT `order_product_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`),
  CONSTRAINT `order_product_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of order_product
-- ----------------------------
INSERT INTO `order_product` VALUES ('55', 'ORD-2434545', '1', '10000', '0', '0', '100', '', null, null, null, null, '10000');
INSERT INTO `order_product` VALUES ('57', 'ORD-2434545', '1', '11000', '0', '0', '100', '', null, null, null, null, '11000');
INSERT INTO `order_product` VALUES ('58', 'ORD-3434345', '1', '20000', '0', '0', '100', '', null, null, null, null, '20000');

-- ----------------------------
-- Table structure for `product`
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `product_name` varchar(150) NOT NULL,
  `product_description` text NOT NULL,
  `product_price` float NOT NULL,
  `product_stock` int(11) DEFAULT NULL,
  `product_discount` float DEFAULT NULL,
  `product_sold` int(11) NOT NULL,
  `product_weight` float NOT NULL,
  `product_status` tinyint(1) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `product_material` text,
  PRIMARY KEY (`product_id`),
  KEY `fk_r_1` (`cat_id`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `product_category` (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('40', '23', 'erer', 'gfhfghgfh', '45545', '344', '12', '0', '122', '1', 'super', '2017-04-24 22:53:29', null, null, null);
INSERT INTO `product` VALUES ('55', '22', 'sendal', 'aaaaa', '0', null, null, '0', '0', '1', 'admin', '2017-03-23 11:22:24', null, null, 'aa');
INSERT INTO `product` VALUES ('57', '22', 'sendal', 'aaaaa', '0', null, null, '0', '1', '1', 'admin', '2017-03-23 11:24:02', null, null, 'aaaa');
INSERT INTO `product` VALUES ('58', '22', 'Product Test', 'deskripsi', '0', null, null, '0', '1000', '1', 'admin', '2017-03-23 12:25:24', null, null, 'Spons');
INSERT INTO `product` VALUES ('61', '22', 'fgdf', 'dfgdfg', '32234', '234', '34', '0', '324', '1', 'super', '2017-04-24 23:02:13', null, null, null);
INSERT INTO `product` VALUES ('62', '27', 'Sweater SW-34', 'sdfsdf sdfdfsf sdfs ', '200000', '100', '10', '0', '100', '1', 'super', '2017-05-04 04:15:50', null, null, null);

-- ----------------------------
-- Table structure for `product_category`
-- ----------------------------
DROP TABLE IF EXISTS `product_category`;
CREATE TABLE `product_category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_cat_id` int(11) DEFAULT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_image` varchar(255) DEFAULT NULL,
  `cat_permalink` varchar(100) DEFAULT NULL,
  `cat_status` tinyint(1) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `fk_parent_category` (`parent_cat_id`),
  CONSTRAINT `product_category_ibfk_1` FOREIGN KEY (`parent_cat_id`) REFERENCES `product_category` (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product_category
-- ----------------------------
INSERT INTO `product_category` VALUES ('22', null, 'Women', 'upload/cat_image/a28d3a5a365a0c1f73537baf257dddb6.png', 'woman', '1', 'admin', '2017-03-23 10:56:11', 'admin', '2017-03-24 18:19:17');
INSERT INTO `product_category` VALUES ('23', null, 'Men', 'upload/cat_image/eaff1f353974a620f4b6700eee2f33b9.png', 'man', '1', 'admin', '2017-03-31 15:06:56', null, null);
INSERT INTO `product_category` VALUES ('25', null, 'Children', 'upload/cat_image/2324a70e26af5edfdcc3a48e4e9837b0.png', 'children', '1', 'admin', '2017-04-05 13:56:58', null, null);
INSERT INTO `product_category` VALUES ('26', null, 'Other', null, 'other', '1', null, null, null, null);
INSERT INTO `product_category` VALUES ('27', '22', 'Sweater', null, 'woman/sweater', '1', 'super', '2017-04-24 08:49:00', 'super', '2017-04-24 09:02:21');
INSERT INTO `product_category` VALUES ('29', '22', 'Blouse', null, 'woman/blouse', '1', 'super', '2017-04-24 09:04:57', 'super', '2017-04-24 09:05:58');
INSERT INTO `product_category` VALUES ('30', '22', 'Scarf', null, 'woman/scarf', '1', null, null, null, null);
INSERT INTO `product_category` VALUES ('31', '23', 'Sweater', null, 'men/sweater', '1', null, null, null, null);
INSERT INTO `product_category` VALUES ('32', '23', 'Jacket', null, 'men/jacket', '1', null, null, null, null);
INSERT INTO `product_category` VALUES ('33', '23', 'Hat', null, 'men/hat', '1', null, null, null, null);
INSERT INTO `product_category` VALUES ('35', '25', 'Sweater', null, 'children/sweater', '1', null, null, null, null);
INSERT INTO `product_category` VALUES ('36', '25', 'Hat', null, 'children/hat', '1', null, null, null, null);
INSERT INTO `product_category` VALUES ('37', '25', 'Socks', null, 'children/socks', '1', null, null, null, null);
INSERT INTO `product_category` VALUES ('38', '26', 'Syal', null, 'other/syal', '1', null, null, null, null);

-- ----------------------------
-- Table structure for `product_photo`
-- ----------------------------
DROP TABLE IF EXISTS `product_photo`;
CREATE TABLE `product_photo` (
  `product_id` int(11) DEFAULT NULL,
  `prodphoto_path` varchar(255) NOT NULL,
  `prodphoto_description` tinyint(1) DEFAULT NULL,
  `prodphoto_isprimary` tinyint(1) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `prodphoto_token` varchar(255) NOT NULL,
  PRIMARY KEY (`prodphoto_path`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `product_photo_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product_photo
-- ----------------------------
INSERT INTO `product_photo` VALUES ('40', 'upload/prod_photo/40/77bc087ebe9f3226fe128e96c792b80e.png', null, '1', null, null, null, null, '');
INSERT INTO `product_photo` VALUES ('61', 'upload/prod_photo/77bc087ebe9f3226fe128e96c792b80e.png', null, '0', null, null, null, null, '');
INSERT INTO `product_photo` VALUES ('62', 'upload/prod_photo/d2ecf13b0dd2d05496365805d643f5d5.png', null, '1', null, null, null, null, '');

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  `role_status` tinyint(1) NOT NULL,
  `role_canlogin` tinyint(1) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(10) DEFAULT NULL,
  `is_deleted` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'Super Admin', '1', '1', '0000-00-00 00:00:00', null, '2016-05-21 19:13:08', 'admin', null);
INSERT INTO `role` VALUES ('7', 'Administrator', '1', '1', '0000-00-00 00:00:00', 'super', '2016-05-21 19:13:35', 'admin', null);

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `username` varchar(10) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(10) DEFAULT NULL,
  `is_deleted` smallint(6) DEFAULT NULL,
  `id_clinic` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  KEY `fk_user_role` (`role_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1', 'super', '1b3231655cebb7a1f783eddf27d254ca', 'Administrator Super', '2016-04-22 13:54:09', 'super', '0000-00-00 00:00:00', '', null, null);
